/* GStreamer Recorder
 * (c) 2003 Ronald Bultje <rbultje@ronald.bitfreak.net>
 *
 * main.c: general/gnome/gst intialization
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <getopt.h>
#include <glib.h>
#include <glib/gi18n.h>
#include <gst/gst.h>

#include "debug.h"
#include "list.h"
#include "manager.h"
#include "stock.h"
#include "window.h"

static void
register_stock_icons (void)
{
  GtkIconFactory *icon_factory;
  struct
  {
    gchar *filename, *stock_id;
  } list[] = {
    {
    "brightness.png", GST_REC_STOCK_BRIGHTNESS}, {
    "contrast.png", GST_REC_STOCK_CONTRAST}, {
    "pause.png", GST_REC_STOCK_PAUSE}, {
    "play.png", GST_REC_STOCK_PLAY}, {
    "record.png", GST_REC_STOCK_RECORD}, {
    "saturation.png", GST_REC_STOCK_SATURATION}, {
    "slider.png", GST_REC_STOCK_SLIDER}, {
    "stop.png", GST_REC_STOCK_STOP}, {
    NULL, NULL}
  };
  gint num;

  icon_factory = gtk_icon_factory_new ();
  gtk_icon_factory_add_default (icon_factory);

  for (num = 0; list[num].filename != NULL; num++) {
    gchar *filename = gnome_program_locate_file (NULL,
        GNOME_FILE_DOMAIN_APP_PIXMAP,
        list[num].filename, TRUE, NULL);

    if (filename) {
      GdkPixbuf *pixbuf = gdk_pixbuf_new_from_file (filename, NULL);
      GtkIconSet *icon_set = gtk_icon_set_new_from_pixbuf (pixbuf);

      gtk_icon_factory_add (icon_factory, list[num].stock_id, icon_set);
      g_free (filename);
    }
  }
}

gint
main (gint argc, gchar * argv[])
{
  gchar *appfile;
  struct poptOption options[] = {
    {NULL, '\0', POPT_ARG_INCLUDE_TABLE, NULL, 0, "GStreamer", NULL},
    POPT_TABLEEND
  };

  /* i18n */
  bindtextdomain (GETTEXT_PACKAGE, GNOMELOCALEDIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);

  /* init gstreamer */
  options[0].arg = (void *) gst_init_get_popt_table ();

  /* init gtk/gnome */
  gnome_program_init (PACKAGE, VERSION,
      LIBGNOMEUI_MODULE,
      argc, argv,
      GNOME_PARAM_POPT_TABLE, options, GNOME_PARAM_APP_DATADIR, DATA_DIR, NULL);

  /* init ourselves */
  gst_rec_debug_init ();
  gst_rec_elements_init ();
  register_stock_icons ();
  gst_rec_lists_init ();

  /* add appicon image */
  appfile = gnome_program_locate_file (NULL,
      GNOME_FILE_DOMAIN_APP_PIXMAP, ICON_DIR "/cupid.png", TRUE, NULL);
  if (appfile) {
    gtk_window_set_default_icon_from_file (appfile, NULL);
    g_free (appfile);
  }

  create_window ();
  gtk_main ();

  return 0;
}
