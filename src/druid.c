/* GStreamer Recorder
 * (c) 2003 Ronald Bultje <rbultje@ronald.bitfreak.net>
 *
 * druid.c: first-time setup druid
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib/gi18n.h>
#include <gnome.h>

#include <gst/tuner/tuner.h>
#include <gst/mixer/mixer.h>

#include "druid.h"
#include "keys.h"
#include "list.h"

static void gst_rec_druid_class_init (GstRecDruidClass * klass);
static void gst_rec_druid_init (GstRecDruid * druid);
static void gst_rec_druid_dispose (GObject * object);

static GtkWidget *gst_rec_druid_contents (GConfClient * client,
    GstRecDruid * druid);

static GtkWidgetClass *parent_class = NULL;

GType
gst_rec_druid_get_type (void)
{
  static GType gst_rec_druid_type = 0;

  if (!gst_rec_druid_type) {
    static const GTypeInfo gst_rec_druid_info = {
      sizeof (GstRecDruidClass),
      NULL,
      NULL,
      (GClassInitFunc) gst_rec_druid_class_init,
      NULL,
      NULL,
      sizeof (GstRecDruid),
      0,
      (GInstanceInitFunc) gst_rec_druid_init,
      NULL
    };

    gst_rec_druid_type =
        g_type_register_static (GST_REC_TYPE_CONFIGURATION,
        "GstRecDruid", &gst_rec_druid_info, 0);
  }

  return gst_rec_druid_type;
}

static void
gst_rec_druid_class_init (GstRecDruidClass * klass)
{
  GObjectClass *gobject_class = (GObjectClass *) klass;

  parent_class = g_type_class_ref (GST_REC_TYPE_CONFIGURATION);

  gobject_class->dispose = gst_rec_druid_dispose;
}

static void
gst_rec_druid_init (GstRecDruid * druid)
{
  gtk_dialog_set_has_separator (GTK_DIALOG (druid), FALSE);
}

static void
gst_rec_druid_dispose (GObject * object)
{
  GstRecDruid *druid = GST_REC_DRUID (object);

  if (GST_REC_CONFIGURATION (druid)->client != NULL) {
    gconf_client_set_bool (GST_REC_CONFIGURATION (druid)->client,
        GST_REC_KEY_SETUP, TRUE, NULL);
  }

  if (((GObjectClass *) parent_class)->dispose)
    ((GObjectClass *) parent_class)->dispose (object);
}

/*
 * A GstRecDruid is just a GtkWindow with a GnomeDruid
 * embedded in it.
 */

GtkWidget *
gst_rec_druid_new (GstRec * rec, GConfClient * client)
{
  GstRecDruid *druid = g_object_new (GST_REC_TYPE_DRUID, NULL);

  /* backend setting */
  gst_rec_configuration_set_backend (GST_REC_CONFIGURATION (druid),
      rec, client);

  gtk_window_set_title (GTK_WINDOW (druid), _("Cupid Druid"));
  druid->druid = GNOME_DRUID (gst_rec_druid_contents (client, druid));
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (druid)->vbox),
      GTK_WIDGET (druid->druid), TRUE, TRUE, 0);
  gtk_widget_show (GTK_WIDGET (druid->druid));

  return GTK_WIDGET (druid);
}

/*
 * Cancel druid (from window).
 */

/*
 * Cancel druid (from page).
 */

static gboolean
cb_cancel (GnomeDruidPage * page, GtkWidget * druid, gpointer data)
{
  GstRecDruid *parent = GST_REC_DRUID (data);

  gtk_widget_destroy (GTK_WIDGET (parent));

  return TRUE;
}

/*
 * Finish off.
 */

static void
cb_finish (GnomeDruidPage * page, GtkWidget * druid, gpointer data)
{
  GstRecDruid *parent = GST_REC_DRUID (data);

  gtk_widget_destroy (GTK_WIDGET (parent));
}

/* 
 * Prepare summary page.
 */

static void
cb_prepare (GnomeDruidPage * page, GtkWidget * druid, gpointer data)
{
  GstRecConfiguration *conf = GST_REC_CONFIGURATION (data);
  GstRec *rec = conf->rec;
  GstRecVideoSource *video_source;
  GstRecAudioSource *audio_source;
  GstElement *video_element, *audio_element;
  GstRecMuxer *muxer;
  GstRecVideoEncoder *video_encoder;
  GstRecAudioEncoder *audio_encoder;
  gchar *text, *video_source_text, *audio_source_text;
  guint width, height;
  gdouble fps;
  guint channels, rate;
  GtkTreeIter iter;

  gtk_combo_box_get_active_iter (conf->video_sources.selection_menu, &iter);
  gtk_tree_model_get (gtk_combo_box_get_model (conf->video_sources.
          selection_menu), &iter, 1, &video_source, -1);
  width = gst_rec_get_video_width (rec);
  height = gst_rec_get_video_height (rec);
  fps = gst_rec_get_video_fps (rec);
  video_element = gst_rec_get_video_source (rec);
  if (GST_IS_TUNER (video_element)) {
    GstTuner *tuner = GST_TUNER (video_element);
    const GstTunerChannel *channel = gst_tuner_get_channel (tuner);
    const GstTunerNorm *norm = gst_tuner_get_norm (tuner);

    video_source_text =
        g_strdup_printf (_("Video source: %s "
            "(input: %s, norm: %s, size: %d x %d at %.02f fps)"),
            video_source ? video_source->name : "None",
            channel->label, norm->label,
            width, height, fps);
  } else {
    video_source_text =
        g_strdup_printf (_("Video source: %s (size: %d x %d at %.02f fps)"),
            video_source ? video_source->name : "None", width, height, fps);
  }

  gtk_combo_box_get_active_iter (conf->audio_sources.selection_menu, &iter);
  gtk_tree_model_get (gtk_combo_box_get_model (conf->audio_sources.
          selection_menu), &iter, 1, &audio_source, -1);
  rate = gst_rec_get_audio_samplerate (rec);
  channels = gst_rec_get_audio_channels (rec);
  audio_element = gst_rec_get_audio_source (rec);
  if (GST_IS_MIXER (audio_element)) {
    GstMixer *mixer = GST_MIXER (audio_element);
    const GList *tracks = gst_mixer_list_tracks (mixer);
    const GstMixerTrack *track = NULL;

    while (tracks) {
      GstMixerTrack *one = tracks->data;

      if (GST_MIXER_TRACK_HAS_FLAG (one, GST_MIXER_TRACK_RECORD)) {
        track = one;
        break;
      }

      tracks = tracks->next;
    }

    audio_source_text =
        g_strdup_printf (_("Audio source: %s (input: %s, %d Hz, %d channels)"),
            audio_source ? audio_source->name : "None",
            track ? track->label : "unknown", rate, channels);
  } else {
    audio_source_text =
        g_strdup_printf (_("Audio source: %s (%d Hz, %d channels)"),
            audio_source ? audio_source->name : "None", rate, channels);
  }

  if (gtk_combo_box_get_active_iter (conf->muxers.selection_menu, &iter)) {
    gtk_tree_model_get (gtk_combo_box_get_model (conf->muxers.selection_menu),
			&iter, 1, &muxer, -1);    
  } else {
    muxer = NULL;
  }

  if (gtk_combo_box_get_active_iter (conf->video_encoders.selection_menu, &iter)) {
    gtk_tree_model_get (gtk_combo_box_get_model (conf->video_encoders.
						 selection_menu), &iter, 1, &video_encoder, -1);
  } else {
    video_encoder = NULL;
  }
  if (gtk_combo_box_get_active_iter (conf->audio_encoders.selection_menu, &iter)) {
    gtk_tree_model_get (gtk_combo_box_get_model (conf->audio_encoders.
						 selection_menu), &iter, 1, &audio_encoder, -1);
  } else {
    audio_encoder = NULL;
  }

  text = g_strdup_printf (_("Configuration of Cupid is now "
          "completed. Below is a summary of your settings:\n"
          "\n%s\n%s\n\n"
          "Output format: %s\n"
          "Video encoder: %s\n"
          "Audio encoder: %s\n"
          "\n"
          "Press \"Apply\" to finish this druid."),
      video_source_text, audio_source_text,
      muxer ? muxer->name : "None",
      video_encoder ? video_encoder->name : "None",
      audio_encoder ? audio_encoder->name : "None");
  gnome_druid_page_edge_set_text (GNOME_DRUID_PAGE_EDGE (page), text);
  g_free (text);
  g_free (video_source_text);
  g_free (audio_source_text);
}

/*
 * Video source, video format (norm, input, size).
 */

static void
gst_rec_druid_video_source_page (GnomeDruidPageStandard * page,
    GstRecDruid * druid)
{
  GstRecConfiguration *conf = GST_REC_CONFIGURATION (druid);
  GtkWidget *widget = gst_rec_configuration_get_video_source_page (conf),
      *label =
      gtk_label_new (_("Below is a list of all video sources that were "
          "detected on your computer. Please select the "
          "plugin/device that you would like to use as a "
          "default. If your TV-card isn't in here, you "
          "might need to install the proper GStreamer "
          "plugins (e.g. \"video4linux\" or \"video4linux2\") first.")), *box =
      gtk_vbox_new (FALSE, 12);

  gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, GTK_MISC (label)->yalign);
  gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);

  gtk_table_set_col_spacings (GTK_TABLE (widget), 6);
  gtk_table_set_row_spacings (GTK_TABLE (widget), 2);

  gtk_box_pack_start (GTK_BOX (box), label, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (box), widget, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (GNOME_DRUID_PAGE_STANDARD (page)->vbox),
      box, TRUE, FALSE, 0);

  gtk_widget_show (widget);
  gtk_widget_show (label);
  gtk_widget_show (box);
}

/*
 * Audio source, audio format (input, samplerate, channels).
 */

static void
gst_rec_druid_audio_source_page (GnomeDruidPageStandard * page,
    GstRecDruid * druid)
{
  GstRecConfiguration *conf = GST_REC_CONFIGURATION (druid);
  GtkWidget *widget = gst_rec_configuration_get_audio_source_page (conf),
      *label =
      gtk_label_new (_("Below is a list of all audio sources that were "
          "detected on your computer. Please select the "
          "plugin/device that you would like to use as a "
          "default. If your soundcard isn't in here, you "
          "might need to install the proper GStreamer "
          "plugins (e.g. \"OSS\" or \"ALSA\") first.")), *box =
      gtk_vbox_new (FALSE, 12);

  gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, GTK_MISC (label)->yalign);
  gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);

  gtk_table_set_col_spacings (GTK_TABLE (widget), 6);
  gtk_table_set_row_spacings (GTK_TABLE (widget), 2);

  gtk_box_pack_start (GTK_BOX (box), label, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (box), widget, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (GNOME_DRUID_PAGE_STANDARD (page)->vbox),
      box, TRUE, FALSE, 0);

  gtk_widget_show (widget);
  gtk_widget_show (label);
  gtk_widget_show (box);
}

/*
 * Output format (muxer, video encoder, audio encoder).
 */

static void
gst_rec_druid_output_page (GnomeDruidPageStandard * page, GstRecDruid * druid)
{
  GstRecConfiguration *conf = GST_REC_CONFIGURATION (druid);
  GtkWidget *widget = gst_rec_configuration_get_output_format_page (conf),
      *label =
      gtk_label_new (_("Below are lists of all output file formats, video "
          "encoders and audio encoders that were found on "
          "your system. If your preferred file format or "
          "codec is missing, then you either need to install "
          "a plugin for that codec or format, or there is none "
          "available yet.")), *box = gtk_vbox_new (FALSE, 6);

  gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, GTK_MISC (label)->yalign);
  gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);

  gtk_table_set_col_spacings (GTK_TABLE (widget), 6);
  gtk_table_set_row_spacings (GTK_TABLE (widget), 2);

  gtk_box_pack_start (GTK_BOX (box), label, FALSE, FALSE, 6);
  gtk_box_pack_start (GTK_BOX (box), widget, FALSE, FALSE, 0);

  gtk_box_pack_start (GTK_BOX (GNOME_DRUID_PAGE_STANDARD (page)->vbox),
      box, TRUE, FALSE, 0);

  gtk_widget_show (widget);
  gtk_widget_show (label);
  gtk_widget_show (box);
}

/*
 * All pages together.
 */

static GtkWidget *
gst_rec_druid_contents (GConfClient * client, GstRecDruid * parent)
{
  GtkWidget *druid, *page;
  guint num = 0;
  struct
  {
    gchar *title;
    void (*fill) (GnomeDruidPageStandard * page, GstRecDruid * druid);
  }
  pages[] = {
    {
    _("%d/%d: Video Source"), gst_rec_druid_video_source_page}
    , {
    _("%d/%d: Audio Source"), gst_rec_druid_audio_source_page}
    , {
    _("%d/%d: Output Format"), gst_rec_druid_output_page}
    , {
    NULL}
  ,};
  gchar *s;

  druid = gnome_druid_new ();
  /* FIXME: gnome_druid_set_show_help (GNOME_DRUID (druid), TRUE); */

  /* Front page */
  page = gnome_druid_page_edge_new (GNOME_EDGE_START);
  g_signal_connect (G_OBJECT (page), "cancel", G_CALLBACK (cb_cancel), parent);
  gnome_druid_page_edge_set_title (GNOME_DRUID_PAGE_EDGE (page),
      _("First-time Configuration"));
  gnome_druid_page_edge_set_text (GNOME_DRUID_PAGE_EDGE (page),
      _("In the next few pages, this druid will ask you "
          "several questions which will decide which sources, "
          "codecs and file format will be used for video "
          "and audio recording.\n"
          "\n"
          "Press \"Forward\" to start the druid. Press "
          "\"Cancel\" at any time to stop the druid. You "
          "can manually re-configure all settings in the "
          "Preferences dialog."));
  gnome_druid_append_page (GNOME_DRUID (druid), GNOME_DRUID_PAGE (page));
  gtk_widget_show (GTK_WIDGET (page));

  /* Config contents */
  while (pages[num].title) {
    page = gnome_druid_page_standard_new ();
    g_signal_connect (G_OBJECT (page), "cancel",
        G_CALLBACK (cb_cancel), parent);
    s = g_strdup_printf (pages[num].title, num + 1, 3);
    gnome_druid_page_standard_set_title (GNOME_DRUID_PAGE_STANDARD (page), s);
    g_free (s);
    pages[num].fill (GNOME_DRUID_PAGE_STANDARD (page), parent);
    gnome_druid_append_page (GNOME_DRUID (druid), GNOME_DRUID_PAGE (page));
    gtk_widget_show (GTK_WIDGET (page));

    num++;
  }

  /* Summary page */
  page = gnome_druid_page_edge_new (GNOME_EDGE_FINISH);
  g_signal_connect (G_OBJECT (page), "cancel", G_CALLBACK (cb_cancel), parent);
  g_signal_connect (G_OBJECT (page), "finish", G_CALLBACK (cb_finish), parent);
  g_signal_connect (G_OBJECT (page), "prepare",
      G_CALLBACK (cb_prepare), parent);
  gnome_druid_page_edge_set_title (GNOME_DRUID_PAGE_EDGE (page),
      _("Configuration Completed"));
  gnome_druid_append_page (GNOME_DRUID (druid), GNOME_DRUID_PAGE (page));
  gtk_widget_show (GTK_WIDGET (page));

  return druid;
}
