/* GStreamer Recorder
 * (c) 2003 Ronald Bultje <rbultje@ronald.bitfreak.net>
 *
 * stats.c: statistics widget
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <libgnomevfs/gnome-vfs.h>

#include "stats.h"

static void gst_rec_stats_init (GstRecStats * stats);
static void gst_rec_stats_class_init (GstRecStatsClass * klass);
static void gst_rec_stats_dispose (GObject * object);

static GtkTableClass *parent_class = NULL;

GType
gst_rec_stats_get_type (void)
{
  static GType gst_rec_stats_type = 0;

  if (!gst_rec_stats_type) {
    static const GTypeInfo gst_rec_stats_info = {
      sizeof (GstRecStatsClass),
      NULL,
      NULL,
      (GClassInitFunc) gst_rec_stats_class_init,
      NULL,
      NULL,
      sizeof (GstRecStats),
      0,
      (GInstanceInitFunc) gst_rec_stats_init,
      NULL
    };

    gst_rec_stats_type = g_type_register_static (GTK_TYPE_TABLE,
	"GstRecStats", &gst_rec_stats_info, 0);
  }

  return gst_rec_stats_type;
}

static void
gst_rec_stats_init (GstRecStats * stats)
{
  gchar *labels[] = { _("Recording time"),
    _("Video frames/audio samples"),
    _("Lost/inserted/deleted frames"),
    _("Free disk space")
  };
  guint num_labels = sizeof (labels) / sizeof (gchar *), i;

  gtk_table_set_homogeneous (GTK_TABLE (stats), FALSE);
  gtk_table_resize (GTK_TABLE (stats), num_labels, 2);

  stats->items = g_malloc (num_labels * sizeof (GstRecStatsItem));
  for (i = 0; i < num_labels; i++) {
    gchar *txt;
    GstRecStatsItem *item = &stats->items[i];

    txt = g_strdup_printf ("<b>%s</b>:", labels[i]);
    item->label = gtk_label_new (txt);
    g_free (txt);
    gtk_label_set_use_markup (GTK_LABEL (item->label), TRUE);
    gtk_misc_set_alignment (GTK_MISC (item->label), 0.0,
	GTK_MISC (item->label)->yalign);
    gtk_table_attach_defaults (GTK_TABLE (stats), item->label, 0, 1, i, i + 1);
    gtk_widget_show (item->label);

    item->value = gtk_label_new ("");
    gtk_misc_set_alignment (GTK_MISC (item->value), 1.0,
	GTK_MISC (item->value)->yalign);
    gtk_table_attach_defaults (GTK_TABLE (stats), item->value, 1, 2, i, i + 1);
    gtk_widget_show (item->value);
  }
  stats->num_items = num_labels;
}

static void
gst_rec_stats_class_init (GstRecStatsClass * klass)
{
  GObjectClass *gobject_class;

  parent_class = g_type_class_ref (GTK_TYPE_TABLE);

  gobject_class = (GObjectClass *) klass;
  gobject_class->dispose = gst_rec_stats_dispose;
}

static void
gst_rec_stats_dispose (GObject * object)
{
  GstRecStats *stats = GST_REC_STATS (object);

  g_free (stats->items);

  if (((GObjectClass *) parent_class)->dispose)
    ((GObjectClass *) parent_class)->dispose (object);
}

GtkWidget *
gst_rec_stats_new (gboolean expand)
{
  GstRecStats *stats;

  stats = g_object_new (GST_REC_TYPE_STATS, NULL);
  gst_rec_stats_reset (stats);
  gst_rec_stats_expand (stats, expand);

  return GTK_WIDGET (stats);
}

void
gst_rec_stats_expand (GstRecStats * stats, gboolean expand)
{
  guint i;

  if (expand) {
    for (i = 1; i < stats->num_items; i++) {
      GstRecStatsItem *item = &stats->items[i];

      gtk_widget_show (item->label);
      gtk_widget_show (item->value);
    }
  } else {
    for (i = 1; i < stats->num_items; i++) {
      GstRecStatsItem *item = &stats->items[i];

      gtk_widget_hide (item->label);
      gtk_widget_hide (item->value);
    }
  }
}

void
gst_rec_stats_set (GstRecStats * stats,
    GstClockTime time,
    guint64 frames,
    guint64 samples,
    guint lost, guint inserted, guint deleted, GnomeVFSFileSize size)
{
  gchar *txt;
  GtkLabel *time_label = GTK_LABEL (stats->items[0].value),
      *frames_label = GTK_LABEL (stats->items[1].value),
      *lost_label = GTK_LABEL (stats->items[2].value),
      *space_label = GTK_LABEL (stats->items[3].value);

  /* time */
  txt = g_strdup_printf ("%u:%02u:%02u.%03u",
      (guint) (time / (GST_SECOND * 60 * 60)),
      (guint) ((time / (GST_SECOND * 60)) % 60),
      (guint) ((time / GST_SECOND) % 60),
      (guint) ((time / (GST_SECOND / 1000)) % 1000));
  gtk_label_set_text (time_label, txt);
  g_free (txt);

  /* frames/samples */
  txt = g_strdup_printf ("%" G_GUINT64_FORMAT " / %" G_GUINT64_FORMAT,
      frames, samples);
  gtk_label_set_text (frames_label, txt);
  g_free (txt);

  /* lost/inserted/deleted */
  txt = g_strdup_printf ("%u / %u / %u", lost, inserted, deleted);
  gtk_label_set_text (lost_label, txt);
  g_free (txt);

  /* free disk space */
  txt = gnome_vfs_format_file_size_for_display (size);
  gtk_label_set_text (space_label, txt);
  g_free (txt);
}

void
gst_rec_stats_file (GstRecStats * stats, GnomeVFSFileSize size)
{
  gst_rec_stats_set (stats, 0, 0, 0, 0, 0, 0, size);
}

void
gst_rec_stats_reset (GstRecStats * stats)
{
  gst_rec_stats_set (stats, 0, 0, 0, 0, 0, 0, 0);
}
