/* GStreamer Recorder
 * (c) 2003 Ronald Bultje <rbultje@ronald.bitfreak.net>
 *
 * configuration.h: set-up of UI-part of the pipeline selection
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gtk/gtk.h>
#include <gconf/gconf-client.h>
#include "number.h"
#include "record.h"

#ifndef __GST_REC_CONFIGURATION_H__
#define __GST_REC_CONFIGURATION_H__

#define GST_REC_TYPE_CONFIGURATION \
  (gst_rec_configuration_get_type())
#define GST_REC_CONFIGURATION(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_REC_TYPE_CONFIGURATION, \
			      GstRecConfiguration))
#define GST_REC_CONFIGURATION_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass), GST_REC_TYPE_CONFIGURATION, \
			   GstRecConfigurationClass))
#define GST_REC_IS_CONFIGURATION(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_REC_TYPE_CONFIGURATION))
#define GST_REC_IS_CONFIGURATION_CLASS(obj) \
  (G_TYPE_CHECK_CLASS_TYPE((klass), GST_REC_TYPE_CONFIGURATION))

typedef struct _GstRecConfiguration
{
  GtkDialog dialog;

  /* recorder object, for signals */
  GstRec *rec;

  /* GConf client object, for storing configuration */
  GConfClient *client;

  /* element lists */
  struct
  {
    GtkComboBox *selection_menu;
  } video_sources, audio_sources, video_encoders, audio_encoders, muxers;

  struct {
    GtkTable *table;
  } video_sources_page, audio_sources_page, output_format_page;

  /* video input/norm, audio input */
  struct
  {
    GtkComboBox *selection_menu;
    GtkWidget *title;
  } video_inputs, video_norms, audio_inputs;

  /* video size */
  struct
  {
    GstRecNumber *width, *height, *framerate;
    GtkWidget *title, *x, *at, *fps;
  } video_size;

  /* audio format */
  struct
  {
    GtkComboBox *selection_menu;
    GtkWidget *title;
  } audio_samplerates, audio_channels;

  /* signal IDs */
  GList *ids;
} GstRecConfiguration;

typedef struct _GstRecConfigurationClass
{
  GtkDialogClass parent;
} GstRecConfigurationClass;

GType gst_rec_configuration_get_type (void);

/* init call - done by the child class */
void gst_rec_configuration_set_backend (GstRecConfiguration * conf,
    GstRec * rec, GConfClient * client);

/* pages */
GtkWidget *gst_rec_configuration_get_video_source_page (GstRecConfiguration *
    conf);
GtkWidget *gst_rec_configuration_get_audio_source_page (GstRecConfiguration *
    conf);
GtkWidget *gst_rec_configuration_get_output_format_page (GstRecConfiguration *
    conf);
GtkWidget *gst_rec_configuration_get_metadata_page (GstRecConfiguration *
    conf);

#endif /* __GST_REC_CONFIGURATION_H__ */
