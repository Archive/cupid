/* GStreamer Recorder
 * (c) 2003 Ronald Bultje <rbultje@ronald.bitfreak.net>
 *
 * snapshot.c: snapshot save dialog
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib/gi18n.h>

#include "snapshot.h"
#include "window.h"

static void gst_rec_snapshot_class_init (GstRecSnapshotClass * klass);
static void gst_rec_snapshot_init (GstRecSnapshot * snap);
static void gst_rec_snapshot_dispose (GObject * object);
static void gst_rec_snapshot_response (GtkDialog * dialog, gint response_id);

static gboolean gst_rec_snapshot_save (GstRecSnapshot * snap);

static GtkDialogClass *parent_class = NULL;

GType
gst_rec_snapshot_get_type (void)
{
  static GType gst_rec_snapshot_type = 0;

  if (!gst_rec_snapshot_type) {
    static const GTypeInfo gst_rec_snapshot_info = {
      sizeof (GstRecSnapshotClass),
      NULL,
      NULL,
      (GClassInitFunc) gst_rec_snapshot_class_init,
      NULL,
      NULL,
      sizeof (GstRecSnapshot),
      0,
      (GInstanceInitFunc) gst_rec_snapshot_init,
      NULL
    };

    gst_rec_snapshot_type =
        g_type_register_static (GTK_TYPE_DIALOG,
        "GstRecSnapshot", &gst_rec_snapshot_info, 0);
  }

  return gst_rec_snapshot_type;
}

static void
gst_rec_snapshot_class_init (GstRecSnapshotClass * klass)
{
  GtkDialogClass *gtkdialog_class = GTK_DIALOG_CLASS (klass);
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  parent_class = g_type_class_ref (GTK_TYPE_DIALOG);

  gtkdialog_class->response = gst_rec_snapshot_response;
  object_class->dispose = gst_rec_snapshot_dispose;
}

static void
gst_rec_snapshot_init (GstRecSnapshot * snap)
{
  GtkWidget *hbox, *image, *table, *namelabel,
      *dirlabel, *nameentry, *dirbutton;

  gtk_container_set_border_width (GTK_CONTAINER (snap), 6);

  /* make window look cute */
  gtk_window_set_title (GTK_WINDOW (snap), _("Cupid: Save Snapshot"));
  gtk_container_set_border_width (GTK_CONTAINER (snap), 10);
  gtk_window_set_resizable (GTK_WINDOW (snap), FALSE);
  gtk_dialog_set_has_separator (GTK_DIALOG (snap), FALSE);
  gtk_dialog_add_buttons (GTK_DIALOG (snap),
      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
      GTK_STOCK_SAVE, GTK_RESPONSE_OK,
      NULL);

  /* container */
  hbox = gtk_hbox_new (FALSE, 12);

  /* frame for image */
  image = gtk_image_new ();
  gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, TRUE, 0);
  snap->image = GTK_IMAGE (image);
  gtk_widget_show (image);

  /* save-to */
  table = gtk_table_new (2, 2, FALSE);
  gtk_table_set_row_spacings (GTK_TABLE (table), 6);
  gtk_table_set_col_spacings (GTK_TABLE (table), 12);

  namelabel = gtk_label_new_with_mnemonic (_("_Name:"));
  gtk_table_attach (GTK_TABLE (table), namelabel, 0, 1, 0, 1,
      (GtkAttachOptions) (GTK_FILL), (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (namelabel), 0, 0.5);
  gtk_widget_show (namelabel);

  dirlabel = gtk_label_new_with_mnemonic (_("Save in _folder:"));
  gtk_table_attach (GTK_TABLE (table), dirlabel, 0, 1, 1, 2,
      (GtkAttachOptions) (GTK_FILL), (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (dirlabel), 0, 0.5);
  gtk_widget_show (dirlabel);

  nameentry = gtk_entry_new ();
  gtk_label_set_mnemonic_widget (GTK_LABEL (namelabel), nameentry);
  gtk_table_attach (GTK_TABLE (table), nameentry, 1, 2, 0, 1,
      (GtkAttachOptions) (GTK_EXPAND | GTK_FILL), (GtkAttachOptions) (0), 0, 0);
  gtk_entry_set_activates_default (GTK_ENTRY (nameentry), TRUE);
  gtk_entry_set_width_chars (GTK_ENTRY (nameentry), 24);
  snap->nameentry = GTK_ENTRY (nameentry);
  gtk_widget_show (nameentry);

  dirbutton = gtk_file_chooser_button_new ("Select a directory",
      GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER);
  gtk_label_set_mnemonic_widget (GTK_LABEL (dirlabel), dirbutton);
  gtk_table_attach (GTK_TABLE (table), dirbutton, 1, 2, 1, 2,
      (GtkAttachOptions) (GTK_FILL), (GtkAttachOptions) (GTK_FILL), 0, 0);
  snap->dirbutton = GTK_FILE_CHOOSER_BUTTON (dirbutton);
  gtk_widget_show (dirbutton);
  
  gtk_box_pack_start (GTK_BOX (hbox), table, TRUE, TRUE, 0);
  gtk_widget_show (table);

  /* add to dialog */
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (snap)->vbox), hbox, TRUE, TRUE, 0);
  gtk_widget_show (hbox);

  snap->pixbuf = NULL;
}

static void
gst_rec_snapshot_dispose (GObject * object)
{
  GstRecSnapshot *snap = GST_REC_SNAPSHOT (object);

  if (snap->pixbuf) {
    g_object_unref (G_OBJECT (snap->pixbuf));
    snap->pixbuf = NULL;
  }

  G_OBJECT_CLASS (parent_class)->dispose (object);
}

static void
gst_rec_snapshot_response (GtkDialog * dialog, gint response_id)
{
  switch (response_id) {
    case GTK_RESPONSE_OK:
      if (!gst_rec_snapshot_save (GST_REC_SNAPSHOT (dialog)))
        return;
      /* fall-through */

    case GTK_RESPONSE_CANCEL:
      gtk_widget_destroy (GTK_WIDGET (dialog));
      break;

    default:
      break;
  }

  if (((GtkDialogClass *) parent_class)->response)
    ((GtkDialogClass *) parent_class)->response (dialog, response_id);
}

/*
 * New dialog.
 */

GtkWidget *
gst_rec_snapshot_new (GdkPixbuf * pixbuf)
{
  GstRecSnapshot *snap;

  snap = GST_REC_SNAPSHOT (g_object_new (GST_REC_TYPE_SNAPSHOT, NULL));
  gst_rec_snapshot_set_pixbuf (snap, pixbuf);

  return GTK_WIDGET (snap);
}

/*
 * Update snapshot.
 */

static void
gst_rec_snapshot_update (GstRecSnapshot * snap)
{
  GdkPixbuf *tmp;

  g_return_if_fail (snap->pixbuf != NULL);

  tmp = gdk_pixbuf_scale_simple (snap->pixbuf, 200, 150,
      GDK_INTERP_BILINEAR);
  gtk_image_set_from_pixbuf (snap->image, tmp);
  g_object_unref (G_OBJECT (tmp));
}

void
gst_rec_snapshot_set_pixbuf (GstRecSnapshot * snap, GdkPixbuf * pixbuf)
{
  if (snap->pixbuf) {
    g_object_unref (G_OBJECT (snap->pixbuf));
    snap->pixbuf = NULL;
  }
  if (pixbuf) {
    snap->pixbuf = pixbuf;
  }

  gst_rec_snapshot_update (snap);
}

/*
 * Save.
 */

static gboolean
gst_rec_snapshot_save (GstRecSnapshot * snap)
{
  GError *err = NULL;
  gchar *folder;
  const gchar *filename;
  gchar *file;

  g_return_val_if_fail (snap->pixbuf != NULL, FALSE);

  folder = gtk_file_chooser_get_current_folder (
      GTK_FILE_CHOOSER (snap->dirbutton));
  filename = gtk_entry_get_text (GTK_ENTRY (snap->nameentry));
  file = g_build_filename (folder, filename, NULL);
  g_free (folder);

  if (!gdk_pixbuf_save (snap->pixbuf, file, "png", &err, NULL)) {
    gst_rec_popup_error (GTK_WINDOW (snap),
        _("Failed to save image: %s"), err->message);
    return FALSE;
  }

  return TRUE;
}
