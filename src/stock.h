/* GStreamer Recorder
 * (c) 2003 Ronald Bultje <rbultje@ronald.bitfreak.net>
 *
 * stock.h: stock icon defines
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GST_REC_STOCK_H__
#define __GST_REC_STOCK_H__

/* Stock icon definitions */
#define GST_REC_STOCK_BRIGHTNESS "cupid-brightness"
#define GST_REC_STOCK_CONTRAST   "cupid-contrast"
#define GST_REC_STOCK_PAUSE      "cupid-pause"
#define GST_REC_STOCK_PLAY       "cupid-play"
#define GST_REC_STOCK_RECORD     "cupid-record"
#define GST_REC_STOCK_SATURATION "cupid-saturation"
#define GST_REC_STOCK_SLIDER     "cupid-slider"
#define GST_REC_STOCK_STOP       "cupid-stop"

#endif /* __GST_REC_STOCK_H__ */
