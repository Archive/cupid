/* GStreamer Recorder
 * (c) 2003 Ronald Bultje <rbultje@ronald.bitfreak.net>
 *
 * tv.h: tv widget definition
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GST_TV_H__
#define __GST_TV_H__

#include <glib.h>
#include <gst/gst.h>
#include <gdk/gdk.h>
#include <gtk/gtkwidget.h>

G_BEGIN_DECLS
#define GST_REC_TYPE_TV \
  (gst_rec_tv_get_type ())
#define GST_REC_TV(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GST_REC_TYPE_TV, GstRecTv))
#define GST_REC_TV_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), GST_REC_TYPE_TV, GstRecTvClass))
#define GST_REC_IS_TV(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GST_REC_TYPE_TV))
#define GST_REC_IS_TV_CLASS(obj) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), GST_REC_TYPE_TV))

typedef struct _GstRecTv
{
  GtkWidget widget;

  /* video overlay element */
  GstElement *element;
  gboolean source;
  guint idle_expose;
} GstRecTv;

typedef struct _GstRecTvClass
{
  GtkWidgetClass klass;
} GstRecTvClass;

GType gst_rec_tv_get_type (void);
GtkWidget *gst_rec_tv_new (GstElement * element);
void gst_rec_tv_set_element (GstRecTv * tv, GstElement * element);

G_END_DECLS
#endif /* __GST_TV_H__ */
