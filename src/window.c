/* GStreamer Recorder
 * (c) 2003 Ronald Bultje <rbultje@ronald.bitfreak.net>
 *
 * window.c: user interface setup
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <string.h>
#include <unistd.h>
#include <glib.h>
#include <glib/gi18n.h>
#include <gst/gst.h>
#include <libgnomeui/libgnomeui.h>
#include <libgnomevfs/gnome-vfs.h>
#include <gconf/gconf-client.h>

#include <gst/mixer/mixer.h>
#include <gst/tuner/tuner.h>
#include <gst/xoverlay/xoverlay.h>

#include "convert.h"
#include "debug.h"
#include "druid.h"
#include "keys.h"
#include "list.h"
#include "preferences.h"
#include "record.h"
#include "sliders.h"
#include "snapshot.h"
#include "stats.h"
#include "stock.h"
#include "tv.h"
#include "window.h"

static void gst_rec_window_class_init (GstRecWindowClass * klass);
static void gst_rec_window_init (GstRecWindow * win);
static void gst_rec_window_dispose (GObject * object);

static gboolean gst_rec_window_key_press (GtkWidget *widget,
    GdkEventKey *event);

static void cb_destroy (GtkWidget * widget, gpointer data);
static void cb_preferences (GtkWidget * widget, gpointer data);
static void cb_toggle_minimal (GtkWidget * widget, gpointer data);
static void cb_toggle_statistics (GtkWidget * widget, gpointer data);
static void cb_toggle_fullscreen (GtkWidget * widget, gpointer data);
static void cb_about (GtkWidget * widget, gpointer data);
static void cb_video_source_changed (GstRec * rec,
    GstElement * video_source, gpointer data);
static void cb_statistics (GstRec * rec, GstClockTime time,
    guint64 frames, guint64 samples, guint lost_frames,
    guint ins_frames, guint del_frames, gpointer data);
static gboolean cb_allow_overwrite (GstElement * element,
    GnomeVFSURI * uri, gpointer data);
static void cb_error (GstElement * element,
    GstElement * originator, GError * error, gchar * debug, gpointer data);
static void cb_change_state_record (GtkWidget * widget, gpointer data);
static void cb_change_state_stop (GtkWidget * widget, gpointer data);
static void cb_play (GtkWidget * widget, gpointer data);
static void cb_snapshot (GtkWidget * widget, gpointer data);
static void cb_sliders (GtkWidget * widget, gpointer data);
static void cb_file_changed (GtkWidget * widget, gpointer data);
static void cb_file_browse (GtkWidget * widget, gpointer data);
static void cb_file_browse_response (GtkWidget * widget, gint response_id,
    gpointer data);
static void cb_file_browse_destroy (GtkWidget * widget, gpointer data);
static void cb_state_change (GstElement * element,
    GstElementState old_state, GstElementState new_state, gpointer data);
static void cb_toggle_widget (GConfClient * client,
    guint connection_id, GConfEntry * entry, gpointer data);
static void cb_toggle_element (GConfClient * client,
    guint connection_id, GConfEntry * entry, gpointer data);
static void load_config (GstRecWindow * win);

static GnomeAppClass *parent_class = NULL;

GType
gst_rec_window_get_type (void)
{
  static GType gst_rec_window_type = 0;

  if (!gst_rec_window_type) {
    static const GTypeInfo gst_rec_window_info = {
      sizeof (GstRecWindowClass),
      NULL,
      NULL,
      (GClassInitFunc) gst_rec_window_class_init,
      NULL,
      NULL,
      sizeof (GstRecWindow),
      0,
      (GInstanceInitFunc) gst_rec_window_init,
      NULL
    };
    gst_rec_window_type = g_type_register_static (GNOME_TYPE_APP,
	"GstRecWindow", &gst_rec_window_info, 0);
  }
  return gst_rec_window_type;
}

static void
gst_rec_window_class_init (GstRecWindowClass * klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  parent_class = g_type_class_ref (GNOME_TYPE_APP);

  gobject_class->dispose = gst_rec_window_dispose;
  widget_class->key_press_event = gst_rec_window_key_press;
}

/*
 * Menus.
 */

static GnomeUIInfo file_menu[] = {
  GNOMEUIINFO_MENU_EXIT_ITEM (cb_destroy, NULL),
  GNOMEUIINFO_END
};

#define GNOMEUIINFO_TOGGLEITEM_QUICKKEY(label, tip, cb, stock_id, quickkey) \
        { GNOME_APP_UI_TOGGLEITEM, label, tip, (gpointer) cb, \
	  NULL, NULL, GNOME_APP_PIXMAP_STOCK, stock_id, \
	  quickkey, GDK_CONTROL_MASK, NULL }

static GnomeUIInfo view_menu[] = {
  GNOMEUIINFO_TOGGLEITEM_QUICKKEY (N_("_Minimal"),
      N_("Toggle minimal (TV-like) mode"),
      cb_toggle_minimal, NULL, 'm'),
  GNOMEUIINFO_TOGGLEITEM_QUICKKEY (N_("S_tatistics"),
      N_("Toggle visibility of statistics"),
      cb_toggle_statistics, NULL, 0),
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_TOGGLEITEM_QUICKKEY (N_("_Full Screen"),
      N_("Toggle full-screen mode"),
      cb_toggle_fullscreen, NULL, 'f'),
  GNOMEUIINFO_END
};

#define GNOMEUIINFO_ITEM_QUICKKEY(label, tip, cb, stock_id, quickkey) \
        { GNOME_APP_UI_ITEM, label, tip, (gpointer) cb, \
	  NULL, NULL, GNOME_APP_PIXMAP_STOCK, stock_id, \
	  quickkey, GDK_CONTROL_MASK, NULL }

static GnomeUIInfo actions_menu[] = {
  GNOMEUIINFO_ITEM_QUICKKEY (N_("S_top"), N_("Stop Recording"),
      cb_change_state_stop,
      GST_REC_STOCK_STOP, 't'),
#if 0
  GNOMEUIINFO_ITEM_QUICKKEY (N_("_Pause"), N_("Pause Recording"),
      cb_change_state_pause,
      GST_REC_STOCK_PAUSE, 'p'),
#endif
  GNOMEUIINFO_ITEM_QUICKKEY (N_("_Record"), N_("Start Recording"),
      cb_change_state_record,
      GST_REC_STOCK_RECORD, 'r'),
  GNOMEUIINFO_ITEM_QUICKKEY (N_("_Play"), N_("Play Movie"),
      cb_play,
      GST_REC_STOCK_PLAY, 'p'),
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_ITEM_QUICKKEY (N_("_Snapshot"), N_("Take a snapshot"),
      cb_snapshot, NULL, 's'),
  GNOMEUIINFO_END
};

static GnomeUIInfo settings_menu[] = {
  GNOMEUIINFO_MENU_PREFERENCES_ITEM (cb_preferences, NULL),
  GNOMEUIINFO_END
};

static GnomeUIInfo help_menu[] = {
  GNOMEUIINFO_HELP (PACKAGE),
  GNOMEUIINFO_MENU_ABOUT_ITEM (cb_about, NULL),
  GNOMEUIINFO_END
};

#define GNOMEUIINFO_MENU_ACTIONS_TREE(tree) \
	{ GNOME_APP_UI_SUBTREE_STOCK, N_("_Actions"), \
	  NULL, tree, NULL, NULL, (GnomeUIPixmapType) 0, \
	  NULL, 0, (GdkModifierType) 0, NULL }

static GnomeUIInfo main_menu[] = {
  GNOMEUIINFO_MENU_FILE_TREE (file_menu),
  GNOMEUIINFO_MENU_VIEW_TREE (view_menu),
  GNOMEUIINFO_MENU_ACTIONS_TREE (actions_menu),
  GNOMEUIINFO_MENU_SETTINGS_TREE (settings_menu),
  GNOMEUIINFO_MENU_HELP_TREE (help_menu),
  GNOMEUIINFO_END
};

/*
 * Toolbar.
 */

static GnomeUIInfo toolbar[] = {
  GNOMEUIINFO_ITEM_STOCK (N_("Stop"), N_("Stop Recording"),
      cb_change_state_stop, GST_REC_STOCK_STOP),
#if 0
  GNOMEUIINFO_ITEM_STOCK (N_("Pause"), N_("Pause Recording"),
      cb_change_state_pause, GST_REC_STOCK_PAUSE),
#endif
  GNOMEUIINFO_ITEM_STOCK (N_("Record"), N_("Start Recording"),
      cb_change_state_record, GST_REC_STOCK_RECORD),
  GNOMEUIINFO_ITEM_STOCK (N_("Play"), N_("Play Movie"),
      cb_play, GST_REC_STOCK_PLAY),
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_ITEM_STOCK (N_("Balance"), N_("Video Color and Audio Volume"),
      cb_sliders, GST_REC_STOCK_SLIDER),
  GNOMEUIINFO_END
};

static void
gst_rec_window_init (GstRecWindow * win)
{
  GtkWidget *appbar, *area, *filebox, *fileentry,
      *filebutton, *stats, *box, *tv;
  GstRec *rec;
  GstElement *filesink;

  /* gconf */
  win->client = gconf_client_get_default ();
  gconf_client_add_dir (win->client, GST_REC_KEY_BASEDIR,
      GCONF_CLIENT_PRELOAD_RECURSIVE, NULL);

  /* UI config entries */
  gconf_client_notify_add (win->client, GST_REC_KEY_UIDIR,
      cb_toggle_widget, win, NULL, NULL);

  /* Gst config entries */
  gconf_client_notify_add (win->client, GST_REC_KEY_GSTDIR,
      cb_toggle_element, win, NULL, NULL);

  /* backend setup */
  rec = gst_rec_new ();
  win->rec = rec;
  g_signal_connect (G_OBJECT (rec), "state_change",
      G_CALLBACK (cb_state_change), win);
  g_signal_connect (G_OBJECT (rec), "error", G_CALLBACK (cb_error), win);
  filesink = gst_element_factory_make ("gnomevfssink", "output-0");
  g_signal_connect (filesink, "allow-overwrite",
      G_CALLBACK (cb_allow_overwrite), win);
  gst_rec_set_output (rec, filesink);
  win->output = filesink;

  /* main window */
  gnome_app_construct (GNOME_APP (win), PACKAGE, "Cupid");

  /* menu bar */
  gnome_app_create_menus_with_data (GNOME_APP (win),
      main_menu, (gpointer) win);
  GTK_CHECK_MENU_ITEM (view_menu[1].widget)->active = TRUE;

  /* toolbar */
  gnome_app_create_toolbar_with_data (GNOME_APP (win), toolbar,
      (gpointer) win);

  /* main area */
  area = gtk_vbox_new (FALSE, 2);
  win->mainbox = area;
  gtk_container_set_border_width (GTK_CONTAINER (area), 2);
  gnome_app_set_contents (GNOME_APP (win), area);

  /* TV widget */
  tv = gst_rec_tv_new (NULL);
  gtk_box_pack_start (GTK_BOX (area), tv, TRUE, TRUE, 0);
  g_signal_connect (G_OBJECT (rec), "video_source_changed",
      G_CALLBACK (cb_video_source_changed), win);
  win->tv = GST_REC_TV (tv);

  /* a second box containing the rest (different padding) */
  box = gtk_vbox_new (FALSE, 6);
  win->box = box;
  gtk_container_set_border_width (GTK_CONTAINER (box), 4);
  gtk_box_pack_start (GTK_BOX (area), box, FALSE, FALSE, 0);

  /* file selection widget */
  filebox = gtk_hbox_new (FALSE, 6);
  win->filebox = filebox;
  gtk_box_pack_start (GTK_BOX (box), filebox, TRUE, TRUE, 0);
  
  fileentry = gtk_entry_new ();
  g_signal_connect (G_OBJECT (fileentry), "changed",
      G_CALLBACK (cb_file_changed), win);
  win->fileentry = GTK_ENTRY (fileentry);
  gtk_box_pack_start (GTK_BOX (filebox), fileentry, TRUE, TRUE, 0);
  
  filebutton = gtk_button_new_with_label (_("Browse"));
  g_signal_connect (G_OBJECT (filebutton), "pressed",
      G_CALLBACK (cb_file_browse), win);
  gtk_box_pack_end (GTK_BOX (filebox), filebutton, FALSE, FALSE, 0);

  /* statistics */
  stats = gst_rec_stats_new (TRUE);
  g_signal_connect (G_OBJECT (rec), "statistics",
      G_CALLBACK (cb_statistics), win);
  win->stats = GST_REC_STATS (stats);
  gtk_table_set_col_spacings (GTK_TABLE (stats), 6);
  gtk_box_pack_start (GTK_BOX (box), stats, TRUE, TRUE, 0);

  /* status bar */
  appbar = gnome_appbar_new (FALSE, TRUE, GNOME_PREFERENCES_USER);
  gnome_app_set_statusbar (GNOME_APP (win), appbar);
  gnome_app_install_appbar_menu_hints (GNOME_APPBAR (appbar), main_menu);
}

void
create_window (void)
{
  GtkWidget *win = GTK_WIDGET (g_object_new (GST_REC_TYPE_WINDOW, NULL));

  /* show off */
  gtk_widget_show_all (win);
  gtk_widget_hide (toolbar[0].widget);
  gtk_widget_hide (actions_menu[0].widget);
  gtk_widget_set_sensitive (toolbar[2].widget, FALSE);
  gtk_widget_set_sensitive (actions_menu[2].widget, FALSE);

  load_config (GST_REC_WINDOW (win));
  gst_rec_set_tag (GST_REC_WINDOW (win)->rec, GST_TAG_ENCODER, "Cupid");

  g_signal_connect (win, "destroy", G_CALLBACK (gtk_main_quit), NULL);
}

static void
gst_rec_window_dispose (GObject * object)
{
  GstRecWindow *win = GST_REC_WINDOW (object);

  if (win->rec) {
    gst_object_unref (GST_OBJECT (win->rec));
    win->rec = NULL;
  }
  if (win->client) {
    g_object_unref (win->client);
    win->client = NULL;
  }

  if (((GObjectClass *) parent_class)->dispose)
    ((GObjectClass *) parent_class)->dispose (object);
}

/*
 * Key presses, for fullscreen.
 */

static gboolean
gst_rec_window_key_press (GtkWidget *widget, GdkEventKey *event)
{
  GstRecWindow *win = GST_REC_WINDOW (widget);

  if (win->fullscreen &&
      ((event->state == GDK_CONTROL_MASK && event->keyval == 'f') ||
       event->keyval == GDK_Escape)) {
    GTK_CHECK_MENU_ITEM (view_menu[3].widget)->active = FALSE;
    cb_toggle_fullscreen (view_menu[3].widget, win);

    return TRUE;
  }

  if (GTK_WIDGET_CLASS (parent_class)->key_press_event)
    return GTK_WIDGET_CLASS (parent_class)->key_press_event (widget, event);

  return FALSE;
}

/*
 * Menu / Toolbar callbacks.
 */

static void
cb_destroy (GtkWidget * widget, gpointer data)
{
  gtk_widget_destroy (GTK_WIDGET (data));
}

static void
cb_preferences_destroy (GtkWidget * widget, gpointer data)
{
  *(GstRecPreferences **) data = NULL;
}

static void
cb_preferences (GtkWidget * widget, gpointer data)
{
  GstRecWindow *win = GST_REC_WINDOW (data);
  static GtkWidget *prefs = NULL;

  if (prefs != NULL) {
    /* re-focus */
    gtk_window_present (GTK_WINDOW (prefs));

    return;
  }

  prefs = gst_rec_preferences_new (win->rec, win->client);
  g_signal_connect (G_OBJECT (prefs), "destroy",
      G_CALLBACK (cb_preferences_destroy), &prefs);
  gtk_widget_show (prefs);
}

static void
cb_toggle_minimal (GtkWidget * widget, gpointer data)
{
  GstRecWindow *win = GST_REC_WINDOW (data);

  gconf_client_set_bool (win->client, GST_REC_KEY_MINIMAL_MODE,
      GTK_CHECK_MENU_ITEM (widget)->active, NULL);
}

static void
cb_toggle_statistics (GtkWidget * widget, gpointer data)
{
  GstRecWindow *win = GST_REC_WINDOW (data);

  gconf_client_set_bool (win->client, GST_REC_KEY_STATISTICS_VISIBILITY,
      GTK_CHECK_MENU_ITEM (widget)->active, NULL);
}

static void
cb_toggle_fullscreen (GtkWidget * widget, gpointer data)
{
  GstRecWindow *win = GST_REC_WINDOW (data);
  GtkWidget *menu, *toolbar, *status;

  win->fullscreen = GTK_CHECK_MENU_ITEM (widget)->active;

  menu = GNOME_APP (win)->menubar;
  toolbar = GTK_WIDGET (gnome_app_get_dock_item_by_name (GNOME_APP (win),
      GNOME_APP_TOOLBAR_NAME));
  status = GNOME_APP (win)->statusbar;
                                                                                
  if (!win->fullscreen) {
    gtk_widget_show (menu);
    if (!gconf_client_get_bool (win->client, GST_REC_KEY_MINIMAL_MODE, NULL)) {
      gtk_widget_show (toolbar);
      gtk_widget_show (win->box);
      gtk_widget_show (status);
    }
    gtk_container_set_border_width (GTK_CONTAINER (win->mainbox), 2);

    gtk_window_unfullscreen (GTK_WINDOW (win));
  } else {
    gtk_widget_hide (menu);
    gtk_widget_hide (toolbar);
    gtk_widget_hide (win->box);
    gtk_widget_hide (status);
    gtk_container_set_border_width (GTK_CONTAINER (win->mainbox), 0);

    gtk_window_fullscreen (GTK_WINDOW (win));
  }
}

static void
cb_about (GtkWidget * widget, gpointer data)
{
  GstRecWindow *win = GST_REC_WINDOW (data);
  
  const gchar *authors[] = { "Ronald Bultje <rbultje@ronald.bitfreak.net>",
    NULL
  };

  /* Translators note: here, put your own name - it will be shown
   * in the about dialog. */
  const gchar *translator_credits = _("translator-credits");

  if (strcmp (translator_credits, "translator-credits") == 0)
      translator_credits = NULL;
  
  gtk_show_about_dialog (GTK_WINDOW (win),
      "name", "Cupid",
      "version", VERSION,
      "copyright", "(c) 2003-2005 Ronald Bultje",
      "comments", _("A GNOME/GStreamer video capture application"),
      "authors", authors,
      "translator-credits", translator_credits,
      NULL);
}

static void
cb_video_source_changed (GstRec * rec, GstElement * video_source, gpointer data)
{
  GstRecWindow *win = GST_REC_WINDOW (data);
  GstElement *xoverlay;

  xoverlay = gst_rec_video_get_by_interface (GST_REC (win->rec),
      GST_TYPE_X_OVERLAY);
  gst_rec_tv_set_element (win->tv, xoverlay);
}

static GnomeVFSFileSize
get_free_space (GstRecWindow * win)
{
  GnomeVFSFileSize size;
  GnomeVFSURI *parent;
  gchar *dir;

  if (win->file) {
    if ((dir = gnome_vfs_uri_extract_dirname (win->file))) {
      if ((parent = gnome_vfs_uri_new (dir))) {
        if (gnome_vfs_get_volume_free_space (parent, &size) != GNOME_VFS_OK)
          size = -1;
        gnome_vfs_uri_unref (parent);
      }
      g_free (dir);
    }
  }

  return size;
}

static void
cb_statistics (GstRec * rec,
    GstClockTime time,
    guint64 frames,
    guint64 samples,
    guint lost_frames, guint ins_frames, guint del_frames, gpointer data)
{
  GstRecWindow *win = GST_REC_WINDOW (data);

  gst_rec_stats_set (win->stats, time, frames, samples,
      lost_frames, ins_frames, del_frames, get_free_space (win));
}

static gboolean
cb_allow_overwrite (GstElement * element, GnomeVFSURI * uri, gpointer data)
{
  GstRecWindow *win = GST_REC_WINDOW (data);
  GtkWidget *window;
  gint res;
  gchar *filename = gnome_vfs_uri_to_string (uri, GNOME_VFS_URI_HIDE_PASSWORD);

  window = gtk_message_dialog_new (GTK_WINDOW (win),
      GTK_DIALOG_MODAL, GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO,
      "The file %s already exists. Overwrite?", filename);
  g_free (filename);
  gtk_widget_show (window);
  res = gtk_dialog_run (GTK_DIALOG (window));
  gtk_widget_destroy (window);

  return (res == GTK_RESPONSE_YES);
}

typedef struct _GstRecErrorHandler
{
  GstRecWindow *win;
  GstElement *source;
  gchar *msg;
}
GstRecErrorHandler;

static gboolean
cb_error_real (gpointer data)
{
  GstRecErrorHandler *handler = data;
  GstRecWindow *win = handler->win;
  GstElementFactory *factory = gst_element_get_factory (handler->source);

  gst_rec_popup_error (win,
      _("Element %s gave an error:\n\n%s"),
      gst_element_factory_get_longname (factory), handler->msg);
  g_free (handler->msg);
  gst_object_unref (GST_OBJECT (handler->source));
  g_free (handler);

  /* die here */
  gst_element_set_state (GST_ELEMENT (win->rec), GST_STATE_NULL);

  /* once */
  return FALSE;
}

static void
cb_error (GstElement * element,
    GstElement * originator, GError * error, gchar * debug, gpointer data)
{
  GstRecErrorHandler *handler = g_new (GstRecErrorHandler, 1);

  /* move to our own thread */
  handler->win = GST_REC_WINDOW (data);
  handler->source = originator;
  gst_object_ref (GST_OBJECT (originator));
  handler->msg = g_strdup (error->message);
  g_idle_add ((GSourceFunc) cb_error_real, handler);
}

static void
cb_change_state_record (GtkWidget * widget, gpointer data)
{
  GstRecWindow *win = GST_REC_WINDOW (data);

  if (gst_rec_set_state (win->rec, GST_STATE_PLAYING) == GST_STATE_FAILURE) {
    gst_rec_popup_error (win, _("Failed to start recording!"));
  }
}

#if 0
static void
cb_change_state_pause (GtkWidget * widget, gpointer data)
{
  GstRecWindow *win = GST_REC_WINDOW (data);

  if (gst_rec_set_state (win->rec, GST_STATE_PAUSED) == GST_STATE_FAILURE) {
    gst_rec_popup_error (win, _("Failed to pause recording!"));
  }
}
#endif

static void
cb_change_state_stop (GtkWidget * widget, gpointer data)
{
  GstRecWindow *win = GST_REC_WINDOW (data);

  if (gst_rec_set_state (win->rec, GST_STATE_NULL) == GST_STATE_FAILURE) {
    gst_rec_popup_error (win, _("Failed to stop recording!"));
  }
}

static void
cb_play (GtkWidget * widget, gpointer data)
{
  GstRecWindow *win = GST_REC_WINDOW (data);
  GError *error = NULL;
  gchar *cmd, *file;

  file = gnome_vfs_uri_to_string (win->file, GNOME_VFS_URI_HIDE_NONE);
  cmd = g_strdup_printf ("totem %s", file);
  g_free (file);
  if (!g_spawn_command_line_async (cmd, &error)) {
    g_free (cmd);
    cmd = g_strdup_printf (_("Failed to start totem: %s"), error->message);
    gst_rec_popup_error (win, cmd);
    g_error_free (error);
  }
  g_free (cmd);
}

/*
 * Snapshot support.
 */

static void
cb_destroypixbuf (guchar * pix, gpointer data)
{
  gst_buffer_unref (GST_BUFFER (data));
}

static void
cb_snapshot_destroy (GtkWidget * widget, gpointer data)
{
  *(GstRecSnapshot **) data = NULL;
}

static void
cb_snapshot (GtkWidget * widget, gpointer data)
{
  GstRecWindow *win = GST_REC_WINDOW (data);
  GstBuffer *buf, *res;
  GstCaps *from;
  static GtkWidget *snap = NULL;
  GdkPixbuf *pixbuf;

  /* take snapshot */
  if (!gst_rec_get_snapshot (win->rec, &buf, &from)) {
    gst_rec_popup_error (win,
        _("Failed to create a snapshot - please note that this feature "
          "currently only works for webcams, not yet for TV-cards."));
    return;
  }
  buf = gst_buffer_copy_on_write (buf);

  /* convert to a better format */
  res = gst_rec_convert (buf, from,
      gst_caps_new_simple ("video/x-raw-rgb",
          "width", G_TYPE_INT, gst_rec_get_video_width (win->rec),
          "height", G_TYPE_INT, gst_rec_get_video_height (win->rec),
          "bpp", G_TYPE_INT, 24,
          "depth", G_TYPE_INT, 24,
          "pixel-aspect-ratio", GST_TYPE_FRACTION, 1, 1,
          "endianness", G_TYPE_INT, G_BIG_ENDIAN,
          "red_mask", G_TYPE_INT, 0xff0000,
          "green_mask", G_TYPE_INT, 0x00ff00,
          "blue_mask", G_TYPE_INT, 0x0000ff, NULL));
  if (!res) {
    g_warning ("Failed to convert snapshot to a picture - this should "
        "never happen.");
    return;
  }

  /* make GdkPixbuf */
  pixbuf = gdk_pixbuf_new_from_data (GST_BUFFER_DATA (res),
      GDK_COLORSPACE_RGB, FALSE, 8, gst_rec_get_video_width (win->rec),
      gst_rec_get_video_height (win->rec),
      3 * gst_rec_get_video_width (win->rec), cb_destroypixbuf, res);
  if (!pixbuf) {
    gst_buffer_unref (res);
    g_warning ("Failed to create pixbuf");
    return;
  }

  /* now show window, or update image */
  if (snap != NULL) {
    gst_rec_snapshot_set_pixbuf (GST_REC_SNAPSHOT (snap), pixbuf);
    gtk_window_present (GTK_WINDOW (snap));
  } else {
    snap = gst_rec_snapshot_new (pixbuf);
    g_signal_connect (snap, "destroy",
        G_CALLBACK (cb_snapshot_destroy), &snap);
    gtk_widget_show (snap);
  }
}

static void
cb_sliders (GtkWidget * widget, gpointer data)
{
  GstRecWindow *win = GST_REC_WINDOW (data);
  GtkWidget *window;

  /* NULL is a valid response here! It simply means that no
   * single "slider-able" interface is implemented. */
  window = gst_rec_sliders_new (gst_rec_get_video_source (win->rec),
      gst_rec_get_audio_source (win->rec), TRUE);
  if (window) {
    gtk_widget_realize (window);
    gst_rec_sliders_attach (GST_REC_SLIDERS (window), widget);
    gtk_widget_show (window);
    gst_rec_sliders_run (GST_REC_SLIDERS (window));
  }
}

static void
cb_file_changed (GtkWidget * widget, gpointer data)
{
  GstRecWindow *win = GST_REC_WINDOW (data);
  GnomeVFSURI *old_uri = win->file, *new_uri = NULL;
  const gchar *filename = gtk_entry_get_text (win->fileentry);
  GnomeVFSFileSize size;
  gboolean sensitive;
  gchar *fullpath, *pwd;

  /* Note that we unref the old URI after creating the new one, because
   * that might prevent a whole tree of unrefs for each dir which will
   * be recreated right thereafter by the new URI. */
  if (filename) {
    if ((filename[0] == '/') || (!strncmp(filename, "file:", 5))) {
      /* Absolute pathname */
      new_uri = gnome_vfs_uri_new (filename);
    } else {
      /* Relative pathname */
      pwd = g_malloc(PATH_MAX);
      getcwd(pwd,PATH_MAX);
      fullpath = g_strdup_printf ("file://%s/%s", pwd, filename);
      new_uri = gnome_vfs_uri_new (fullpath);
      g_free(pwd);
      g_free(fullpath);
    }
  }
  GST_DEBUG ("Filename changed to %s", filename);
  g_object_set (G_OBJECT (win->output), "uri", new_uri, NULL);
  if (old_uri)
    gnome_vfs_uri_unref (old_uri);
  win->file = new_uri;

  /* update free space counter */
  size = get_free_space (win);
  if (size != (GnomeVFSFileSize) - 1)
    gst_rec_stats_file (win->stats, size);
  else
    gst_rec_stats_reset (win->stats);

  /* update play buttons */
  sensitive = (win->file != NULL && gnome_vfs_uri_exists (win->file));
  gtk_widget_set_sensitive (toolbar[2].widget, sensitive);
  gtk_widget_set_sensitive (actions_menu[2].widget, sensitive);
}

static void
cb_file_browse (GtkWidget * widget, gpointer data)
{
  GstRecWindow *win = GST_REC_WINDOW (data);
  static GtkWidget *filedialog = NULL;
  
  if (filedialog != NULL)
    gtk_window_present (GTK_WINDOW (filedialog));
  else {
    filedialog = gtk_file_chooser_dialog_new ("Cupid: Select target file",
        GTK_WINDOW (data), GTK_FILE_CHOOSER_ACTION_SAVE,
        GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
        GTK_STOCK_SAVE, GTK_RESPONSE_OK,
        NULL);
    g_signal_connect (G_OBJECT (filedialog), "response",
        G_CALLBACK (cb_file_browse_response), win);
    g_signal_connect (G_OBJECT (filedialog), "destroy",
        G_CALLBACK (cb_file_browse_destroy), &filedialog);
    gtk_widget_show (filedialog);
  }
}

static void
cb_file_browse_response (GtkWidget * widget, gint response_id, gpointer data)
{
  GstRecWindow *win = GST_REC_WINDOW (data);
  
  switch (response_id) {
    case GTK_RESPONSE_OK:
      gtk_entry_set_text (win->fileentry, gtk_file_chooser_get_filename (
          GTK_FILE_CHOOSER (widget)));
      /* fall-through */
    case GTK_RESPONSE_CANCEL:
      gtk_widget_destroy (widget);
      break;
  }
}

static void
cb_file_browse_destroy (GtkWidget * widget, gpointer data)
{
  *(GtkWidget **) data = NULL;
}

typedef struct _GstRecStateChangeHandler {
  GstRecWindow *win;
  gint transition;
} GstRecStateChangeHandler;

static gboolean
cb_state_change_real (gpointer data)
{
  GstRecStateChangeHandler *handler = data;
  GstRecWindow *win = GST_REC_WINDOW (handler->win);

  switch (handler->transition) {
    case GST_STATE_NULL_TO_READY:
      gtk_widget_set_sensitive (win->filebox, FALSE);
      break;
    case GST_STATE_READY_TO_NULL:
      gtk_widget_set_sensitive (win->filebox, TRUE);
      break;
    case GST_STATE_PAUSED_TO_PLAYING:
      gtk_widget_hide (toolbar[1].widget);
      gtk_widget_show (toolbar[0].widget);
      gtk_widget_hide (actions_menu[1].widget);
      gtk_widget_show (actions_menu[0].widget);
      break;
    case GST_STATE_PLAYING_TO_PAUSED:
      gtk_widget_hide (toolbar[0].widget);
      gtk_widget_show (toolbar[1].widget);
      gtk_widget_hide (actions_menu[0].widget);
      gtk_widget_show (actions_menu[1].widget);
      break;
    default:
      break;
  }

  g_free (handler);

  return FALSE;
}

static void
cb_state_change (GstElement * element,
    GstElementState old_state, GstElementState new_state, gpointer data)
{
  GstRecStateChangeHandler *handler = g_new (GstRecStateChangeHandler, 1);

  handler->win = data;
  handler->transition = (old_state << 8 | new_state);

  g_idle_add ((GSourceFunc) cb_state_change_real, handler);
}

/*
 * GConf callbacks.
 */

static void
cb_toggle_widget (GConfClient * client,
    guint connection_id, GConfEntry * entry, gpointer data)
{
  GstRecWindow *win = GST_REC_WINDOW (data);
  GnomeUIInfo *menu = NULL;
  gboolean value;

  /* booleans only here */
  g_return_if_fail (entry->value->type == GCONF_VALUE_BOOL);
  value = gconf_value_get_bool (entry->value);

  if (!strcmp (entry->key, GST_REC_KEY_MINIMAL_MODE)) {
    GtkWidget *toolbar, *status;

    toolbar = GTK_WIDGET (gnome_app_get_dock_item_by_name (GNOME_APP (win),
        GNOME_APP_TOOLBAR_NAME));
    status = GNOME_APP (win)->statusbar;

    if (!value) {
      gtk_widget_show (toolbar);
      gtk_widget_show (win->box);
      gtk_widget_show (status);
    } else {
      gtk_widget_hide (toolbar);
      gtk_widget_hide (win->box);
      gtk_widget_hide (status);
    }

    menu = &view_menu[0];
  } else if (!strcmp (entry->key, GST_REC_KEY_STATISTICS_VISIBILITY)) {
    gst_rec_stats_expand (win->stats, value);

    menu = &view_menu[1];
  } else {
    GST_LOG ("Unknown UI key %s", entry->key);
  }

  if (menu) {
    gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (menu->widget), value);
  }
}

static gboolean
cb_refresh_video (gpointer data)
{
  GstRec *rec = GST_REC (data);

  g_signal_emit_by_name (rec, "video_source_changed",
      rec->video_source);

  return FALSE;
}

static void
cb_toggle_element (GConfClient * client,
    guint connection_id, GConfEntry * entry, gpointer data)
{
  GstRecWindow *win = GST_REC_WINDOW (data);

  GST_LOG ("Setting key %s", entry->key);

  if (!strcmp (entry->key, GST_REC_KEY_VIDEO_SOURCE) ||
      !strcmp (entry->key, GST_REC_KEY_AUDIO_SOURCE) ||
      !strcmp (entry->key, GST_REC_KEY_VIDEO_ENCODER) ||
      !strcmp (entry->key, GST_REC_KEY_AUDIO_ENCODER) ||
      !strcmp (entry->key, GST_REC_KEY_VIDEO_FILTER) ||
      !strcmp (entry->key, GST_REC_KEY_AUDIO_FILTER) ||
      !strcmp (entry->key, GST_REC_KEY_VIDEO_NORM) ||
      !strcmp (entry->key, GST_REC_KEY_VIDEO_INPUT) ||
      !strcmp (entry->key, GST_REC_KEY_AUDIO_INPUT) ||
      !strcmp (entry->key, GST_REC_KEY_MUXER)) {
    const gchar *value;

    /* strings only here */
    g_return_if_fail (entry->value->type == GCONF_VALUE_STRING);
    value = gconf_value_get_string (entry->value);

    if (!strcmp (entry->key, GST_REC_KEY_VIDEO_SOURCE)) {
      gst_rec_set_video_source (win->rec,
          gst_rec_config_to_video_source (value));
    } else if (!strcmp (entry->key, GST_REC_KEY_AUDIO_SOURCE)) {
      gst_rec_set_audio_source (win->rec,
          gst_rec_config_to_audio_source (value));
    } else if (!strcmp (entry->key, GST_REC_KEY_VIDEO_ENCODER)) {
      gst_rec_set_video_encoder (win->rec,
          gst_rec_config_to_video_encoder (value));
    } else if (!strcmp (entry->key, GST_REC_KEY_AUDIO_ENCODER)) {
      gst_rec_set_audio_encoder (win->rec,
          gst_rec_config_to_audio_encoder (value));
    } else if (!strcmp (entry->key, GST_REC_KEY_MUXER)) {
      gst_rec_set_muxer (win->rec, gst_rec_config_to_muxer (value));
    } else if (!strcmp (entry->key, GST_REC_KEY_VIDEO_FILTER)) {
      gst_rec_set_video_filter (win->rec,
          gst_rec_config_to_video_filter (value));
    } else if (!strcmp (entry->key, GST_REC_KEY_AUDIO_FILTER)) {
      gst_rec_set_audio_filter (win->rec,
          gst_rec_config_to_audio_filter (value));
    } else if (!strcmp (entry->key, GST_REC_KEY_VIDEO_NORM)) {
      const GList *norms;
      GstTuner *tuner;
      GstElement *video_source;
      GstTunerNorm *norm = NULL;

      video_source = gst_rec_get_video_source (win->rec);
      if (!video_source || !GST_IS_TUNER (video_source))
        return;

      tuner = GST_TUNER (video_source);
      for (norms = gst_tuner_list_norms (tuner);
           norms != NULL; norms = norms->next) {
        if (!strcasecmp (GST_TUNER_NORM (norms->data)->label, value)) {
          norm = norms->data;
          break;
        }
      }
      g_return_if_fail (norm != NULL);

      gst_tuner_set_norm (tuner, norm);
    } else if (!strcmp (entry->key, GST_REC_KEY_VIDEO_INPUT)) {
      const GList *inputs;
      GstTuner *tuner;
      GstElement *video_source;
      GstTunerChannel *input = NULL;

      video_source = gst_rec_get_video_source (win->rec);
      if (!video_source || !GST_IS_TUNER (video_source))
        return;

      tuner = GST_TUNER (video_source);
      for (inputs = gst_tuner_list_channels (tuner);
           inputs != NULL; inputs = inputs->next) {
        if (!strcasecmp (GST_TUNER_CHANNEL (inputs->data)->label, value) &&
            GST_TUNER_CHANNEL_HAS_FLAG (GST_TUNER_CHANNEL (inputs->data),
                GST_TUNER_CHANNEL_INPUT)) {
          input = inputs->data;
          break;
        }
      }
      g_return_if_fail (input != NULL);

      gst_tuner_set_channel (tuner, input);
    } else if (!strcmp (entry->key, GST_REC_KEY_AUDIO_INPUT)) {
      const GList *tracks;
      GstMixer *mixer;
      GstMixerTrack *track;
      GstElement *audio_source;

      audio_source = gst_rec_get_audio_source (win->rec);
      if (!audio_source || !GST_IS_MIXER (audio_source))
        return;

      mixer = GST_MIXER (audio_source);
      for (tracks = gst_mixer_list_tracks (mixer);
           tracks != NULL; tracks = tracks->next) {
        gboolean match;

        track = tracks->data;
        match = !strcasecmp (track->label, value);

        if (match) {
          gst_mixer_set_mute (mixer, track, FALSE);
        }
        gst_mixer_set_record (mixer, track, match);
      }
    }
  } else if (!strcmp (entry->key, GST_REC_KEY_VIDEO_WIDTH) ||
      !strcmp (entry->key, GST_REC_KEY_VIDEO_HEIGHT) ||
      !strcmp (entry->key, GST_REC_KEY_AUDIO_SAMPLERATE) ||
      !strcmp (entry->key, GST_REC_KEY_AUDIO_CHANNELS)) {
    int value;

    /* strings only here */
    g_return_if_fail (entry->value->type == GCONF_VALUE_INT);
    value = gconf_value_get_int (entry->value);

    if (!strcmp (entry->key, GST_REC_KEY_VIDEO_WIDTH)) {
      gst_rec_set_video_width (win->rec, value);
    } else if (!strcmp (entry->key, GST_REC_KEY_VIDEO_HEIGHT)) {
      gst_rec_set_video_height (win->rec, value);
    } else if (!strcmp (entry->key, GST_REC_KEY_AUDIO_SAMPLERATE)) {
      gst_rec_set_audio_samplerate (win->rec, value);
    } else if (!strcmp (entry->key, GST_REC_KEY_AUDIO_CHANNELS)) {
      gst_rec_set_audio_channels (win->rec, value);
    }
  } else if (!strcmp (entry->key, GST_REC_KEY_VIDEO_FRAMERATE)) {
    gdouble value;

    g_return_if_fail (entry->value->type == GCONF_VALUE_FLOAT);
    value = gconf_value_get_float (entry->value);

    gst_rec_set_video_fps (win->rec, value);
  } else if (!strncmp (entry->key, GST_REC_KEY_METADATA,
                       strlen (GST_REC_KEY_METADATA))) {
    const gchar *value;

    /* strings only here */
    g_return_if_fail (entry->value->type == GCONF_VALUE_STRING);
    g_return_if_fail (strlen (entry->key) > strlen (GST_REC_KEY_METADATA));
    value = gconf_value_get_string (entry->value);

    gst_rec_set_tag (win->rec, entry->key + strlen (GST_REC_KEY_METADATA),
                     value);
  } else if (!strcmp (entry->key, GST_REC_KEY_VIDEO_EMUMODE)) {
    g_return_if_fail (entry->value->type == GCONF_VALUE_INT);

    g_object_set (G_OBJECT (win->rec->video_source_emulator), "mode",
        gconf_value_get_int (entry->value), NULL);

    /* hack to re-load TV widget */
    g_idle_add ((GSourceFunc) cb_refresh_video, win->rec);
  } else {
    GST_LOG ("Unknown engine key %s", entry->key);
    return;
  }
}

/*
 * Load all GConf entries. We do this by re-setting each entry...
 * Also initiate the config when we haven't done this before yet.
 * Why doesn't GConf have a function for this directly?...
 */

static void
load_key (GstRecWindow * win, const gchar * key,
    const GConfValue * in_value)
{
  GConfValue *value, *fake;

  if (!in_value)
    return;
  if (!(value = gconf_value_copy (in_value)))
    return;

  if (value->type == GCONF_VALUE_BOOL) {
    fake = gconf_value_new (GCONF_VALUE_STRING);
    gconf_value_set_string (fake, "bla");
  } else {
    fake = gconf_value_new (GCONF_VALUE_BOOL);
    gconf_value_set_bool (fake, FALSE);
  }
  gconf_client_set (win->client, key, fake, NULL);
  gconf_client_set (win->client, key, value, NULL);
}

static void
load_config (GstRecWindow * win)
{
  GSList *dirs, *entries;
  const gchar *dir;
  GConfEntry *entry;
  GConfValue *value;
  GList *elements;

  /* first-time setup? */
  if (!(value = gconf_client_get (win->client, GST_REC_KEY_SETUP, NULL)) ||
      value->type != GCONF_VALUE_BOOL || !gconf_value_get_bool (value)) {
    GtkWidget *druid;

    druid = gst_rec_druid_new (win->rec, win->client);
    gtk_window_set_transient_for (GTK_WINDOW (druid),
        GTK_WINDOW (win));
    gtk_window_set_modal (GTK_WINDOW (druid), TRUE);
    gtk_widget_show (druid);

    return;
  }

  /* Now, before we load everything, we first want to do all device
   * probing. Why? Simple! If we load OSS (or any) and we add it, it
   * will be set to READY. Probing can only be done in NULL, so any
   * try to probe devices will fail. */
  elements = gst_rec_list_video_sources ();
  gst_rec_list_replace (&elements, NULL);
  elements = gst_rec_list_audio_sources ();
  gst_rec_list_replace (&elements, NULL);

  /* bleh... GConf, implement this sanely! */
  entry = gconf_client_get_entry (win->client, GST_REC_KEY_VIDEO_SOURCE,
      NULL, FALSE, NULL);
  if (entry)
    cb_toggle_element (win->client, 0, entry, win);
  entry = gconf_client_get_entry (win->client, GST_REC_KEY_AUDIO_SOURCE,
      NULL, FALSE, NULL);
  if (entry)
    cb_toggle_element (win->client, 0, entry, win);

  for (dirs = gconf_client_all_dirs (win->client, GST_REC_KEY_BASEDIR, NULL);
       dirs != NULL; dirs = dirs->next) {
    dir = dirs->data;

    for (entries = gconf_client_all_entries (win->client, dir, NULL);
         entries != NULL; entries = entries->next) {
      entry = entries->data;
      if (!entry || !entry->value || !entry->key)
        continue;
      if (strcmp (entry->key, GST_REC_KEY_VIDEO_SOURCE) != 0 &&
          strcmp (entry->key, GST_REC_KEY_AUDIO_SOURCE) != 0) {
        load_key (win, entry->key, entry->value);
      }
    }
  }
}
