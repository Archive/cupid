/* GStreamer Recorder
 * (c) 2003 Ronald Bultje <rbultje@ronald.bitfreak.net>
 *
 * druid.h: first-time setup druid definition
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GST_REC_DRUID_H__
#define __GST_REC_DRUID_H__

#include <gconf/gconf-client.h>
#include <libgnomeui/gnome-druid.h>

#include "configuration.h"

G_BEGIN_DECLS
#define GST_REC_TYPE_DRUID \
  (gst_rec_druid_get_type ())
#define GST_REC_DRUID(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GST_REC_TYPE_DRUID, GstRecDruid))
#define GST_REC_DRUID_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), GST_REC_TYPE_DRUID, GstRecDruidClass))
#define GST_REC_IS_DRUID(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GST_REC_TYPE_DRUID))
#define GST_REC_IS_DRUID_CLASS(obj) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), GST_REC_TYPE_DRUID))
    typedef struct _GstRecDruid
{
  GstRecConfiguration parent;

  /* here, a druid is the container */
  GnomeDruid *druid;
} GstRecDruid;

typedef struct _GstRecDruidClass
{
  GstRecConfigurationClass parent;
} GstRecDruidClass;

GType gst_rec_druid_get_type (void);

GtkWidget *gst_rec_druid_new (GstRec * rec, GConfClient * client);

G_END_DECLS
#endif /* __GST_REC_DRUID_H__ */
