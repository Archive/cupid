/* GStreamer Recorder
 * (c) 2003 Ronald Bultje <rbultje@ronald.bitfreak.net>
 *
 * stats.h: statistics widget definition
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gtk/gtktable.h>
#include <gst/gst.h>
#include <libgnomevfs/gnome-vfs.h>

#ifndef __GST_REC_STATS_H__
#define __GST_REC_STATS_H__

#define GST_REC_TYPE_STATS \
  (gst_rec_stats_get_type())
#define GST_REC_STATS(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_REC_TYPE_STATS, GstRecStats))
#define GST_REC_STATS_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass), GST_REC_TYPE_STATS, GstRecStatsClass))
#define GST_REC_IS_STATS(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_REC_TYPE_STATS))
#define GST_REC_IS_STATS_CLASS(obj) \
  (G_TYPE_CHECK_CLASS_TYPE((klass), GST_REC_TYPE_STATS))

typedef struct _GstRecStatsItem
{
  GtkWidget *label, *value;
} GstRecStatsItem;

typedef struct _GstRecStats
{
  GtkTable parent;

  GstRecStatsItem *items;
  guint num_items;
} GstRecStats;

typedef struct _GstRecStatsClass
{
  GtkTableClass parent;
} GstRecStatsClass;

GType gst_rec_stats_get_type (void);

GtkWidget *gst_rec_stats_new (gboolean expand);
void gst_rec_stats_expand (GstRecStats * stats, gboolean expand);
void gst_rec_stats_set (GstRecStats * stats,
    GstClockTime time,
    guint64 frames,
    guint64 samples,
    guint lost, guint inserted, guint deleted, GnomeVFSFileSize size);
void gst_rec_stats_file (GstRecStats * stats, GnomeVFSFileSize size);
void gst_rec_stats_reset (GstRecStats * stats);

#endif /* __GST_REC_STATS_H__ */
