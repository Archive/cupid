/* GStreamer Recorder
 * (c) 2005 Ronald Bultje <rbultje@ronald.bitfreak.net>
 *
 * number.h: collector widget for range, list and fixed numbers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gtk/gtk.h>

#ifndef __GST_REC_NUMBER_H__
#define __GST_REC_NUMBER_H__

#define GST_REC_TYPE_NUMBER \
  (gst_rec_number_get_type())
#define GST_REC_NUMBER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_REC_TYPE_NUMBER, \
			      GstRecNumber))
#define GST_REC_NUMBER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass), GST_REC_TYPE_NUMBER, \
			   GstRecNumberClass))
#define GST_REC_IS_NUMBER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_REC_TYPE_NUMBER))
#define GST_REC_IS_NUMBER_CLASS(obj) \
  (G_TYPE_CHECK_CLASS_TYPE((klass), GST_REC_TYPE_NUMBER))

typedef enum {
  GST_REC_NUMBER_UNKNOWN,
  GST_REC_NUMBER_FIXED,
  GST_REC_NUMBER_LIST,
  GST_REC_NUMBER_RANGE
} GstRecNumberType;

typedef struct _GstRecNumber
{
  GtkHBox parent;

  /* we can be either a fixed number (in which case a label is enough),
   * a range (spinbutton) or a list (combobox). At any time, only one
   * of those will be shown. Type is the current active one. */
  GtkComboBox *combo;
  GtkLabel *label;
  GtkSpinButton *spin;
  GstRecNumberType type;

  /* This is the absolute min/max. The min/max in the widgets can be
   * toggled at run-time, though. */
  gdouble min, max, value, step;
  guint digits;
} GstRecNumber;

typedef struct _GstRecNumberClass
{
  GtkHBoxClass parent;

  /* is emitted if the active widget changed value */
  void (* value_changed) (GtkWidget *widget);
} GstRecNumberClass;

GType gst_rec_number_get_type (void);

/*
 * Create new number widget. Set a value later on.
 */
GtkWidget *gst_rec_number_new (gdouble value, gdouble min,
    gdouble max, gdouble step, guint digits);

/*
 * Set a specific range. The given value can be of any of the
 * following types: int, int range, int list. float, float range,
 * float list.
 */
void gst_rec_number_set_range (GstRecNumber * number, const GValue * range);

/*
 * Get/set value.
 */
gdouble gst_rec_number_get_value (GstRecNumber * number);
void gst_rec_number_set_value (GstRecNumber * number, gdouble value);

#endif /* __GST_REC_NUMBER_H__ */
