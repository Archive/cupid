/* GStreamer Recorder
 * (c) 2003 Ronald Bultje <rbultje@ronald.bitfreak.net>
 *
 * preferences.c: preferences dialog
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <string.h>
#include <glib/gi18n.h>

#include "debug.h"
#include "keys.h"
#include "list.h"
#include "parameter.h"
#include "preferences.h"
#include "record.h"

static void gst_rec_preferences_class_init (GstRecPreferencesClass * klass);
static void gst_rec_preferences_init (GstRecPreferences * prefs);
static void gst_rec_preferences_response (GtkDialog * dialog, gint response_id);
static GtkWidget *gst_rec_preferences_paramview (GstElement * element);

static GstRecConfigurationClass *parent_class = NULL;

GType
gst_rec_preferences_get_type (void)
{
  static GType gst_rec_preferences_type = 0;

  if (!gst_rec_preferences_type) {
    static const GTypeInfo gst_rec_preferences_info = {
      sizeof (GstRecPreferencesClass),
      NULL,
      NULL,
      (GClassInitFunc) gst_rec_preferences_class_init,
      NULL,
      NULL,
      sizeof (GstRecPreferences),
      0,
      (GInstanceInitFunc) gst_rec_preferences_init,
      NULL
    };

    gst_rec_preferences_type =
        g_type_register_static (GST_REC_TYPE_CONFIGURATION,
        "GstRecPreferences", &gst_rec_preferences_info, 0);
  }

  return gst_rec_preferences_type;
}

static void
gst_rec_preferences_class_init (GstRecPreferencesClass * klass)
{
  GtkDialogClass *gtkdialog_class = (GtkDialogClass *) klass;

  parent_class = g_type_class_ref (GST_REC_TYPE_CONFIGURATION);

  gtkdialog_class->response = gst_rec_preferences_response;
}

static void
gst_rec_preferences_init (GstRecPreferences * prefs)
{
  GtkWidget *notebook;

  /* make window look cute */
  gtk_window_set_title (GTK_WINDOW (prefs), _("Cupid: Preferences"));
  gtk_dialog_add_buttons (GTK_DIALOG (prefs),
      GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE,
      /* help goes here (future) */
      NULL);

  /* give window some content */
  notebook = gtk_notebook_new ();
  gtk_container_set_border_width (GTK_CONTAINER (notebook), 6);
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (prefs)->vbox),
      notebook, TRUE, TRUE, 0);
  gtk_widget_show (notebook);
  prefs->notebook = GTK_NOTEBOOK (notebook);
}

static void
gst_rec_preferences_response (GtkDialog * dialog, gint response_id)
{
  switch (response_id) {
    case GTK_RESPONSE_CLOSE:
      gtk_widget_destroy (GTK_WIDGET (dialog));
      break;

    default:
      break;
  }

  if (((GtkDialogClass *) parent_class)->response)
    ((GtkDialogClass *) parent_class)->response (dialog, response_id);
}

/*
 * Callback that disables/enables widgets during state changes.
 * Goal is to prevent the user from changing settings while
 * recording is active (PAUSED/PLAYING); horrible things will
 * happen.
 */

static void
cb_state_change (GstElement * element,
    GstElementState old_state, GstElementState new_state, gpointer data)
{
  GtkWidget *view = GTK_WIDGET (data);

  if (old_state != GST_STATE_NULL && new_state == GST_STATE_NULL)
    gtk_widget_set_sensitive (view, TRUE);
  else if (old_state == GST_STATE_NULL && new_state != GST_STATE_NULL)
    gtk_widget_set_sensitive (view, FALSE);
}

/*
 * Element change.
 */

static void
cb_element_change (GstRec * rec, GstElement * element, gpointer data)
{
  GtkContainer *container = GTK_CONTAINER (data);
  GtkWidget *view;

  /* get rid of old */
  gtk_container_remove (container, gtk_bin_get_child (GTK_BIN (container)));

  /* create new */
  view = gst_rec_preferences_paramview (element);
  gtk_container_add (container, view);
  gtk_widget_show (view);
}

/*
 * Signal callback if we close the element details subwindow.
 */

static void
cb_response (GtkDialog * dialog, guint response_id, gpointer data)
{
  //GstRecPreferences *prefs = GST_REC_PREFERENCES (data);

  switch (response_id) {
    case GTK_RESPONSE_CLOSE:
      gtk_widget_destroy (GTK_WIDGET (dialog));
      break;

    default:
      break;
  }
}

/*
 * Destroy, clean up signal IDs.
 */

#define g_signal_handlers_disconnect_by_func_only(instance, func) \
  g_signal_handlers_disconnect_matched ((instance), \
					(GSignalMatchType) G_SIGNAL_MATCH_FUNC, \
					0, 0, NULL, (func), NULL)

static void
cb_destroy (GtkWidget * widget, gpointer data)
{
  GstRecPreferences *prefs = data;
  GstRec *rec = GST_REC_CONFIGURATION (prefs)->rec;

  g_signal_handlers_disconnect_by_func_only (rec, cb_element_change);
  g_signal_handlers_disconnect_by_func_only (rec, cb_state_change);
}

/*
 * Signal handler data.
 */

typedef struct _GstRecPreferencesElementDetailsData
{
  GstRecPreferences *prefs;
  GstElement *(*get_element) (GstRec * rec);
  gchar *element_change_signal_name;
}
GstRecPreferencesElementDetailsData;

/*
 * Signal callback if 'details...' button has been clicked.
 */

static void
cb_button_click (GtkButton * button, gpointer data)
{
  GstRecPreferencesElementDetailsData *details = data;
  GstRecPreferences *prefs = details->prefs;
  GstRec *rec = GST_REC_CONFIGURATION (prefs)->rec;
  GstElement *element = details->get_element (rec);
  GtkWidget *dialog, *scroll, *view, *port;
  GtkAdjustment *hadj, *vadj;

  dialog = gtk_dialog_new_with_buttons (_("Cupid: Element details"),
      GTK_WINDOW (prefs),
      GTK_DIALOG_DESTROY_WITH_PARENT,
      GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE, NULL);

  /* a scrolled window inside it, where all the properties will reside */
  scroll = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scroll),
      GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
  hadj = gtk_scrolled_window_get_hadjustment (GTK_SCROLLED_WINDOW (scroll));
  vadj = gtk_scrolled_window_get_vadjustment (GTK_SCROLLED_WINDOW (scroll));
  port = gtk_viewport_new (hadj, vadj);
  gtk_viewport_set_shadow_type (GTK_VIEWPORT (port), GTK_SHADOW_NONE);
  gtk_container_add (GTK_CONTAINER (scroll), port);
  gtk_widget_show (port);
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), scroll,
      TRUE, TRUE, 0);
  gtk_widget_show (scroll);

  /* and inside this are the properties */
  view = gst_rec_preferences_paramview (element);
  gtk_table_set_col_spacings (GTK_TABLE (view), 6);
  gtk_table_set_row_spacings (GTK_TABLE (view), 2);
  gtk_container_add (GTK_CONTAINER (port), view);
  if (GST_STATE (rec) != GST_STATE_NULL)
    gtk_widget_set_sensitive (view, FALSE);
  g_signal_connect (rec, "state-change", G_CALLBACK (cb_state_change), view);
  g_signal_connect (rec, details->element_change_signal_name,
      G_CALLBACK (cb_element_change), port);
  gtk_widget_show (view);

  g_signal_connect (dialog, "response", G_CALLBACK (cb_response), prefs);
  g_signal_connect (dialog, "destroy", G_CALLBACK (cb_destroy), prefs);

  gtk_widget_show (dialog);
}

/*
 * Create new.
 */

GtkWidget *
gst_rec_preferences_new (GstRec * rec, GConfClient * client)
{
  GstRecPreferences *prefs = g_object_new (GST_REC_TYPE_PREFERENCES, NULL);
  gint i, n = 0;
  struct
  {
    char *label;
    GtkWidget *(*get_page) (GstRecConfiguration * conf);
    GstElement *(*get_element) (GstRec * rec);
    gchar *element_change_signal;
    //GstRecPrefProps *cont;
  }
  items[] =
  {
    {
    _("Metadata"), gst_rec_configuration_get_metadata_page, NULL, NULL}
    ,{
    _("Output format"), gst_rec_configuration_get_output_format_page,
          NULL, NULL}
    , {
    _("Audio encoder"), NULL,
          gst_rec_get_audio_encoder, "audio_encoder_changed"}
    , {
    _("Video encoder"), NULL,
          gst_rec_get_video_encoder, "video_encoder_changed"}
    , {
    _("File format"), NULL,
          gst_rec_get_muxer, "muxer_changed"}
    , {
    _("Audio source"), gst_rec_configuration_get_audio_source_page,
          gst_rec_get_audio_source, "audio_source_changed"}
    , {
    _("Video source"), gst_rec_configuration_get_video_source_page,
          gst_rec_get_video_source, "video_source_changed"}
    , {
    NULL, NULL, NULL, NULL}
  };
  /* FIXME: we leak this... */
  GstRecPreferencesElementDetailsData *details =
      g_new (GstRecPreferencesElementDetailsData, 5);

  /* backend setting */
  gst_rec_configuration_set_backend (GST_REC_CONFIGURATION (prefs),
      rec, client);

  /* pages */
  for (i = 0; items[i].label != NULL; i++) {
    GtkWidget *label, *page, *vbox, *hbox, *button, *separator;

    label = gtk_label_new (items[i].label);
    gtk_misc_set_alignment (GTK_MISC (label), 0.0, GTK_MISC (label)->yalign);
    gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);

    vbox = gtk_vbox_new (FALSE, 6);
    gtk_container_set_border_width (GTK_CONTAINER (vbox), 6);

    page = items[i].get_page (GST_REC_CONFIGURATION (prefs));
    gtk_box_pack_start (GTK_BOX (vbox), page, FALSE, FALSE, 0);
    gtk_widget_show (page);

    gtk_table_set_col_spacings (GTK_TABLE (page), 6);
    gtk_table_set_row_spacings (GTK_TABLE (page), 2);

    if (!items[i].get_element && items[i + 1].get_page)
      goto next;

    separator = gtk_hseparator_new ();
    gtk_box_pack_start (GTK_BOX (vbox), separator, FALSE, FALSE, 0);
    gtk_widget_show (separator);

    /* button to get to the properties */
    hbox = gtk_hbox_new (TRUE, 0);

    do {
      gchar *d_str;

      if (!items[i].get_page || !items[i].get_element)
        i++;

      details[n].prefs = prefs;
      details[n].get_element = items[i].get_element;
      details[n].element_change_signal_name = items[i].element_change_signal;

      /* Translators note: the %s is filled in by the type of element
       * that these details are for, e.g. video source, audio encoder. */
      d_str = g_strdup_printf (_("%s details"), items[i].label);
      button = gtk_button_new_with_label (d_str);
      g_free (d_str);
      g_signal_connect (button, "clicked", G_CALLBACK (cb_button_click),
          &details[n]);
      gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, FALSE, 0);
      gtk_widget_show (button);

      n++;
    } while (!items[i+1].get_page && items[i+1].label);

    gtk_widget_show (hbox);
    gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);

  next:
    gtk_notebook_prepend_page (prefs->notebook, vbox, label);
    gtk_widget_show (vbox);
    gtk_widget_show (label);
  }

  return GTK_WIDGET (prefs);
}

static GtkWidget *
gst_rec_preferences_paramview (GstElement * element)
{
  guint num = 0, n;
  GParamSpec **pspecs = NULL, *pspec;
  GList *pspeclist = NULL, *one;
  GtkWidget *table, *label, *param;

  /* get parameters */
  if (element) {
    pspecs = g_object_class_list_properties (G_OBJECT_GET_CLASS (element),
        &num);
  }

  /* get rid of the general ones that we handle elsewhere (name etc.) */
  for (n = 0; n < num; n++) {
    pspec = pspecs[n];

    if (!strcmp (pspec->name, "name") ||
        !strcmp (pspec->name, "device") ||
        !strcmp (pspec->name, "mixerdev") ||
        !strcmp (pspec->name, "device-name") ||
        !strcmp (pspec->name, "width") ||
        !strcmp (pspec->name, "height") ||
        !strcmp (pspec->name, "fourcc") ||
        !strcmp (pspec->name, "samplerate") ||
        !strcmp (pspec->name, "sync") ||
        !strcmp (pspec->name, "sync-mode") ||
        !strcmp (pspec->name, "autoprobe") ||
        !strcmp (pspec->name, "autoprobe-fps") ||
        !strcmp (pspec->name, "latency-offset") ||
        !strcmp (pspec->name, "copy-mode") ||
        !strcmp (pspec->name, "use-fixed-fps") ||
        !strcmp (pspec->name, "num-buffers"))
      continue;

    pspeclist = g_list_append (pspeclist, pspec);
  }

  num = g_list_length (pspeclist);
  if (num == 0) {
    table = gtk_table_new (1, 1, FALSE);
    label = gtk_label_new (_("Element has no parameters"));
    gtk_misc_set_alignment (GTK_MISC (label), 0.0, GTK_MISC (label)->yalign);
    gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
    gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 0, 1);
    gtk_widget_show (label);
    return table;
  }

  table = gtk_table_new (num, 2, FALSE);
  gtk_table_set_col_spacings (GTK_TABLE (table), 6);
  gtk_table_set_row_spacings (GTK_TABLE (table), 2);

  for (one = pspeclist, n = 0; one != NULL; one = one->next, n++) {
    pspec = one->data;

    label = gtk_label_new (pspec->_nick);
    gtk_misc_set_alignment (GTK_MISC (label), 0.0, GTK_MISC (label)->yalign);
    gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
    gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, n, n + 1);
    if (!(pspec->flags & G_PARAM_WRITABLE))
      gtk_widget_set_sensitive (label, FALSE);
    gtk_widget_show (label);

    param = gst_rec_parameter_new (element, pspec);
    gtk_table_attach_defaults (GTK_TABLE (table), param, 1, 2, n, n + 1);
    gtk_widget_show (param);
  }

  g_list_free (pspeclist);

  return table;
}
