/* GStreamer Recorder
 * (c) 2003 Ronald Bultje <rbultje@ronald.bitfreak.net>
 *
 * sliders.c: floating slider window for audio volume and colorbalance
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#define _GNU_SOURCE             /* strcasestr */
#include <string.h>
#include <gst/gst.h>
#include <glib/gi18n.h>
#include <gnome.h>
#include <gst/colorbalance/colorbalance.h>
#include <gst/mixer/mixer.h>

#include "debug.h"
#include "sliders.h"
#include "stock.h"

typedef struct _GstRecSlidersColorBalanceChannel
{
  GstColorBalance *balance;
  GstColorBalanceChannel *channel;
} GstRecSlidersColorBalanceChannel;

typedef struct _GstRecSliderMixerTrack
{
  GstMixer *mixer;
  GstMixerTrack *track;
  gint *volumes;
} GstRecSlidersMixerTrack;

static void gst_rec_sliders_class_init (GstRecSlidersClass * klass);
static void gst_rec_sliders_dispose (GObject * obj);
static void gst_rec_sliders_init (GstRecSliders * sliders);

static gboolean gst_rec_sliders_mouse (GtkWidget * widget,
    GdkEventButton * event);
static gboolean gst_rec_sliders_keyboard (GtkWidget * widget,
    GdkEventKey * event);

static GtkWindowClass *parent_class = NULL;

GType
gst_rec_sliders_get_type (void)
{
  static GType gst_rec_sliders_type = 0;

  if (!gst_rec_sliders_type) {
    static const GTypeInfo gst_rec_sliders_info = {
      sizeof (GstRecSlidersClass),
      NULL,
      NULL,
      (GClassInitFunc) gst_rec_sliders_class_init,
      NULL,
      NULL,
      sizeof (GstRecSliders),
      0,
      (GInstanceInitFunc) gst_rec_sliders_init,
      NULL
    };

    gst_rec_sliders_type =
        g_type_register_static (GTK_TYPE_WINDOW,
        "GstRecSliders", &gst_rec_sliders_info, 0);
  }

  return gst_rec_sliders_type;
}

static void
gst_rec_sliders_class_init (GstRecSlidersClass * klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *gtkwidget_class = GTK_WIDGET_CLASS (klass);

  parent_class = g_type_class_ref (GTK_TYPE_WINDOW);

  gobject_class->dispose = gst_rec_sliders_dispose;
  gtkwidget_class->button_press_event = gst_rec_sliders_mouse;
  gtkwidget_class->key_press_event = gst_rec_sliders_keyboard;
}

static void
gst_rec_sliders_init (GstRecSliders * sliders)
{
  sliders->colorbalance = NULL;
  sliders->audiovolume = NULL;

  sliders->volumetracks = NULL;
  sliders->colorbalancechannels = NULL;

  sliders->floating = FALSE;
}

static void
gst_rec_sliders_dispose (GObject * obj)
{
  GstRecSliders *sliders = GST_REC_SLIDERS (obj);
  GList *item;

  gst_object_replace ((GstObject **) & sliders->colorbalance, NULL);
  gst_object_replace ((GstObject **) & sliders->audiovolume, NULL);

  for (item = sliders->volumetracks; item != NULL; item = item->next) {
    GstRecSlidersMixerTrack *track = item->data;

    g_object_unref (G_OBJECT (track->track));
    g_free (track->volumes);
    g_free (track);
  }
  g_list_free (sliders->volumetracks);
  sliders->volumetracks = NULL;

  for (item = sliders->colorbalancechannels; item != NULL; item = item->next) {
    GstRecSlidersColorBalanceChannel *channel = item->data;

    g_object_unref (G_OBJECT (channel->channel));
    g_free (channel);
  }
  g_list_free (sliders->colorbalancechannels);
  sliders->colorbalancechannels = NULL;

  if (((GObjectClass *) parent_class)->dispose)
    ((GObjectClass *) parent_class)->dispose (obj);
}

/*
 * Callback if volume slider is moved.
 */

static void
cb_volume_changed (GtkRange * range, gpointer data)
{
  GstRecSlidersMixerTrack *track = data;
  guint value = gtk_range_get_value (range), i;

  /* change volume */
  for (i = 0; i < track->track->num_channels; i++)
    track->volumes[i] = value;
  gst_mixer_set_volume (track->mixer, track->track, track->volumes);

  GST_DEBUG ("Changed audio volume of mixer track %s to %d",
      track->track->label, value);
}

/*
 * Callback if colorbalance value is changed.
 */

static void
cb_color_balance_changed (GtkRange * range, gpointer data)
{
  GstRecSlidersColorBalanceChannel *channel = data;
  guint value = gtk_range_get_value (range);

  gst_color_balance_set_value (channel->balance, channel->channel, value);

  GST_DEBUG ("Changed value of color balance %s to %d",
      channel->channel->label, value);
}

static const gchar *
get_icon_for_label (const gchar * name)
{
  if (!strcasecmp (name, "brightness"))
    return GST_REC_STOCK_BRIGHTNESS;
  else if (!strcasecmp (name, "contrast"))
    return GST_REC_STOCK_CONTRAST;
  else if (!strcasecmp (name, "saturation"))
    return GST_REC_STOCK_SATURATION;
  /* gnome stock for audio */
  else if (strcasestr (name, "line"))
    return GNOME_STOCK_LINE_IN;
  else if (strcasestr (name, "microphone"))
    return GNOME_STOCK_MIC;
  else if (strcasestr (name, "cd"))
    return GTK_STOCK_CDROM;

  return NULL;
}

/*
 * Creates the floating window.
 */

GtkWidget *
gst_rec_sliders_new (GstElement * colorbalance,
    GstElement * audiovolume, gboolean floating)
{
  GtkTooltips *tool;
  GstRecSliders *sliders;
  GstColorBalance *balance = NULL;
  GstMixer *mixer = NULL;
  guint num_sliders = 0, x = 0;
  const GList *colors = NULL, *item;
  GstMixerTrack *track = NULL;
  GtkWidget *table, *slider, *frame, *icon;
  gboolean have_video_sliders = FALSE, have_audio_sliders = FALSE;

  /* start by counting the number of sliders */
  if (colorbalance && GST_IS_COLOR_BALANCE (colorbalance)) {
    balance = GST_COLOR_BALANCE (colorbalance);
    colors = gst_color_balance_list_channels (balance);
    num_sliders += g_list_length ((GList *) colors);
    have_video_sliders = TRUE;
  }

  if (audiovolume && GST_IS_MIXER (audiovolume)) {
    const GList *tracks;

    mixer = GST_MIXER (audiovolume);
    tracks = gst_mixer_list_tracks (mixer);
    for (item = tracks; item != NULL; item = item->next) {
      GstMixerTrack *t_track = GST_MIXER_TRACK (item->data);

      if (GST_MIXER_TRACK_HAS_FLAG (t_track, GST_MIXER_TRACK_RECORD)) {
        track = t_track;
        break;
      }
    }
    if (track) {
      num_sliders++;
      have_audio_sliders = TRUE;
    }
  }

  if (have_video_sliders && have_audio_sliders)
    num_sliders++;

  if (num_sliders == 0)
    return NULL;

  sliders = g_object_new (GST_REC_TYPE_SLIDERS, NULL);
  gst_object_replace ((GstObject **) & sliders->colorbalance,
      GST_OBJECT (colorbalance));
  gst_object_replace ((GstObject **) & sliders->audiovolume,
      GST_OBJECT (audiovolume));

  /* now fill in the values */
  tool = gtk_tooltips_new ();
  frame = gtk_frame_new (NULL);
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_OUT);

  table = gtk_table_new (2, num_sliders, FALSE);
  gtk_table_set_col_spacings (GTK_TABLE (table), 6);
  gtk_table_set_row_spacings (GTK_TABLE (table), 6);
  gtk_container_set_border_width (GTK_CONTAINER (table), 6);
  if (track) {
    GstRecSlidersMixerTrack *strack = g_new (GstRecSlidersMixerTrack, 1);
    gint volume = 0, i;
    gchar *label;
    const gchar *sicon;

    strack->volumes = g_new (gint, track->num_channels);
    g_object_ref (G_OBJECT (track));
    strack->track = track;
    strack->mixer = mixer;

    if ((sicon = get_icon_for_label (track->label))) {
      icon = gtk_image_new_from_stock (sicon, GTK_ICON_SIZE_MENU);
      gtk_table_attach (GTK_TABLE (table), icon, x, x + 1, 0, 1,
          GTK_EXPAND, 0, 0, 0);
    }

    slider = gtk_vscale_new_with_range (track->min_volume,
        track->max_volume, 1);
    /* Translator comment: the %s is filled in by the name of the track,
     * e.g. 'line-in' or 'microphone'. */
    label = g_strdup_printf (_("%s volume"), track->label);
    gtk_tooltips_set_tip (tool, slider, label, NULL);
    g_free (label);
    gtk_scale_set_draw_value (GTK_SCALE (slider), FALSE);
    gtk_range_set_inverted (GTK_RANGE (slider), TRUE);
    gst_mixer_get_volume (mixer, track, strack->volumes);
    for (i = 0; i < track->num_channels; i++)
      volume += strack->volumes[i];
    volume /= track->num_channels;
    gtk_range_set_value (GTK_RANGE (slider), volume);

    g_signal_connect (G_OBJECT (slider), "value_changed",
        G_CALLBACK (cb_volume_changed), strack);

    gtk_table_attach (GTK_TABLE (table), slider, x, x + 1, 1, 2,
        GTK_EXPAND, GTK_EXPAND | GTK_FILL, 0, 0);
    x++;
    sliders->volumetracks = g_list_append (sliders->volumetracks, strack);
  }

  if (track && colors) {
    gtk_table_attach (GTK_TABLE (table),
        gtk_vseparator_new (), x, x + 1, 0, 2,
        GTK_EXPAND, GTK_EXPAND | GTK_FILL, 0, 0);
    x++;
  }

  if (colors) {
    for (item = colors; item != NULL; item = item->next) {
      GstRecSlidersColorBalanceChannel *schannel =
          g_new (GstRecSlidersColorBalanceChannel, 1);
      const gchar *sicon;

      g_object_ref (G_OBJECT (item->data));
      schannel->channel = item->data;
      schannel->balance = balance;

      if ((sicon = get_icon_for_label (schannel->channel->label))) {
        icon = gtk_image_new_from_stock (sicon, GTK_ICON_SIZE_MENU);
        gtk_table_attach (GTK_TABLE (table), icon, x, x + 1, 0, 1,
            GTK_EXPAND, 0, 0, 0);
      }

      slider = gtk_vscale_new_with_range (schannel->channel->min_value,
          schannel->channel->max_value, 1);
      gtk_tooltips_set_tip (tool, slider, schannel->channel->label, NULL);
      gtk_scale_set_draw_value (GTK_SCALE (slider), FALSE);
      gtk_range_set_inverted (GTK_RANGE (slider), TRUE);
      gtk_range_set_value (GTK_RANGE (slider),
          gst_color_balance_get_value (balance, schannel->channel));

      g_signal_connect (G_OBJECT (slider), "value_changed",
          G_CALLBACK (cb_color_balance_changed), schannel);

      gtk_table_attach (GTK_TABLE (table), slider, x, x + 1, 1, 2,
          GTK_EXPAND, GTK_EXPAND | GTK_FILL, 0, 0);
      x++;
      sliders->colorbalancechannels =
          g_list_append (sliders->colorbalancechannels, schannel);
    }
  }
  gtk_container_add (GTK_CONTAINER (frame), table);
  gtk_widget_set_size_request (table, -1, 125);
  gtk_container_add (GTK_CONTAINER (sliders), frame);
  gtk_widget_show_all (frame);

  sliders->floating = floating;
  gtk_window_set_decorated (GTK_WINDOW (sliders), !floating);

  return GTK_WIDGET (sliders);
}

/*
 * Position the window below (centered) an existing widget.
 */

void
gst_rec_sliders_attach (GstRecSliders * slider, GtkWidget * widget)
{
  gint x, y;

  gdk_window_get_origin (widget->window, &x, &y);
  x += widget->allocation.x;
  x -= (GTK_WIDGET (slider)->allocation.width - widget->allocation.width) / 2;
  y += widget->allocation.height + widget->allocation.y;

  gtk_window_move (GTK_WINDOW (slider), x, y);
}

/*
 * Runs it and grabs input. Is destroyed on external mouseclick.
 */

void
gst_rec_sliders_run (GstRecSliders * sliders)
{
  if (sliders->floating) {
    GtkWidget *widget = GTK_WIDGET (sliders);

    gtk_widget_grab_focus (widget);
    gtk_grab_add (widget);
    gdk_pointer_grab (widget->window, TRUE,
        GDK_BUTTON_PRESS_MASK |
        GDK_BUTTON_RELEASE_MASK |
        GDK_POINTER_MOTION_MASK, NULL, NULL, GDK_CURRENT_TIME);
    gdk_keyboard_grab (widget->window, TRUE, GDK_CURRENT_TIME);
  }
}

/*
 * Mouse/keyboard event grabber functions. Meant for release
 * of the grab when clicked outside. The sliders deal with
 * motion themselves already.
 */

static void
gst_rec_sliders_detach (GtkWidget * widget)
{
  gdk_keyboard_ungrab (GDK_CURRENT_TIME);
  gdk_pointer_ungrab (GDK_CURRENT_TIME);
  gtk_grab_remove (widget);
  gtk_widget_destroy (widget);
}

static gboolean
gst_rec_sliders_mouse (GtkWidget * widget, GdkEventButton * event)
{
  /* on-click outside our GtkWindow, go away */
  if (GST_REC_SLIDERS (widget)->floating && widget->window != event->window) {
    gst_rec_sliders_detach (widget);
    return TRUE;
  }

  return FALSE;
}

static gboolean
gst_rec_sliders_keyboard (GtkWidget * widget, GdkEventKey * event)
{
  /* On escape/space, go away */
  if (GST_REC_SLIDERS (widget)->floating &&
      (event->keyval == GDK_Escape || event->keyval == GDK_space)) {
    gst_rec_sliders_detach (widget);
    return TRUE;
  }

  return FALSE;
}
