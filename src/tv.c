/* GStreamer Recorder
 * (c) 2003 Ronald Bultje <rbultje@ronald.bitfreak.net>
 *
 * tv.c: tv widget
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gtk/gtk.h>
#include <gtk/gtkmain.h>
#include <gtk/gtksignal.h>
#include <gdk/gdkx.h>
#include <gst/xoverlay/xoverlay.h>

#include "debug.h"
#include "tv.h"

static void gst_rec_tv_class_init (GstRecTvClass * klass);
static void gst_rec_tv_init (GstRecTv * tv);
static void gst_rec_tv_dispose (GObject * object);
static void gst_rec_tv_realize (GtkWidget * widget);
static void gst_rec_tv_size_request (GtkWidget * widget,
    GtkRequisition * requisition);
static void gst_rec_tv_size_allocate (GtkWidget * widget,
    GtkAllocation * allocation);
static gboolean gst_rec_tv_expose (GtkWidget * widget, GdkEventExpose * event);
static void gst_rec_tv_draw (GstRecTv * tv, gboolean on);

static GtkWidgetClass *parent_class = NULL;

GType
gst_rec_tv_get_type (void)
{
  static GType gst_rec_tv_type = 0;

  if (!gst_rec_tv_type) {
    static const GTypeInfo gst_rec_tv_info = {
      sizeof (GstRecTvClass),
      NULL,
      NULL,
      (GClassInitFunc) gst_rec_tv_class_init,
      NULL,
      NULL,
      sizeof (GstRecTv),
      0,
      (GInstanceInitFunc) gst_rec_tv_init,
      NULL
    };
    gst_rec_tv_type = g_type_register_static (GTK_TYPE_WIDGET,
	"GstRecTv", &gst_rec_tv_info, 0);
  }
  return gst_rec_tv_type;
}

static void
gst_rec_tv_class_init (GstRecTvClass * klass)
{
  GObjectClass *gobject_class;
  GtkWidgetClass *widget_class;

  gobject_class = (GObjectClass *) klass;
  widget_class = (GtkWidgetClass *) klass;

  parent_class = g_type_class_ref (GTK_TYPE_WIDGET);

  gobject_class->dispose = gst_rec_tv_dispose;

  widget_class->realize = gst_rec_tv_realize;
  widget_class->size_allocate = gst_rec_tv_size_allocate;
  widget_class->size_request = gst_rec_tv_size_request;
  widget_class->expose_event = gst_rec_tv_expose;
}

static void
gst_rec_tv_init (GstRecTv * tv)
{
  tv->element = NULL;
  tv->idle_expose = 0;
}

GtkWidget *
gst_rec_tv_new (GstElement * element)
{
  GstRecTv *tv = g_object_new (GST_REC_TYPE_TV, NULL);

  gst_rec_tv_set_element (tv, element);

  return GTK_WIDGET (tv);
}

static void
gst_rec_tv_dispose (GObject * object)
{
  GstRecTv *tv = GST_REC_TV (object);

  gst_rec_tv_set_element (tv, NULL);
  if (tv->idle_expose) {
    g_source_remove (tv->idle_expose);
    tv->idle_expose = 0;
  }

  if (((GObjectClass *) parent_class)->dispose)
    ((GObjectClass *) parent_class)->dispose (object);
}

void
gst_rec_tv_set_element (GstRecTv * tv, GstElement * element)
{
  GstElementFactory *f;

  if (element != tv->element) {
    gst_rec_tv_draw (tv, FALSE);
    gst_object_replace ((GstObject **) & tv->element, GST_OBJECT (element));
    if (element) {
      GST_LOG ("Setting up xoverlay interface for %s",
          GST_ELEMENT_NAME (element));

      f = gst_element_get_factory (element);
      tv->source =
          (g_strrstr (gst_element_factory_get_klass (f), "Source") != NULL);
    }
  }

  gst_rec_tv_draw (tv, TRUE);
}

static void
gst_rec_tv_realize (GtkWidget * widget)
{
  GdkWindowAttr attributes;
  gint attributes_mask;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GST_REC_IS_TV (widget));

  GTK_WIDGET_SET_FLAGS (widget, GTK_REALIZED);

  attributes.x = widget->allocation.x;
  attributes.y = widget->allocation.y;
  attributes.width = widget->allocation.width;
  attributes.height = widget->allocation.height;
  attributes.wclass = GDK_INPUT_OUTPUT;
  attributes.window_type = GDK_WINDOW_CHILD;
  attributes.event_mask = gtk_widget_get_events (widget) |
      GDK_EXPOSURE_MASK |
      GDK_BUTTON_PRESS_MASK |
      GDK_BUTTON_RELEASE_MASK |
      GDK_POINTER_MOTION_MASK | GDK_POINTER_MOTION_HINT_MASK;
  attributes.visual = gtk_widget_get_visual (widget);
  attributes.colormap = gtk_widget_get_colormap (widget);

  attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;
  widget->window = gdk_window_new (widget->parent->window,
      &attributes, attributes_mask);

  widget->style = gtk_style_attach (widget->style, widget->window);

  gdk_window_set_user_data (widget->window, widget);

  gtk_style_set_background (widget->style, widget->window, GTK_STATE_ACTIVE);
}


static void
gst_rec_tv_size_request (GtkWidget * widget, GtkRequisition * requisition)
{
  g_return_if_fail (widget != NULL);
  g_return_if_fail (GST_REC_IS_TV (widget));
  g_return_if_fail (requisition != NULL);

  requisition->width = 160;
  requisition->height = 120;
}

static void
gst_rec_tv_size_allocate (GtkWidget * widget, GtkAllocation * allocation)
{
  g_return_if_fail (widget != NULL);
  g_return_if_fail (GST_REC_IS_TV (widget));
  g_return_if_fail (allocation != NULL);

  widget->allocation = *allocation;

  if (GTK_WIDGET_REALIZED (widget))
    gdk_window_move_resize (widget->window,
	allocation->x, allocation->y, allocation->width, allocation->height);
}

static void
gst_rec_tv_draw (GstRecTv * tv, gboolean on)
{
  XID window;

  if (!GTK_WIDGET_REALIZED (GTK_WIDGET (tv)))
    return;

  gdk_draw_rectangle (GTK_WIDGET (tv)->window,
      GTK_WIDGET (tv)->style->black_gc,
      1, 0, 0,
      GTK_WIDGET (tv)->allocation.width, GTK_WIDGET (tv)->allocation.height);

  if (!tv->element || !GST_IS_X_OVERLAY (tv->element))
    return;

  if (on) {
    window = GDK_WINDOW_XWINDOW (GTK_WIDGET (tv)->window);
  } else {
    window = 0;
  }

  gst_x_overlay_set_xwindow_id (GST_X_OVERLAY (tv->element), window);

  if (window != 0) {
    gst_x_overlay_expose (GST_X_OVERLAY (tv->element));
  }
}

static gboolean
idle_expose (gpointer data)
{
  GstRecTv *tv = GST_REC_TV (data);

  /* ehw - hack */
  if (tv->source) {
    gst_rec_tv_draw (data, TRUE);
  }

  return FALSE;
}

static gboolean
gst_rec_tv_expose (GtkWidget * widget, GdkEventExpose * event)
{
  GstRecTv *tv;

  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GST_REC_IS_TV (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);

  if (event->count > 0)
    return FALSE;

  tv = GST_REC_TV (widget);

  gst_rec_tv_draw (tv, TRUE);
  if (tv->idle_expose)
    g_source_remove (tv->idle_expose);
  g_timeout_add (500, idle_expose, widget);

  return FALSE;
}
