/* GStreamer Recorder
 * (c) 2005 Ronald Bultje <rbultje@ronald.bitfreak.net>
 *
 * number.c: number chooser widget collection
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "debug.h"
#include "number.h"

enum {
  SIGNAL_VALUE_CHANGED,
  NUM_SIGNALS
};

static void gst_rec_number_class_init (GstRecNumberClass * klass);
static void gst_rec_number_init (GstRecNumber * param);
static void gst_rec_number_dispose (GObject * object);

static void cb_spin_valuechanged (GtkWidget * widget, gpointer data);
static void cb_combo_selectionchanged (GtkWidget * widget, gpointer data);

static guint signals[NUM_SIGNALS] = { 0 };
static GtkContainerClass *parent_class = NULL;

GType
gst_rec_number_get_type (void)
{
  static GType gst_rec_number_type = 0;

  if (!gst_rec_number_type) {
    static const GTypeInfo gst_rec_number_info = {
      sizeof (GstRecNumberClass),
      NULL,
      NULL,
      (GClassInitFunc) gst_rec_number_class_init,
      NULL,
      NULL,
      sizeof (GstRecNumber),
      0,
      (GInstanceInitFunc) gst_rec_number_init,
      NULL
    };

    gst_rec_number_type =
        g_type_register_static (GTK_TYPE_HBOX,
        "GstRecNumber", &gst_rec_number_info, 0);
  }

  return gst_rec_number_type;
}

static void
gst_rec_number_class_init (GstRecNumberClass * klass)
{
  GObjectClass *gobject_class = (GObjectClass *) klass;

  parent_class = g_type_class_ref (GTK_TYPE_HBOX);

  signals[SIGNAL_VALUE_CHANGED] =
      g_signal_new ("value-changed", G_TYPE_FROM_CLASS (klass),
          G_SIGNAL_RUN_LAST,
          G_STRUCT_OFFSET (GstRecNumberClass, value_changed),
          NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);

  gobject_class->dispose = gst_rec_number_dispose;
}

static void
gst_rec_number_init (GstRecNumber * num)
{
  num->combo = NULL;
  num->spin = NULL;
  num->label = NULL;
  num->type = GST_REC_NUMBER_RANGE;

  gtk_box_set_spacing (GTK_BOX (num), 0);
  gtk_box_set_homogeneous (GTK_BOX (num), FALSE);
}

static void
gst_rec_number_dispose (GObject * object)
{
  GstRecNumber *num = GST_REC_NUMBER (object);

  if (num->combo) {
    g_signal_handlers_disconnect_by_func (num->combo,
        G_CALLBACK (cb_combo_selectionchanged), num);
    num->combo = NULL;
  }

  if (num->spin) {
    g_signal_handlers_disconnect_by_func (num->spin,
        G_CALLBACK (cb_spin_valuechanged), num);
    num->spin = NULL;
  }

  G_OBJECT_CLASS (parent_class)->dispose (object);
}

GtkWidget *
gst_rec_number_new (gdouble value, gdouble min, gdouble max,
    gdouble step, guint digits)
{
  GstRecNumber *num = g_object_new (GST_REC_TYPE_NUMBER, NULL);
  GtkWidget *combo, *spin, *label;
  GtkCellRenderer *renderer;

  combo = gtk_combo_box_new ();
  g_signal_connect (combo, "changed",
      G_CALLBACK (cb_combo_selectionchanged), num);
  renderer = gtk_cell_renderer_text_new ();
  gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (combo), renderer, TRUE);
  gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (combo), renderer,
      "text", 0, NULL);
  gtk_box_pack_start (GTK_BOX (num), combo, FALSE, FALSE, 0);
  num->combo = GTK_COMBO_BOX (combo);

  spin = gtk_spin_button_new_with_range (min, max, step);
  g_signal_connect (spin, "value-changed",
      G_CALLBACK (cb_spin_valuechanged), num);
  gtk_box_pack_start (GTK_BOX (num), spin, FALSE, FALSE, 0);
  gtk_widget_show (spin);
  num->spin = GTK_SPIN_BUTTON (spin);
  gtk_spin_button_set_value (num->spin, value);
  gtk_spin_button_set_digits (num->spin, digits);
  gtk_spin_button_set_update_policy (num->spin, GTK_UPDATE_IF_VALID);

  label = gtk_label_new ("");
  gtk_box_pack_start (GTK_BOX (num), label, FALSE, FALSE, 0);
  num->label = GTK_LABEL (label);

  num->min = min;
  num->max = max;
  num->digits = digits;
  num->value = value;
  num->step = step;

  return GTK_WIDGET (num);
}

/*
 * Callbacks for if one widget changed value (probably active one).
 */

static void
cb_spin_valuechanged (GtkWidget * spin, gpointer data)
{
  GstRecNumber *num = GST_REC_NUMBER (data);

  GST_LOG ("Spinbutton changed value");

  if (num->type != GST_REC_NUMBER_RANGE)
    return;

  g_signal_emit (num, signals[SIGNAL_VALUE_CHANGED], 0);
}

static void
cb_combo_selectionchanged (GtkWidget * combo, gpointer data)
{
  GstRecNumber *num = GST_REC_NUMBER (data);

  GST_LOG ("Combobox changed value");

  if (num->type != GST_REC_NUMBER_LIST)
    return;

  g_signal_emit (num, signals[SIGNAL_VALUE_CHANGED], 0);
}

/*
 * Public functions.
 */

void
gst_rec_number_set_value (GstRecNumber * num, gdouble value)
{
  switch (num->type) {
    case GST_REC_NUMBER_RANGE:
      gtk_spin_button_set_value (num->spin, value);
      break;
    case GST_REC_NUMBER_FIXED:
      g_return_if_fail (num->value == value);
      break;
    case GST_REC_NUMBER_LIST: {
      GtkTreeIter iter;
      gboolean found = FALSE, avail;
      GtkTreeModel *model;
      gdouble x;

      model = gtk_combo_box_get_model (num->combo);
      for (avail = gtk_tree_model_get_iter_first (model, &iter);
           avail; avail = gtk_tree_model_iter_next (model, &iter)) {
        gtk_tree_model_get (gtk_combo_box_get_model (num->combo),
            &iter, 1, &x, -1);
        if (x == value) {
          found = TRUE;
          break;
        }
      }
      g_return_if_fail (found);
      gtk_combo_box_set_active_iter (num->combo, &iter);
      break;
    }
    default:
      g_return_if_reached ();
  }
}

gdouble
gst_rec_number_get_value (GstRecNumber * num)
{
  switch (num->type) {
    case GST_REC_NUMBER_RANGE:
      return gtk_spin_button_get_value (num->spin);
    case GST_REC_NUMBER_FIXED:
      return num->value;
    case GST_REC_NUMBER_LIST: {
      GtkTreeIter iter;
      gdouble val;

      if (!gtk_combo_box_get_active_iter (num->combo, &iter))
        return num->min;
      gtk_tree_model_get (gtk_combo_box_get_model (num->combo),
          &iter, 1, &val, -1);

      return val;
    }
    default:
      return num->min;
  }
}

static GtkWidget *
get_current (GstRecNumber * num)
{
  switch (num->type) {
    case GST_REC_NUMBER_FIXED:
      return GTK_WIDGET (num->label);
    case GST_REC_NUMBER_RANGE:
      return GTK_WIDGET (num->spin);
    case GST_REC_NUMBER_LIST:
      return GTK_WIDGET (num->combo);
    default:
      return NULL;
  }
}

void
gst_rec_number_set_range (GstRecNumber * num, const GValue * val)
{
  GType t = G_VALUE_TYPE (val);
  GtkWidget *cur;

  /* lose current */
  cur = get_current (num);
  if (cur) {
    gtk_widget_hide (cur);
  }

  /* switch */
  if (t == G_TYPE_INT || t == G_TYPE_DOUBLE) {
    gchar *s;

    num->type = GST_REC_NUMBER_FIXED;

    if (t == G_TYPE_INT) {
      s = g_strdup_printf ("%d", g_value_get_int (val));
      num->value = g_value_get_int (val);
    } else {
      s = g_strdup_printf ("%.*lf", num->digits, g_value_get_double (val));
      num->value = g_value_get_double (val);
    }

    gtk_label_set_text (num->label, s);
    g_free (s);

    /* signal value changed, once */
    g_signal_emit (num, signals[SIGNAL_VALUE_CHANGED], 0);
  } else if (t == GST_TYPE_INT_RANGE || t == GST_TYPE_DOUBLE_RANGE) {
    gdouble min, max, cur;

    cur = gst_rec_number_get_value (num);
    num->type = GST_REC_NUMBER_RANGE;

    if (t == GST_TYPE_INT_RANGE) {
      min = gst_value_get_int_range_min (val);
      max = gst_value_get_int_range_max (val);
    } else {
      min = gst_value_get_double_range_min (val);
      max = gst_value_get_double_range_max (val);
    }

    if (min < num->min)
      min = num->min;
    if (max > num->max)
      max = num->max;
    if (min > max) {
      gdouble t;

      g_warning ("min %lf > max %lf, this should not happen", min, max);
      t = max;
      max = min;
      min = t;
    }

    gtk_spin_button_set_range (num->spin, min, max);
    gtk_spin_button_set_value (num->spin, cur);
  } else if (t == GST_TYPE_LIST) {
    GtkListStore *model;
    GtkTreeIter listiter, selectiter;
    gboolean select = FALSE;
    const GValue *kid;
    gint n, size;
    gchar *s;
    gdouble new_val, old_val;

    old_val = gst_rec_number_get_value (num);
    num->type = GST_REC_NUMBER_LIST;

    model = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_DOUBLE);
    size = gst_value_list_get_size (val);
    for (n = 0; n < size; n++) {
      kid = gst_value_list_get_value (val, n);
      if (G_VALUE_TYPE (kid) == G_TYPE_INT) {
        s = g_strdup_printf ("%d", g_value_get_int (kid));
        new_val = g_value_get_int (kid);
      } else if (G_VALUE_TYPE (kid) == G_TYPE_DOUBLE) {
        s = g_strdup_printf ("%.*lf", num->digits, g_value_get_double (kid));
        new_val = g_value_get_double (kid);
      } else {
        continue;
      }

      if (new_val > num->max || new_val < num->min) {
        g_free (s);
        continue;
      }

      gtk_list_store_append (model, &listiter);
      gtk_list_store_set (model, &listiter, 0, s, 1, new_val, -1);
      g_free (s);
      if (!select || new_val == old_val) {
        selectiter = listiter;
        select = TRUE;
      }
    }
    gtk_combo_box_set_model (num->combo, GTK_TREE_MODEL (model));
    if (select) {
      gtk_combo_box_set_active_iter (num->combo, &selectiter);
    }
    gtk_widget_show_all (GTK_WIDGET (num->combo));
  } else {
    gchar *x = gst_value_serialize (val);

    num->type = GST_REC_NUMBER_UNKNOWN;

    g_warning ("Invalid value type 0x%x (%s)", (guint) t, x);
    g_free (x);
  }

  /* show new */
  cur = get_current (num);
  if (cur) {
    gtk_widget_show (cur);
  }
}
