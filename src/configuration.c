/* GStreamer Recorder
 * (c) 2003 Ronald Bultje <rbultje@ronald.bitfreak.net>
 *
 * configuration.c: general widget (subclasses in configuration.c
 * and druid.c) that handles UI-wise setting of the pipeline.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <string.h>
#include <glib/gi18n.h>
#include <gst/mixer/mixer.h>
#include <gst/tuner/tuner.h>

#include "debug.h"
#include "keys.h"
#include "list.h"
#include "configuration.h"
#include "record.h"

static void gst_rec_configuration_class_init (GstRecConfigurationClass * klass);
static void gst_rec_configuration_init (GstRecConfiguration * conf);
static void gst_rec_configuration_dispose (GObject * obj);
static void gst_rec_configuration_response (GtkDialog * dialog,
    gint response_id);

static GtkDialogClass *parent_class = NULL;

GType
gst_rec_configuration_get_type (void)
{
  static GType gst_rec_configuration_type = 0;

  if (!gst_rec_configuration_type) {
    static const GTypeInfo gst_rec_configuration_info = {
      sizeof (GstRecConfigurationClass),
      NULL,
      NULL,
      (GClassInitFunc) gst_rec_configuration_class_init,
      NULL,
      NULL,
      sizeof (GstRecConfiguration),
      0,
      (GInstanceInitFunc) gst_rec_configuration_init,
      NULL
    };

    gst_rec_configuration_type =
        g_type_register_static (GTK_TYPE_DIALOG,
        "GstRecConfiguration", &gst_rec_configuration_info, 0);
  }

  return gst_rec_configuration_type;
}

static void
gst_rec_configuration_class_init (GstRecConfigurationClass * klass)
{
  GObjectClass *gobject_class = (GObjectClass *) klass;
  GtkDialogClass *gtkdialog_class = (GtkDialogClass *) klass;

  parent_class = g_type_class_ref (GTK_TYPE_DIALOG);

  gobject_class->dispose = gst_rec_configuration_dispose;
  gtkdialog_class->response = gst_rec_configuration_response;
}

static void
gst_rec_configuration_init (GstRecConfiguration * conf)
{
  /* GConf stuff */
  conf->client = NULL;
  conf->rec = NULL;

  /* signal IDs */
  conf->ids = NULL;
}

static void
gst_rec_configuration_dispose (GObject * obj)
{
  GList *item;
  GstRecConfiguration *conf = GST_REC_CONFIGURATION (obj);

  for (item = conf->ids; item != NULL; item = item->next) {
    gulong id = (gulong) item->data;

    g_signal_handler_disconnect (G_OBJECT (conf->rec), id);
  }
  g_list_free (conf->ids);
  conf->ids = NULL;

  /* get rid of rec object */
  gst_rec_configuration_set_backend (conf, NULL, NULL);

  /* give parent a chance to clean up */
  if (((GObjectClass *) parent_class)->dispose)
    ((GObjectClass *) parent_class)->dispose (obj);
}

static void
gst_rec_configuration_response (GtkDialog * dialog, gint response_id)
{
  switch (response_id) {
    case GTK_RESPONSE_DELETE_EVENT:
      gtk_widget_destroy (GTK_WIDGET (dialog));
      break;

    default:
      break;
  }

  if (((GtkDialogClass *) parent_class)->response)
    ((GtkDialogClass *) parent_class)->response (dialog, response_id);
}

void
gst_rec_configuration_set_backend (GstRecConfiguration * conf,
    GstRec * rec, GConfClient * client)
{
  g_return_if_fail (conf != NULL);

  gst_object_replace ((GstObject **) & conf->rec, GST_OBJECT (rec));
  conf->client = client;
}

/*
 * Utility function. Checks if the value given in the second
 * argument is a member of the set in the first argument.
 * Basically because I can't find a good gst_value_is_subset().
 */

static gboolean
gst_rec_value_is_subset (const GValue * set, gint num)
{
  if (G_VALUE_TYPE (set) == G_TYPE_INT) {
    return (num == g_value_get_int (set));
  } else if (G_VALUE_TYPE (set) == GST_TYPE_INT_RANGE) {
    return (num >= gst_value_get_int_range_min (set) &&
            num <= gst_value_get_int_range_max (set));
  } else if (G_VALUE_TYPE (set) == GST_TYPE_LIST) {
    gint size = gst_value_list_get_size (set), n;
    const GValue *kid;

    for (n = 0; n < size; n++) {
      kid = gst_value_list_get_value (set, n);
      if (gst_rec_value_is_subset (kid, num))
        return TRUE;
    }

    /* nope */
    return FALSE;
  } else {
    /* default */
    return FALSE;
  }
}

/*
 * Create a GtkComboBox with a renderer attached for the first column.
 */

static GtkWidget *
gst_rec_combo_box_new_with_renderer (void)
{
  GtkWidget *box;
  GtkCellRenderer *renderer;

  box = gtk_combo_box_new ();
  renderer = gtk_cell_renderer_text_new ();

  gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (box), renderer, TRUE);
  gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (box), renderer,
      "text", 0, NULL);

  return box;
}

/*
 * GtkComboBox object cleanup function.
 */

static gboolean
cb_cleanup (GtkTreeModel * model,
    GtkTreePath * path, GtkTreeIter * iter, gpointer data)
{
  GObject *obj;

  gtk_tree_model_get (model, iter, 1, &obj, -1);
  g_object_unref (obj);

  return TRUE;
}

/*
 * Encoder selection.
 */

static GtkComboBox *
gst_rec_configuration_add_encoder_selection (GstRecConfiguration * conf,
    GtkWidget * table, gint row, GCallback changefunc)
{
  GtkWidget *label, *menuwidget;

  /* label */
  label = gtk_label_new (_("Available encoders:"));
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, GTK_MISC (label)->yalign);
  gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
  gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, row, row + 1);
  gtk_widget_show (label);

  /* encoders */
  menuwidget = gst_rec_combo_box_new_with_renderer ();
  gtk_table_attach_defaults (GTK_TABLE (table), menuwidget, 1, 2, row, row + 1);
  g_signal_connect (G_OBJECT (menuwidget), "changed", changefunc, conf);
  
  /* make sure gconf is in sync with conf */
  g_signal_emit_by_name (menuwidget, "changed");
  return GTK_COMBO_BOX (menuwidget);
}

static void
gst_rec_configuration_get_encoder_model (GstRecConfiguration * conf,
    GtkComboBox * box, GList * encoders, GstRecEncoder * current)
{
  GtkListStore *model;
  GList *item;
  GtkTreeIter listiter, selectiter;
  gboolean select = FALSE;

  /* if there was a menu, clean up old references */
  if (gtk_combo_box_get_model (box))
    gtk_tree_model_foreach (gtk_combo_box_get_model (box), cb_cleanup, NULL);

  /* create menu - FIXME: images */
  model = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_POINTER);
  for (item = encoders; item != NULL; item = item->next) {
    GstRecEncoder *encoder = item->data;

    gtk_list_store_append (model, &listiter);
    gtk_list_store_set (model, &listiter, 0, encoder->name, 1, encoder, -1);
    if (!select || current == encoder) {
      selectiter = listiter;
      select = TRUE;
    }
  }
  gtk_combo_box_set_model (box, GTK_TREE_MODEL (model));
  if (select)
    gtk_combo_box_set_active_iter (box, &selectiter);
  gtk_widget_show_all (GTK_WIDGET (box));

  /* we've taken reference of items, get rid of list encapsulator */
  g_list_free (encoders);
}

static void
gst_rec_configuration_set_video_encoder_menu (GstRecConfiguration * conf)
{
  GList *encoders;
  GstRecVideoEncoder *current;

  encoders = gst_rec_list_video_encoders (gst_rec_get_video_source (conf->rec),
      gst_rec_get_muxer (conf->rec));
  current = gst_rec_list_find_video_encoder (conf->rec, encoders);
  gst_rec_configuration_get_encoder_model (conf,
      conf->video_encoders.selection_menu, encoders, (GstRecEncoder *) current);
  
  /* make sure gconf is in sync with config */
  g_signal_emit_by_name (GTK_COMBO_BOX (conf->video_encoders.selection_menu), "changed");
}

static void
cb_video_encoder_selected (GtkComboBox * box, gpointer data)
{
  GstRecConfiguration *conf = GST_REC_CONFIGURATION (data);
  GstRecVideoEncoder *encoder;
  gchar *config;
  GtkTreeIter iter;

  if (gtk_combo_box_get_active_iter (box, &iter)) {
    /* get selection */
    gtk_tree_model_get (gtk_combo_box_get_model (box), &iter, 1, &encoder, -1);

    /* set newly selected video source */
    config = gst_rec_video_encoder_to_config (encoder);
    gconf_client_set_string (conf->client, GST_REC_KEY_VIDEO_ENCODER,
        config, NULL);
    g_free (config);

    config = gst_rec_video_filter_to_config (encoder);
    gconf_client_set_string (conf->client, GST_REC_KEY_VIDEO_FILTER,
        config, NULL);
    g_free (config);

    GST_INFO ("Selected video encoder %s", encoder->name);
  } else {
    gconf_client_set_string (conf->client, GST_REC_KEY_VIDEO_ENCODER, "", NULL);
    gconf_client_set_string (conf->client, GST_REC_KEY_VIDEO_FILTER, "", NULL);
  }
}

static void
gst_rec_configuration_set_audio_encoder_menu (GstRecConfiguration * conf)
{
  GList *encoders;
  GstRecAudioEncoder *current;

  encoders = gst_rec_list_audio_encoders (gst_rec_get_audio_source (conf->rec),
      gst_rec_get_muxer (conf->rec));
  current = gst_rec_list_find_audio_encoder (conf->rec, encoders);
  gst_rec_configuration_get_encoder_model (conf,
      conf->audio_encoders.selection_menu, encoders, (GstRecEncoder *) current);
}

static void
cb_audio_encoder_selected (GtkComboBox * box, gpointer data)
{
  GstRecConfiguration *conf = GST_REC_CONFIGURATION (data);
  GstRecAudioEncoder *encoder;
  gchar *config;
  GtkTreeIter iter;

  if (gtk_combo_box_get_active_iter (box, &iter)) {
    /* get selection */
    gtk_tree_model_get (gtk_combo_box_get_model (box), &iter, 1, &encoder, -1);

    /* set newly selected video source */
    config = gst_rec_audio_encoder_to_config (encoder);
    gconf_client_set_string (conf->client, GST_REC_KEY_AUDIO_ENCODER,
        config, NULL);
   g_free (config);

   config = gst_rec_audio_filter_to_config (encoder);
   gconf_client_set_string (conf->client, GST_REC_KEY_AUDIO_FILTER,
        config, NULL);
   g_free (config);

   GST_INFO ("Selected audio encoder %s", encoder->name);
  } else {
    gconf_client_set_string (conf->client, GST_REC_KEY_AUDIO_ENCODER, "", NULL);
    gconf_client_set_string (conf->client, GST_REC_KEY_AUDIO_FILTER, "", NULL);
  }
}

/*
 * Muxer selection.
 */

static void
gst_rec_configuration_set_muxer_menu (GstRecConfiguration * conf)
{
  GtkListStore *model;
  GstRecMuxer *current;
  GList *muxers, *item;
  GtkTreeIter listiter, selectiter;
  gboolean select = FALSE;

  /* if there was a menu, clean up old references */
  if (gtk_combo_box_get_model (conf->muxers.selection_menu))
    gtk_tree_model_foreach (gtk_combo_box_get_model (conf->muxers.
            selection_menu), cb_cleanup, NULL);

  /* get available muxers */
  muxers = gst_rec_list_muxers (gst_rec_get_video_source (conf->rec),
      gst_rec_get_audio_source (conf->rec));

  /* get template */
  current = gst_rec_list_find_muxer (conf->rec, muxers);

  /* add each item to the menu */
  model = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_POINTER);
  for (item = muxers; item != NULL; item = item->next) {
    GstRecMuxer *muxer = item->data;

    gtk_list_store_append (model, &listiter);
    gtk_list_store_set (model, &listiter, 0, muxer->name, 1, muxer, -1);
    if (!select || current == muxer) {
      selectiter = listiter;
      select = TRUE;
    }
  }
  gtk_combo_box_set_model (conf->muxers.selection_menu, GTK_TREE_MODEL (model));
  if (select)
    gtk_combo_box_set_active_iter (conf->muxers.selection_menu, &selectiter);
  gtk_widget_show_all (GTK_WIDGET (conf->muxers.selection_menu));

  /* clean list encapsulator */
  g_list_free (muxers);
}

static void
cb_muxer_selected (GtkComboBox * box, gpointer data)
{
  GstRecConfiguration *conf = GST_REC_CONFIGURATION (data);
  GstRecMuxer *muxer;
  gchar *config;
  GtkTreeIter iter;
  gboolean ret;

  /* get selection */
  ret = gtk_combo_box_get_active_iter (box, &iter);
  g_return_if_fail (ret);
  gtk_tree_model_get (gtk_combo_box_get_model (box), &iter, 1, &muxer, -1);

  /* set newly selected video source */
  config = gst_rec_muxer_to_config (muxer);
  gconf_client_set_string (conf->client, GST_REC_KEY_MUXER, config, NULL);
  g_free (config);

  GST_INFO ("Selected muxer %s", muxer->name);
}

static void
cb_muxer_changed (GstRec * rec, GstElement * muxer, gpointer data)
{
  GstRecConfiguration *conf = GST_REC_CONFIGURATION (data);

  /* and change both encoder menus accordingly */
  gst_rec_configuration_set_video_encoder_menu (conf);
  gst_rec_configuration_set_audio_encoder_menu (conf);
}

GtkWidget *
gst_rec_configuration_get_output_format_page (GstRecConfiguration * conf)
{
  GtkWidget *table, *label, *menuwidget;
  gulong id;

  /* table container */
  table = gtk_table_new (3, 2, FALSE);
  conf->output_format_page.table = GTK_TABLE (table);

  /* label */
  label = gtk_label_new (_("Available muxers:"));
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, GTK_MISC (label)->yalign);
  gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
  gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 0, 1);
  gtk_widget_show (label);

  /* muxers */
  menuwidget = gst_rec_combo_box_new_with_renderer ();
  gtk_table_attach_defaults (GTK_TABLE (table), menuwidget, 1, 2, 0, 1);
  g_signal_connect (G_OBJECT (menuwidget), "changed",
      G_CALLBACK (cb_muxer_selected), conf);
  conf->muxers.selection_menu = GTK_COMBO_BOX (menuwidget);
  gst_rec_configuration_set_muxer_menu (conf);

  /* if the muxer has changed, update the encoders pages */
  id = g_signal_connect (G_OBJECT (conf->rec), "muxer_changed",
      G_CALLBACK (cb_muxer_changed), conf);
  conf->ids = g_list_append (conf->ids, (gpointer) id);

  /* video encoders */
  conf->video_encoders.selection_menu =
      gst_rec_configuration_add_encoder_selection (conf, table, 1,
      G_CALLBACK (cb_video_encoder_selected));
  gst_rec_configuration_set_video_encoder_menu (conf);

  /* audio encoders */
  conf->audio_encoders.selection_menu =
      gst_rec_configuration_add_encoder_selection (conf, table, 2,
      G_CALLBACK (cb_audio_encoder_selected));
  gst_rec_configuration_set_audio_encoder_menu (conf);

  return table;
}

/*
 * Source selection.
 */

static GtkComboBox *
gst_rec_configuration_add_device_selection (GstRecConfiguration * conf,
    GtkWidget * table,
    gint row,
    GList * sources, GstElement * current_source, GCallback changefunc)
{
  GtkWidget *label, *menuwidget;
  GtkListStore *model;
  GstRecSource *current;
  GList *item;
  GtkTreeIter listiter, selectiter;
  gboolean select = FALSE;

  /* get factory */
  current = gst_rec_list_find_source (conf->rec, sources, current_source);

  /* label */
  label = gtk_label_new (_("Available sources:"));
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, GTK_MISC (label)->yalign);
  gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
  gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, row, row + 1);
  gtk_widget_show (label);

  /* menu */
  menuwidget = gst_rec_combo_box_new_with_renderer ();
  gtk_table_attach_defaults (GTK_TABLE (table), menuwidget, 1, 2, row, row + 1);

  /* add each item to the menu */
  model = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_POINTER);
  for (item = sources; item != NULL; item = item->next) {
    GstRecSource *source = item->data;

    gtk_list_store_append (model, &listiter);
    gtk_list_store_set (model, &listiter, 0, source->name, 1, source, -1);
    g_object_ref (G_OBJECT (source));
    if (!select || current == source) {
      selectiter = listiter;
      select = TRUE;
    }
  }
  gtk_combo_box_set_model (GTK_COMBO_BOX (menuwidget), GTK_TREE_MODEL (model));
  if (select)
    gtk_combo_box_set_active_iter (GTK_COMBO_BOX (menuwidget), &selectiter);
  gtk_widget_show_all (menuwidget);

  g_signal_connect (G_OBJECT (menuwidget), "changed", changefunc, conf);
  /* we emit a "changed" signal here to ensure that the gconf keys
     defining audio and video sources stays synced with the actual
     configuration. */
  g_signal_emit_by_name (menuwidget, "changed");

  return GTK_COMBO_BOX (menuwidget);
}

static void
gst_rec_configuration_set_video_input_menu (GstRecConfiguration * conf)
{
  GtkListStore *model;
  GstElement *video_source;
  GstTuner *tuner;
  const GstTunerChannel *current_channel;
  const GList *item, *list;
  GtkTreeIter listiter, selectiter;
  gboolean select = FALSE;

  /* if there was a menu, clean up old references */
  if (gtk_combo_box_get_model (conf->video_inputs.selection_menu))
    gtk_tree_model_foreach (gtk_combo_box_get_model (conf->video_inputs.
            selection_menu), cb_cleanup, NULL);

  video_source = gst_rec_get_video_source (conf->rec);
  if (!video_source || !GST_IS_TUNER (video_source)) {
    gtk_widget_hide (GTK_WIDGET (conf->video_inputs.selection_menu));
    gtk_widget_hide (conf->video_inputs.title);
    gtk_table_set_row_spacing (conf->video_sources_page.table, 1, 0);
    gtk_combo_box_set_model (conf->video_inputs.selection_menu,
        GTK_TREE_MODEL (gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_POINTER)));
    return;
  }

  /* FIXME: if no inputs available, disable all this too */

  gtk_widget_show (conf->video_inputs.title);
  gtk_table_set_row_spacing (conf->video_sources_page.table, 1,
      gtk_table_get_default_row_spacing (conf->video_sources_page.table));

  tuner = GST_TUNER (video_source);
  list = gst_tuner_list_channels (tuner);
  current_channel = gst_tuner_get_channel (tuner);

  model = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_POINTER);
  for (item = list; item != NULL; item = item->next) {
    GstTunerChannel *channel = item->data;

    gtk_list_store_append (model, &listiter);
    gtk_list_store_set (model, &listiter, 0, channel->label, 1, channel, -1);
    g_object_ref (G_OBJECT (channel));
    if (!select || channel == current_channel) {
      selectiter = listiter;
      select = TRUE;
    }
  }
  gtk_combo_box_set_model (conf->video_inputs.selection_menu,
      GTK_TREE_MODEL (model));
  if (select)
    gtk_combo_box_set_active_iter (conf->video_inputs.selection_menu,
        &selectiter);
  gtk_widget_show_all (GTK_WIDGET (conf->video_inputs.selection_menu));
}

static void
gst_rec_configuration_set_video_norm_menu (GstRecConfiguration * conf)
{
  GtkListStore *model;
  GstElement *video_source;
  GstTuner *tuner;
  const GstTunerNorm *current_norm;
  const GList *item, *list;
  GtkTreeIter selectiter, listiter;
  gboolean select = FALSE;

  /* if there was a menu, clean up old references */
  if (gtk_combo_box_get_model (conf->video_norms.selection_menu))
    gtk_tree_model_foreach (gtk_combo_box_get_model (conf->video_norms.
            selection_menu), cb_cleanup, NULL);

  video_source = gst_rec_get_video_source (conf->rec);
  if (!video_source || !GST_IS_TUNER (video_source)) {
    gtk_widget_hide (GTK_WIDGET (conf->video_norms.selection_menu));
    gtk_widget_hide (conf->video_norms.title);
    gtk_table_set_row_spacing (conf->video_sources_page.table, 2, 0);
    gtk_combo_box_set_model (conf->video_norms.selection_menu,
        GTK_TREE_MODEL (gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_POINTER)));
    return;
  }

  /* FIXME: if no inputs available, disable all this too */

  gtk_widget_show (conf->video_norms.title);
  gtk_table_set_row_spacing (conf->video_sources_page.table, 2,
      gtk_table_get_default_row_spacing (conf->video_sources_page.table));

  tuner = GST_TUNER (video_source);
  list = gst_tuner_list_norms (tuner);
  current_norm = gst_tuner_get_norm (tuner);

  model = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_POINTER);
  for (item = list; item != NULL; item = item->next) {
    GstTunerNorm *norm = item->data;

    gtk_list_store_append (model, &listiter);
    gtk_list_store_set (model, &listiter, 0, norm->label, 1, norm, -1);
    g_object_ref (G_OBJECT (norm));
    if (!select || norm == current_norm) {
      selectiter = listiter;
      select = TRUE;
    }
  }
  gtk_combo_box_set_model (conf->video_norms.selection_menu,
      GTK_TREE_MODEL (model));
  if (select)
    gtk_combo_box_set_active_iter (conf->video_norms.selection_menu,
        &selectiter);
  gtk_widget_show_all (GTK_WIDGET (conf->video_norms.selection_menu));
}

static void
gst_rec_configuration_set_video_size_range (GstRecConfiguration * conf)
{
  GstElement *video_source;
  GstCaps *caps;
  GstStructure *structure;
  const GValue *wvalue, *hvalue, *fvalue;

  video_source = gst_rec_get_video_source (conf->rec);
  if (!video_source) {
    gtk_widget_set_sensitive (GTK_WIDGET (conf->video_size.width), FALSE);
    gtk_widget_set_sensitive (GTK_WIDGET (conf->video_size.height), FALSE);
    gtk_widget_set_sensitive (GTK_WIDGET (conf->video_size.framerate), FALSE);
    gtk_widget_set_sensitive (GTK_WIDGET (conf->video_size.title), FALSE);
    gtk_widget_set_sensitive (GTK_WIDGET (conf->video_size.x), FALSE);
    gtk_widget_set_sensitive (GTK_WIDGET (conf->video_size.at), FALSE);
    gtk_widget_set_sensitive (GTK_WIDGET (conf->video_size.fps), FALSE);
    return;
  }

  gtk_widget_set_sensitive (GTK_WIDGET (conf->video_size.width), TRUE);
  gtk_widget_set_sensitive (GTK_WIDGET (conf->video_size.height), TRUE);
  gtk_widget_set_sensitive (GTK_WIDGET (conf->video_size.framerate), TRUE);
  gtk_widget_set_sensitive (GTK_WIDGET (conf->video_size.title), TRUE);
  gtk_widget_set_sensitive (GTK_WIDGET (conf->video_size.x), TRUE);
  gtk_widget_set_sensitive (GTK_WIDGET (conf->video_size.at), TRUE);
  gtk_widget_set_sensitive (GTK_WIDGET (conf->video_size.fps), TRUE);

  caps = g_object_get_data (G_OBJECT (video_source), "template");
  if (caps) {
    caps = gst_caps_copy (caps);
  } else {
    caps = gst_pad_get_caps (gst_element_get_pad (video_source, "src"));
  }
  structure = gst_caps_get_structure (caps, 0);
  wvalue = gst_structure_get_value (structure, "width");
  hvalue = gst_structure_get_value (structure, "height");
  fvalue = gst_structure_get_value (structure, "framerate");

  if (!wvalue || !hvalue || !fvalue) {
    g_warning ("no size/fps properties found - size selection will break");
    return;
  }

  /* set new ranges */
  gst_rec_number_set_range (conf->video_size.width, wvalue);
  gst_rec_number_set_range (conf->video_size.height, hvalue);
  gst_rec_number_set_range (conf->video_size.framerate, fvalue);

  /* free caps */
  gst_caps_free (caps);
}

static void
cb_video_source_selected (GtkComboBox * box, gpointer data)
{
  GstRecConfiguration *conf = GST_REC_CONFIGURATION (data);
  GstRecVideoSource *source;
  gchar *config;
  GtkTreeIter iter;
  gboolean ret;

  /* get selection */
  ret = gtk_combo_box_get_active_iter (box, &iter);
  g_return_if_fail (ret);
  gtk_tree_model_get (gtk_combo_box_get_model (box), &iter, 1, &source, -1);

  /* set newly selected video source */
  config = gst_rec_video_source_to_config (source);
  gconf_client_set_string (conf->client, GST_REC_KEY_VIDEO_SOURCE,
      config, NULL);
  g_free (config);

  GST_INFO ("Selected video source %s", source->name);
}

static void
cb_video_source_changed (GstRec * rec, GstElement * video_source, gpointer data)
{
  GstRecConfiguration *conf = GST_REC_CONFIGURATION (data);

  /* update input/norm/size widgets */
  gst_rec_configuration_set_video_input_menu (conf);
  gst_rec_configuration_set_video_norm_menu (conf);
  gst_rec_configuration_set_video_size_range (conf);

  /* And change muxer menu accordingly - first emit signal in case
   * nothing changes... */
  g_signal_emit_by_name (rec, "muxer_changed", rec->muxer);
  gst_rec_configuration_set_muxer_menu (conf);
}

static void
cb_capture_size_changed (GstRecNumber * num, gpointer data)
{
  GstRecConfiguration *conf = GST_REC_CONFIGURATION (data);
  gdouble val;

  val = gst_rec_number_get_value (num);
  if (num == conf->video_size.width) {
    gconf_client_set_int (conf->client, GST_REC_KEY_VIDEO_WIDTH, val, NULL);
  } else if (num == conf->video_size.height) {
    gconf_client_set_int (conf->client, GST_REC_KEY_VIDEO_HEIGHT, val, NULL);
  } else if (num == conf->video_size.framerate) {
    gconf_client_set_float (conf->client, GST_REC_KEY_VIDEO_FRAMERATE,
         val, NULL);
  } else {
    g_warning ("Unknown widget changed size/fps!");
    return;
  }

  GST_INFO ("Capture size %ux%u@%lffps selected",
      (guint) gst_rec_number_get_value (conf->video_size.width),
      (guint) gst_rec_number_get_value (conf->video_size.height),
      gst_rec_number_get_value (conf->video_size.framerate));
}

static void
gst_rec_configuration_add_video_size_selection (GstRecConfiguration * conf,
    GtkWidget * table, guint row)
{
  GtkWidget *label, *num, *box;
  guint w, h;
  gdouble fps;

  w = gst_rec_get_video_width (conf->rec);
  h = gst_rec_get_video_height (conf->rec);
  fps = gst_rec_get_video_fps (conf->rec);

  /* label */
  label = gtk_label_new (_("Capture size:"));
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, GTK_MISC (label)->yalign);
  gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
  gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, row, row + 1);
  gtk_widget_show (label);
  conf->video_size.title = label;

  /* box containing size spinbuttons */
  box = gtk_hbox_new (FALSE, 6);
  gtk_table_attach_defaults (GTK_TABLE (table), box, 1, 2, row, row + 1);

  /* "width" */
  num = gst_rec_number_new (w, 16, 4096, 16, 0);
  gtk_box_pack_start (GTK_BOX (box), num, FALSE, FALSE, 0);
  g_signal_connect (G_OBJECT (num), "value_changed",
      G_CALLBACK (cb_capture_size_changed), conf);
  conf->video_size.width = GST_REC_NUMBER (num);
  gtk_widget_show (num);

  /* Translator comment: the x here implies a separator between 
   * width and height in a size selection widget. */
  label = gtk_label_new (_("x"));
  gtk_box_pack_start (GTK_BOX (box), label, FALSE, FALSE, 0);
  conf->video_size.x = label;
  gtk_widget_show (label);

  /* "height" */
  num = gst_rec_number_new (h, 16, 4096, 16, 0);
  gtk_box_pack_start (GTK_BOX (box), num, FALSE, FALSE, 0);
  g_signal_connect (G_OBJECT (num), "value_changed",
      G_CALLBACK (cb_capture_size_changed), conf);
  conf->video_size.height = GST_REC_NUMBER (num);
  gtk_widget_show (num);

  /* Translator comment: the "at" here implies a separator between
   * width/height and framerate in a size/framerate selection widget.
   * Example: 384 x 288 at 25.0 frames/sec. */
  label = gtk_label_new (_("at"));
  gtk_box_pack_start (GTK_BOX (box), label, FALSE, FALSE, 0);
  conf->video_size.at = label;
  gtk_widget_show (label);

  /* "fps" */
  num = gst_rec_number_new (fps, 1, 100, 1, 2);
  gtk_box_pack_start (GTK_BOX (box), num, FALSE, FALSE, 0);
  g_signal_connect (G_OBJECT (num), "value_changed",
      G_CALLBACK (cb_capture_size_changed), conf);
  conf->video_size.framerate = GST_REC_NUMBER (num);
  gtk_widget_show (num);

  /* Translator comment: fps = frames per second */
  label = gtk_label_new (_("fps"));
  gtk_box_pack_start (GTK_BOX (box), label, FALSE, FALSE, 0);
  conf->video_size.fps = label;
  gtk_widget_show (label);

  /* set defaults */
  gst_rec_configuration_set_video_size_range (conf);

  /* make sure gconf is in sync with config */
  g_signal_emit_by_name (conf->video_size.width, "value_changed");
  g_signal_emit_by_name (conf->video_size.height, "value_changed");
  g_signal_emit_by_name (conf->video_size.framerate, "value_changed");

  /* show */
  gtk_widget_show (box);
}

static void
cb_video_input_selected (GtkComboBox * box, gpointer data)
{
  GstRecConfiguration *conf = GST_REC_CONFIGURATION (data);
  GstTuner *tuner = NULL;
  GstTunerChannel *current_channel;
  GtkTreeIter iter;

  if (GST_IS_TUNER (gst_rec_get_video_source (conf->rec))) {
    tuner = GST_TUNER (gst_rec_get_video_source (conf->rec));
  }
  if (gtk_combo_box_get_active_iter (box, &iter) && tuner != NULL) {
    /* get selection */
    gtk_tree_model_get (gtk_combo_box_get_model (box), &iter,
        1, &current_channel, -1);
    gconf_client_set_string (conf->client, GST_REC_KEY_VIDEO_INPUT,
        current_channel->label, NULL);
    
    GST_INFO ("Video input %s selected", current_channel->label);
  } else {
    gconf_client_set_string (conf->client, GST_REC_KEY_VIDEO_INPUT, "", NULL);
  }
}

static void
cb_video_norm_selected (GtkComboBox * box, gpointer data)
{
  GstRecConfiguration *conf = GST_REC_CONFIGURATION (data);
  GstTuner *tuner = NULL;
  GstTunerNorm *current_norm;
  GtkTreeIter iter;

  if (GST_IS_TUNER (gst_rec_get_video_source (conf->rec))) {
    tuner = GST_TUNER (gst_rec_get_video_source (conf->rec));
  }
  if (gtk_combo_box_get_active_iter (box, &iter) && tuner != NULL) {
    /* get selection */
    gtk_tree_model_get (gtk_combo_box_get_model (box), &iter,
        1, &current_norm, -1);
    gconf_client_set_string (conf->client, GST_REC_KEY_VIDEO_NORM,
        current_norm->label, NULL);
    
    GST_INFO ("Video norm %s selected", current_norm->label);
  } else {
    gconf_client_set_string (conf->client, GST_REC_KEY_VIDEO_NORM, "", NULL);
    GST_INFO ("Video norm (None) selected");
  }
}

GtkWidget *
gst_rec_configuration_get_video_source_page (GstRecConfiguration * conf)
{
  GList *video_sources = gst_rec_list_video_sources ();
  GtkWidget *table, *label, *menuwidget;
  gulong id;

  /* table container */
  table = gtk_table_new (4, 2, FALSE);
  conf->video_sources_page.table = GTK_TABLE (table);

  /* device selection */
  conf->video_sources.selection_menu =
      gst_rec_configuration_add_device_selection (conf, table, 0,
      video_sources,
      gst_rec_get_video_source (conf->rec),
      G_CALLBACK (cb_video_source_selected));

  /* video input */
  label = conf->video_inputs.title = gtk_label_new (_("Video input:"));
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, GTK_MISC (label)->yalign);
  gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
  gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 1, 2);
  menuwidget = gst_rec_combo_box_new_with_renderer ();
  gtk_table_attach_defaults (GTK_TABLE (table), menuwidget, 1, 2, 1, 2);
  g_signal_connect (G_OBJECT (menuwidget), "changed",
      G_CALLBACK (cb_video_input_selected), conf);
  conf->video_inputs.selection_menu = GTK_COMBO_BOX (menuwidget);
  gst_rec_configuration_set_video_input_menu (conf);

  /* video norms */
  label = conf->video_norms.title = gtk_label_new (_("Video norm:"));
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, GTK_MISC (label)->yalign);
  gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
  gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 2, 3);
  menuwidget = gst_rec_combo_box_new_with_renderer ();
  gtk_table_attach_defaults (GTK_TABLE (table), menuwidget, 1, 2, 2, 3);
  g_signal_connect (G_OBJECT (menuwidget), "changed",
      G_CALLBACK (cb_video_norm_selected), conf);
  conf->video_norms.selection_menu = GTK_COMBO_BOX (menuwidget);
  gst_rec_configuration_set_video_norm_menu (conf);

  /* video size selection */
  gst_rec_configuration_add_video_size_selection (conf, table, 3);

  /* signal handler if video source has changed */
  id = g_signal_connect (G_OBJECT (conf->rec), "video_source_changed",
      G_CALLBACK (cb_video_source_changed), conf);
  conf->ids = g_list_append (conf->ids, (gpointer) id);

  /* make sure that gconf is still in sync with conf */
  g_signal_emit_by_name (conf->video_inputs.selection_menu, "changed");
  g_signal_emit_by_name (conf->video_norms.selection_menu, "changed");
  
  return table;
}

static void
gst_rec_configuration_set_audio_input_menu (GstRecConfiguration * conf)
{
  GstElement *audio_source;
  GstMixer *mixer;
  GtkListStore *model;
  const GList *item, *list;
  GtkTreeIter selectiter, listiter;
  gboolean select = FALSE;

  /* if there was a menu, clean up old references */
  if (gtk_combo_box_get_model (conf->audio_inputs.selection_menu))
    gtk_tree_model_foreach (gtk_combo_box_get_model (conf->audio_inputs.
            selection_menu), cb_cleanup, NULL);

  audio_source = gst_rec_get_audio_source (conf->rec);
  if (!audio_source || !GST_IS_MIXER (audio_source)) {
    gtk_widget_hide (GTK_WIDGET (conf->audio_inputs.selection_menu));
    gtk_widget_hide (conf->audio_inputs.title);
    gtk_table_set_row_spacing (conf->audio_sources_page.table, 1, 0);
    gtk_combo_box_set_model (conf->audio_inputs.selection_menu,
        GTK_TREE_MODEL (gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_POINTER)));
    return;
  }

  gtk_widget_show (conf->audio_inputs.title);
  gtk_table_set_row_spacing (conf->audio_sources_page.table, 1,
      gtk_table_get_default_row_spacing (conf->audio_sources_page.table));

  mixer = GST_MIXER (audio_source);
  list = gst_mixer_list_tracks (mixer);

  model = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_POINTER);
  for (item = list; item != NULL; item = item->next) {
    GstMixerTrack *track = item->data;

    gtk_list_store_append (model, &listiter);
    gtk_list_store_set (model, &listiter, 0, track->label, 1, track, -1);
    g_object_ref (G_OBJECT (track));
    if (!select ||
        GST_MIXER_TRACK_HAS_FLAG (track, GST_MIXER_TRACK_RECORD)) {
      selectiter = listiter;
      select = TRUE;
    }
  }
  gtk_combo_box_set_model (conf->audio_inputs.selection_menu,
      GTK_TREE_MODEL (model));
  if (select)
    gtk_combo_box_set_active_iter (conf->audio_inputs.selection_menu,
        &selectiter);
  gtk_widget_show_all (GTK_WIDGET (conf->audio_inputs.selection_menu));
}

static void
gst_rec_configuration_set_audio_format_range (GstRecConfiguration * conf)
{
  GtkListStore *model;
  GstElement *audio_source;
  GstCaps *caps;
  GstStructure *structure;
  const GValue *cvalue, *rvalue;
  gint i;
  guint current_samplerate, current_channels;
  GtkTreeIter listiter, selectiter;
  gboolean select = FALSE;
  guint samplerates[] = { 8000, 11025, 16000, 22050,
    32000, 44100, 48000, 0
  };
  struct
  {
    guint num;
    gchar *name;
  }
  channels[] = {
    {
    1, "Mono"}
    , {
    2, "Stereo"}
    , {
    0}
  };

  current_samplerate = gst_rec_get_audio_samplerate (conf->rec);
  current_channels = gst_rec_get_audio_channels (conf->rec);

  audio_source = gst_rec_get_audio_source (conf->rec);
  if (!audio_source) {
    gtk_widget_set_sensitive (GTK_WIDGET (conf->audio_samplerates.
            selection_menu), FALSE);
    gtk_widget_set_sensitive (GTK_WIDGET (conf->audio_channels.selection_menu),
        FALSE);
    gtk_widget_set_sensitive (GTK_WIDGET (conf->audio_samplerates.title),
        FALSE);
    gtk_widget_set_sensitive (GTK_WIDGET (conf->audio_channels.title), FALSE);
    return;
  }

  gtk_widget_set_sensitive (GTK_WIDGET (conf->audio_samplerates.selection_menu),
      TRUE);
  gtk_widget_set_sensitive (GTK_WIDGET (conf->audio_channels.selection_menu),
      TRUE);
  gtk_widget_set_sensitive (GTK_WIDGET (conf->audio_samplerates.title), TRUE);
  gtk_widget_set_sensitive (GTK_WIDGET (conf->audio_channels.title), TRUE);

  caps = gst_pad_get_caps (gst_element_get_pad (audio_source, "src"));
  structure = gst_caps_get_structure (caps, 0);
  cvalue = gst_structure_get_value (structure, "channels");
  rvalue = gst_structure_get_value (structure, "rate");
  if (!cvalue || !rvalue ||
      (!G_TYPE_CHECK_VALUE_TYPE (rvalue, GST_TYPE_INT_RANGE) &&
          !G_TYPE_CHECK_VALUE_TYPE (rvalue, G_TYPE_INT) &&
          !G_TYPE_CHECK_VALUE_TYPE (rvalue, GST_TYPE_LIST)) ||
      (!G_TYPE_CHECK_VALUE_TYPE (cvalue, GST_TYPE_INT_RANGE) &&
          !G_TYPE_CHECK_VALUE_TYPE (cvalue, G_TYPE_INT) &&
          !G_TYPE_CHECK_VALUE_TYPE (cvalue, GST_TYPE_LIST))) {
    gchar *a = gst_value_serialize (rvalue), *b = gst_value_serialize (cvalue);

    g_warning ("No channels or rate properties! "
        "Audio format selection will break: %s & %s!", a, b);
    g_free (a);
    g_free (b);
    return;
  }

  model = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_INT);
  for (i = 0; samplerates[i] != 0; i++) {
    guint samplerate = samplerates[i];
    gchar *str;

    if (!gst_rec_value_is_subset (rvalue, samplerate))
      continue;

    gtk_list_store_append (model, &listiter);
    str = g_strdup_printf ("%u", samplerate);
    gtk_list_store_set (model, &listiter, 0, str, 1, samplerate, -1);
    g_free (str);
    if (!select || current_samplerate == samplerate) {
      selectiter = listiter;
      select = TRUE;
    }
  }
  gtk_combo_box_set_model (conf->audio_samplerates.selection_menu,
      GTK_TREE_MODEL (model));
  if (select)
    gtk_combo_box_set_active_iter (conf->audio_samplerates.selection_menu,
        &selectiter);
  gtk_widget_show_all (GTK_WIDGET (conf->audio_samplerates.selection_menu));

  select = FALSE;
  model = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_POINTER);
  for (i = 0; channels[i].num != 0; i++) {
    guint channels_num = channels[i].num;
    gchar *str = channels[i].name;

    if (!gst_rec_value_is_subset (cvalue, channels_num))
      continue;

    gtk_list_store_append (model, &listiter);
    gtk_list_store_set (model, &listiter, 0, str, 1, channels_num, -1);
    if (!select || current_channels == channels_num) {
      selectiter = listiter;
      select = TRUE;
    }
  }
  gtk_combo_box_set_model (conf->audio_channels.selection_menu,
      GTK_TREE_MODEL (model));
  if (select)
    gtk_combo_box_set_active_iter (conf->audio_channels.selection_menu,
        &selectiter);
  gtk_widget_show_all (GTK_WIDGET (conf->audio_channels.selection_menu));

  /* free caps */
  gst_caps_free (caps);
}

static void
cb_audio_source_selected (GtkComboBox * box, gpointer data)
{
  GstRecConfiguration *conf = GST_REC_CONFIGURATION (data);
  GstRecAudioSource *source;
  gchar *config;
  GtkTreeIter iter;
  gboolean ret;

  /* get selection */
  ret = gtk_combo_box_get_active_iter (box, &iter);
  g_return_if_fail (ret);
  gtk_tree_model_get (gtk_combo_box_get_model (box), &iter, 1, &source, -1);

  /* set newly selected video source */
  config = gst_rec_audio_source_to_config (source);
  gconf_client_set_string (conf->client, GST_REC_KEY_AUDIO_SOURCE,
      config, NULL);
  g_free (config);

  GST_INFO ("Selected audio source %s", source->name);
}

static void
cb_audio_source_changed (GstRec * rec, GstElement * audio_source, gpointer data)
{
  GstRecConfiguration *conf = GST_REC_CONFIGURATION (data);

  /* update inputs/format widgets */
  gst_rec_configuration_set_audio_input_menu (conf);
  gst_rec_configuration_set_audio_format_range (conf);

  /* And change muxer menu accordingly - first emit signal in
   * case nothing changes. */
  g_signal_emit_by_name (rec, "muxer_changed", rec->muxer);
  gst_rec_configuration_set_muxer_menu (conf);
}

static void
cb_audio_input_selected (GtkComboBox * box, gpointer data)
{
  GstRecConfiguration *conf = GST_REC_CONFIGURATION (data);
  GstMixer *mixer = GST_MIXER (gst_rec_get_audio_source (conf->rec));
  GstMixerTrack *current_track = NULL;
  GtkTreeIter iter;

  if (gtk_combo_box_get_active_iter (box, &iter) && mixer != NULL) {
    gtk_tree_model_get (gtk_combo_box_get_model (box), &iter,
        1, &current_track, -1);

    /* FIXME: we might want to use tack->num_channels and use this
     * for mono/stereo (channels) selection down. If we've got mono
     * input on a soundcard that - in essence - supports stereo,
     * then why give this (stereo) option? It's useless. */

    gconf_client_set_string (conf->client, GST_REC_KEY_AUDIO_INPUT,
        current_track->label, NULL);
    
    GST_INFO ("Audio input %s selected", current_track->label);
  } else {
    gconf_client_set_string (conf->client, GST_REC_KEY_AUDIO_INPUT, "", NULL);
  }
}

static void
cb_audio_format_selected (GtkComboBox * box, gpointer data)
{
  GstRecConfiguration *conf = GST_REC_CONFIGURATION (data);
  gint val;
  GtkTreeIter iter;

  /* This callback will be called when we're building up
   * the list. Since we don't want to crash, we don't
   * enter the callback yet. It will be called again when
   * all widgets are finished. */
  //if (!conf->audio_samplerates.values || !conf->audio_channels.values)
  //return;

  if (gtk_combo_box_get_active_iter (box, &iter)) {
    gtk_tree_model_get (gtk_combo_box_get_model (box), &iter, 1, &val, -1);

    if (box == conf->audio_channels.selection_menu) {
      gconf_client_set_int (conf->client, GST_REC_KEY_AUDIO_CHANNELS, val, NULL);
      
      GST_INFO ("Audio channels %d selected", val);
    } else if (box == conf->audio_samplerates.selection_menu) {
      gconf_client_set_int (conf->client, GST_REC_KEY_AUDIO_SAMPLERATE,
			    val, NULL);
      
      GST_INFO ("Audio samplerate %d selected", val);
    } else {
      g_warning ("Unknown optionmenu in audio-format");
    }
  } else {
    if (box == conf->audio_channels.selection_menu) {
      gconf_client_set_int (conf->client, GST_REC_KEY_AUDIO_CHANNELS, conf->rec->audio_channels, NULL);

      GST_INFO ("Audio channels defaulted to %d", conf->rec->audio_channels);
    } else if (box == conf->audio_samplerates.selection_menu) {
      gconf_client_set_int (conf->client, GST_REC_KEY_AUDIO_SAMPLERATE,
			    conf->rec->audio_samplerate, NULL);
      
      GST_INFO ("Audio samplerate defaulted to &d", conf->rec->audio_samplerate);
    } else {
      g_warning ("Unknown optionmenu in audio-format");
    }
  }
}

static void
gst_rec_configuration_add_audio_format_selection (GstRecConfiguration * conf,
    GtkWidget * table, guint row1, guint row2)
{
  GtkWidget *label, *menuwidget;

  /* label1 */
  label = gtk_label_new (_("Audio samplerate:"));
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, GTK_MISC (label)->yalign);
  gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
  gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, row1, row1 + 1);
  gtk_widget_show (label);
  conf->audio_samplerates.title = label;

  /* label1 */
  label = gtk_label_new (_("Audio channels:"));
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, GTK_MISC (label)->yalign);
  gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
  gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, row2, row2 + 1);
  gtk_widget_show (label);
  conf->audio_channels.title = label;

  /* FIXME: I'm fully aware that a spinbutton (or a comboboxentry)
   * would also be appropriate for samplerate selection. Both
   * have several issues, though. A combobox allows any text
   * to be entered, and has no arrows. A spinbutton has no way
   * of giving a list of "preferred" samplerates. Therefore, I
   * chose an optionmenu. If there's a better widget to make a
   * samplerate selection, please tell me.
   *
   * Maybe a spinbutton for channels also makes sense. Then
   * again, this has no way of giving labels to the numbers. */

  /* audio format menus */
  menuwidget = gst_rec_combo_box_new_with_renderer ();
  gtk_table_attach_defaults (GTK_TABLE (table), menuwidget,
      1, 2, row1, row1 + 1);
  gtk_widget_show (menuwidget);
  g_signal_connect (G_OBJECT (menuwidget), "changed",
      G_CALLBACK (cb_audio_format_selected), conf);
  conf->audio_samplerates.selection_menu = GTK_COMBO_BOX (menuwidget);

  menuwidget = gst_rec_combo_box_new_with_renderer ();
  gtk_table_attach_defaults (GTK_TABLE (table), menuwidget,
      1, 2, row2, row2 + 1);
  gtk_widget_show (menuwidget);
  g_signal_connect (G_OBJECT (menuwidget), "changed",
      G_CALLBACK (cb_audio_format_selected), conf);
  conf->audio_channels.selection_menu = GTK_COMBO_BOX (menuwidget);

  /* defaults */
  gst_rec_configuration_set_audio_format_range (conf);

  /* make sure gconf is in sync with conf */
  g_signal_emit_by_name (conf->audio_samplerates.selection_menu, "changed");
  g_signal_emit_by_name (conf->audio_channels.selection_menu, "changed");
}

GtkWidget *
gst_rec_configuration_get_audio_source_page (GstRecConfiguration * conf)
{
  GList *audio_sources = gst_rec_list_audio_sources ();
  GtkWidget *table, *menuwidget, *label;
  gulong id;

  /* table container */
  table = gtk_table_new (4, 2, FALSE);
  conf->audio_sources_page.table = GTK_TABLE (table);

  /* device selection */
  conf->audio_sources.selection_menu =
      gst_rec_configuration_add_device_selection (conf, table, 0,
      audio_sources,
      gst_rec_get_audio_source (conf->rec),
      G_CALLBACK (cb_audio_source_selected));

  /* audio input */
  label = conf->audio_inputs.title = gtk_label_new (_("Audio input:"));
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, GTK_MISC (label)->yalign);
  gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
  gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 1, 2);
  menuwidget = gst_rec_combo_box_new_with_renderer ();
  gtk_table_attach_defaults (GTK_TABLE (table), menuwidget, 1, 2, 1, 2);
  g_signal_connect (G_OBJECT (menuwidget), "changed",
      G_CALLBACK (cb_audio_input_selected), conf);
  conf->audio_inputs.selection_menu = GTK_COMBO_BOX (menuwidget);
  gst_rec_configuration_set_audio_input_menu (conf);

  /* audio format */
  gst_rec_configuration_add_audio_format_selection (conf, table, 2, 3);

  /* signal handler if video source has changed */
  id = g_signal_connect (G_OBJECT (conf->rec), "audio_source_changed",
      G_CALLBACK (cb_audio_source_changed), conf);
  conf->ids = g_list_append (conf->ids, (gpointer) id);

  /* make sure gconf is in sync with conf */
  g_signal_emit_by_name (conf->audio_inputs.selection_menu, "changed");

  return table;
}

/*
 * Metadata stuff.
 */

static void
cb_tag_changed (GstRecConfiguration * conf,
    const gchar * tag, const gchar *value)
{
  gchar *s;

  s = g_strdup_printf (GST_REC_KEY_METADATA "%s", tag);
  gconf_client_set_string (conf->client, s, value, NULL);
  g_free (s);
}

static void
cb_tag_entry_changed (GtkEntry * entry, gpointer data)
{
  cb_tag_changed (GST_REC_CONFIGURATION (data),
      g_object_get_data (G_OBJECT (entry), "tag"),
      gtk_entry_get_text (entry));
}

static void G_GNUC_UNUSED
cb_tag_buf_changed (GtkTextBuffer * buf, gpointer data)
{
  GtkTextIter s, e;
  gchar *a;

  gtk_text_buffer_get_bounds (buf, &s, &e);
  a = gtk_text_buffer_get_text (buf, &s, &e, FALSE);
  cb_tag_changed (GST_REC_CONFIGURATION (data),
      g_object_get_data (G_OBJECT (buf), "tag"), a);
  g_free (a);
}

GtkWidget *
gst_rec_configuration_get_metadata_page (GstRecConfiguration * conf)
{
  GtkWidget *table = gtk_table_new (3, 2, FALSE), *label, *entry;
  gint n;
  struct {
    const gchar *tag, *label;
  } tags[] = {
    { GST_TAG_TITLE, _("Title") },
    { GST_TAG_ARTIST, _("Artist") },
    { GST_TAG_DESCRIPTION, _("Description") },
    { NULL, NULL }
  };
  gchar *s;
  const gchar *value;

  for (n = 0; tags[n].tag != NULL; n++) {
    value = gst_rec_get_tag (conf->rec, tags[n].tag);
    s = g_strdup_printf ("%s: ", tags[n].label);
    label = gtk_label_new (s);
    g_free (s);
    gtk_misc_set_alignment (GTK_MISC (label), 0.0, GTK_MISC (label)->yalign);
    gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
    if (n == 2 && 0 /* FIXME */) {
      entry = gtk_text_view_new ();
      if (value) {
        gtk_text_buffer_set_text (
            gtk_text_view_get_buffer (GTK_TEXT_VIEW (entry)),
            value, strlen (value));
      }
      g_signal_connect (gtk_text_view_get_buffer (GTK_TEXT_VIEW (entry)),
          "changed", G_CALLBACK (cb_tag_buf_changed), conf);
      g_object_set_data (
          G_OBJECT (gtk_text_view_get_buffer (GTK_TEXT_VIEW (entry))),
          "tag", (gpointer) tags[n].tag);
    } else {
      entry = gtk_entry_new ();
      if (value) {
        gtk_entry_set_text (GTK_ENTRY (entry), value);
      }
      g_signal_connect (entry, "changed",
          G_CALLBACK (cb_tag_entry_changed), conf);
      /* make sure gconf is in sync with conf */
      g_object_set_data (G_OBJECT (entry), "tag", (gpointer) tags[n].tag);
    }

    gtk_table_attach (GTK_TABLE (table), label, 0, 1, n, n + 1,
        GTK_FILL, 0, 0, 0);
    gtk_table_attach_defaults (GTK_TABLE (table), entry, 1, 2, n, n + 1);

    gtk_widget_show (label);
    gtk_widget_show (entry);
  }

  return table;
}
