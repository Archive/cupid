/* GStreamer Recorder
 * (c) 2003 Ronald Bultje <rbultje@ronald.bitfreak.net>
 *
 * window.h: window creation function declaration
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GST_REC_WINDOW_H__
#define __GST_REC_WINDOW_H__

#include <gst/gst.h>
#include <libgnomeui/libgnomeui.h>
#include <libgnomevfs/gnome-vfs.h>

#include "record.h"
#include "sliders.h"
#include "stats.h"
#include "tv.h"

G_BEGIN_DECLS

#define GST_REC_TYPE_WINDOW \
  (gst_rec_window_get_type ())
#define GST_REC_WINDOW(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GST_REC_TYPE_WINDOW, GstRecWindow))
#define GST_REC_WINDOW_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), GST_REC_TYPE_WINDOW, GstRecWindowClass))
#define GST_REC_IS_WINDOW(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GST_REC_TYPE_WINDOW))
#define GST_REC_IS_WINDOW_CLASS(obj) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), GST_REC_TYPE_WINDOW))

/*
 * Popup error.
 */
                                                                                
#define gst_rec_popup_error(win, msg, args...) \
  do { \
    GtkWidget *window; \
    window = gtk_message_dialog_new (GTK_WINDOW (win), \
                                     GTK_DIALOG_MODAL, \
                                     GTK_MESSAGE_ERROR, \
                                     GTK_BUTTONS_CLOSE, \
                                     msg, ##args); \
    gtk_widget_show (window); \
    gtk_dialog_run (GTK_DIALOG (window)); \
    gtk_widget_destroy (window); \
  } while (0);

typedef struct _GstRecWindow
{
  GnomeApp parent;

  /* recording object */
  GstRec *rec;
  GstElement *output;

  /* child widgets */
  GstRecTv *tv;
  GstRecStats *stats;
  GtkEntry *fileentry;
  GtkWidget *box, *mainbox, *filebox;

  /* other stuff */
  GnomeVFSURI *file;
  GConfClient *client;

  /* whether we are currently in fullscreen mode */
  gboolean fullscreen;
} GstRecWindow;

typedef struct _GstRecWindowClass
{
  GnomeAppClass klass;
} GstRecWindowClass;

GType gst_rec_window_get_type (void);
void create_window (void);

#endif /* __GST_REC_WINDOW_H__ */
