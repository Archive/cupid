/* GStreamer Recorder
 * (c) 2003 Ronald Bultje <rbultje@ronald.bitfreak.net>
 *
 * parameter.h: definition of a widget to UI'ify GObject parameters
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gst/gst.h>
#include <gtk/gtk.h>

#ifndef __GST_REC_PARAMETER_H__
#define __GST_REC_PARAMETER_H__

#define GST_REC_TYPE_PARAMETER \
  (gst_rec_parameter_get_type())
#define GST_REC_PARAMETER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_REC_TYPE_PARAMETER, \
			      GstRecParameter))
#define GST_REC_PARAMETER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass), GST_REC_TYPE_PARAMETER, \
			   GstRecParameterClass))
#define GST_REC_IS_PARAMETER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_REC_TYPE_PARAMETER))
#define GST_REC_IS_PARAMETER_CLASS(obj) \
  (G_TYPE_CHECK_CLASS_TYPE((klass), GST_REC_TYPE_PARAMETER))

typedef struct _GstRecParameter
{
  GtkHBox parent;

  /* object/parameter on which we operate */
  GstElement *element;
  GParamSpec *pspec;
} GstRecParameter;

typedef struct _GstRecParameterClass
{
  GtkHBoxClass parent;
} GstRecParameterClass;

GType gst_rec_parameter_get_type (void);

/*
 * Create new parameter widget. Instant-apply.
 */
GtkWidget *gst_rec_parameter_new (GstElement * element, GParamSpec * pspec);

#endif /* __GST_REC_PARAMETER_H__ */
