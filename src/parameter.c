/* GStreamer Recorder
 * (c) 2003 Ronald Bultje <rbultje@ronald.bitfreak.net>
 *
 * parameter.c: parameter widget (loosely based on gst-editor's)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib/gi18n.h>

#include "debug.h"
#include "parameter.h"

static void gst_rec_parameter_class_init (GstRecParameterClass * klass);
static void gst_rec_parameter_init (GstRecParameter * param);
static void gst_rec_parameter_dispose (GObject * object);
static void gst_rec_parameter_set_widget (GstRecParameter * param);

static GtkContainerClass *parent_class = NULL;

GType
gst_rec_parameter_get_type (void)
{
  static GType gst_rec_parameter_type = 0;

  if (!gst_rec_parameter_type) {
    static const GTypeInfo gst_rec_parameter_info = {
      sizeof (GstRecParameterClass),
      NULL,
      NULL,
      (GClassInitFunc) gst_rec_parameter_class_init,
      NULL,
      NULL,
      sizeof (GstRecParameter),
      0,
      (GInstanceInitFunc) gst_rec_parameter_init,
      NULL
    };

    gst_rec_parameter_type =
        g_type_register_static (GTK_TYPE_HBOX,
        "GstRecParameter", &gst_rec_parameter_info, 0);
  }

  return gst_rec_parameter_type;
}

static void
gst_rec_parameter_class_init (GstRecParameterClass * klass)
{
  GObjectClass *gobject_class = (GObjectClass *) klass;

  parent_class = g_type_class_ref (GTK_TYPE_HBOX);

  gobject_class->dispose = gst_rec_parameter_dispose;
}

static void
gst_rec_parameter_init (GstRecParameter * param)
{
  param->pspec = NULL;
  param->element = NULL;
}

static void
gst_rec_parameter_dispose (GObject * object)
{
  GstRecParameter *param = GST_REC_PARAMETER (object);

  gst_object_replace ((GstObject **) & param->element, NULL);

  G_OBJECT_CLASS (parent_class)->dispose (object);
}

GtkWidget *
gst_rec_parameter_new (GstElement * element, GParamSpec * pspec)
{
  GstRecParameter *param = g_object_new (GST_REC_TYPE_PARAMETER, NULL);

  gst_object_replace ((GstObject **) & param->element, GST_OBJECT (element));
  param->pspec = pspec;

  gst_rec_parameter_set_widget (param);

  return GTK_WIDGET (param);
}

/*
 * Callback + implementation for bool values.
 */

static void
cb_bool_toggled (GtkToggleButton * button, gpointer data)
{
  GstRecParameter *param = GST_REC_PARAMETER (data);
  gboolean active = gtk_toggle_button_get_active (button);

  g_object_set (G_OBJECT (param->element), param->pspec->name, active, NULL);
}

static GtkWidget *
gst_rec_parameter_set_widget_bool (GstRecParameter * param)
{
  gboolean active = FALSE;
  GtkWidget *button;

  if (param->pspec->flags & G_PARAM_READABLE) {
    g_object_get (G_OBJECT (param->element), param->pspec->name, &active, NULL);
  }
  button = gtk_check_button_new ();
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), active);
  g_signal_connect (G_OBJECT (button), "toggled",
      G_CALLBACK (cb_bool_toggled), param);

  return button;
}

/*
 * Callback + implementation for int/float values.
 */

static void
cb_int_value_changed (GtkSpinButton * button, gpointer data)
{
  GstRecParameter *param = GST_REC_PARAMETER (data);

  switch (param->pspec->value_type) {
#define CASE_TYPE(ctype, ntype, dtype) \
    case G_TYPE_##ctype: { \
      ntype _val = gtk_spin_button_get_value (button); \
      g_object_set (G_OBJECT (param->element), \
		    param->pspec->name, _val, NULL); \
      break; \
    }

      CASE_TYPE (FLOAT, gfloat, 2);
      CASE_TYPE (DOUBLE, gdouble, 2);
      CASE_TYPE (LONG, glong, 0);
      CASE_TYPE (ULONG, gulong, 0);
      CASE_TYPE (INT, gint, 0);
      CASE_TYPE (UINT, guint, 0);
      CASE_TYPE (INT64, gint64, 0);
      CASE_TYPE (UINT64, guint64, 0);

#undef CASE_TYPE
  }
}

static GtkWidget *
gst_rec_parameter_set_widget_int (GstRecParameter * param)
{
  GtkWidget *button;
  gdouble min = 0, max = 0, step = 1, val = 0, digits = 0;

  switch (param->pspec->value_type) {
#define CASE_TYPE(ctype, ntype, dtype) \
    case G_TYPE_##ctype: { \
      ntype _val = 0, _min, _max; \
      _min = G_PARAM_SPEC_##ctype (param->pspec)->minimum; \
      _max = G_PARAM_SPEC_##ctype (param->pspec)->maximum; \
      if (param->pspec->flags & G_PARAM_READABLE) { \
        g_object_get (G_OBJECT (param->element), \
		      param->pspec->name, &_val, NULL); \
      } \
      min = _min; max = _max; val = _val; digits = dtype; \
      break; \
    }

      CASE_TYPE (FLOAT, gfloat, 2);
      CASE_TYPE (DOUBLE, gdouble, 2);
      CASE_TYPE (LONG, glong, 0);
      CASE_TYPE (ULONG, gulong, 0);
      CASE_TYPE (INT, gint, 0);
      CASE_TYPE (UINT, guint, 0);
      CASE_TYPE (INT64, gint64, 0);
      CASE_TYPE (UINT64, guint64, 0);

#undef CASE_TYPE
  }

  button = gtk_spin_button_new_with_range (min, max, step);
  gtk_spin_button_set_digits (GTK_SPIN_BUTTON (button), digits);
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (button), val);
  g_signal_connect (G_OBJECT (button), "value_changed",
      G_CALLBACK (cb_int_value_changed), param);

  return button;
}

/*
 * Callback + implementation for bool values.
 */

static void
cb_string_activated (GtkEntry * entry, gpointer data)
{
  GstRecParameter *param = GST_REC_PARAMETER (data);
  const gchar *content = gtk_entry_get_text (entry);

  g_object_set (G_OBJECT (param->element), param->pspec->name, content, NULL);
}

static GtkWidget *
gst_rec_parameter_set_widget_string (GstRecParameter * param)
{
  GtkWidget *entry;
  gchar *content = "";

  if (param->pspec->flags & G_PARAM_READABLE) {
    g_object_get (G_OBJECT (param->element),
        param->pspec->name, &content, NULL);
  }
  entry = gtk_entry_new ();
  gtk_entry_set_text (GTK_ENTRY (entry), content);
  g_signal_connect (G_OBJECT (entry), "activate",
      G_CALLBACK (cb_string_activated), param);

  return entry;
}

/*
 * Callback + implementation for enumerations.
 */

static void
cb_menu_changed (GtkComboBox * box, gpointer data)
{
  GstRecParameter *param = GST_REC_PARAMETER (data);
  GEnumValue *value;
  GtkTreeIter iter;
  gboolean ret;

  /* get selection */
  ret = gtk_combo_box_get_active_iter (box, &iter);
  g_return_if_fail (ret);
  gtk_tree_model_get (gtk_combo_box_get_model (box), &iter, 1, &value, -1);

  g_object_set (G_OBJECT (param->element),
      param->pspec->name, value->value, NULL);
}

static GtkWidget *
gst_rec_parameter_set_widget_enum (GstRecParameter * param)
{
  GtkWidget *menuwidget;
  GtkListStore *model;
  GtkTreeIter selectiter, listiter;
  gboolean select = FALSE;
  gint n, active;
  GEnumClass *klass =
      G_ENUM_CLASS (g_type_class_peek (param->pspec->value_type));
  GtkCellRenderer *renderer;

  if (param->pspec->flags & G_PARAM_READABLE) {
    g_object_get (G_OBJECT (param->element), param->pspec->name, &active, NULL);
  }

  menuwidget = gtk_combo_box_new ();
  renderer = gtk_cell_renderer_text_new ();
  gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (menuwidget), renderer, TRUE);
  gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (menuwidget), renderer,
      "text", 0, NULL);

  model = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_POINTER);
  for (n = 0; n < klass->n_values; n++) {
    GEnumValue *value = &klass->values[n];

    if (!value) {
      g_warning ("No value on dot.%d in value array\n", n);
      continue;
    }

    gtk_list_store_append (model, &listiter);
    gtk_list_store_set (model, &listiter, 0, value->value_nick, 1, value, -1);
    if (active == value->value) {
      selectiter = listiter;
      select = TRUE;
    }
  }
  gtk_combo_box_set_model (GTK_COMBO_BOX (menuwidget), GTK_TREE_MODEL (model));
  if (select)
    gtk_combo_box_set_active_iter (GTK_COMBO_BOX (menuwidget), &selectiter);

  g_signal_connect (G_OBJECT (menuwidget), "changed",
      G_CALLBACK (cb_menu_changed), param);

  return menuwidget;
}

/*
 * This function is separated so that - if that might ever
 * be needed - we can change widget on-the-fly. I don't think
 * I'll ever use it, but who knows...
 */

static void
gst_rec_parameter_set_widget (GstRecParameter * param)
{
  GParamSpec *pspec = param->pspec;
  GtkWidget *widget = NULL;

  /* FIXME: clean up old */

  /* draw only if needed */
  if (!param->pspec)
    return;

  /* which widget? */
  switch (G_TYPE_FUNDAMENTAL (pspec->value_type)) {
      /* general FIXME: connect to the notify signal to keep track
       * of when the property changes value without us doing it. */

    case G_TYPE_BOOLEAN:
      widget = gst_rec_parameter_set_widget_bool (param);
      break;

    case G_TYPE_FLOAT:
    case G_TYPE_DOUBLE:
    case G_TYPE_INT:
    case G_TYPE_UINT:
    case G_TYPE_LONG:
    case G_TYPE_ULONG:
    case G_TYPE_INT64:
    case G_TYPE_UINT64:
      widget = gst_rec_parameter_set_widget_int (param);
      break;

    case G_TYPE_STRING:
      widget = gst_rec_parameter_set_widget_string (param);
      break;

#if 0
    case G_TYPE_FLAGS:
      ..break;
#endif

    case G_TYPE_ENUM:
      widget = gst_rec_parameter_set_widget_enum (param);
      break;

    default:
      GST_WARNING ("Value type 0x%lx for property %s unknown",
          pspec->value_type, pspec->name);
      widget = gtk_label_new (_("unknown"));
      gtk_misc_set_alignment (GTK_MISC (widget), 0.0,
          GTK_MISC (widget)->yalign);
      gtk_label_set_justify (GTK_LABEL (widget), GTK_JUSTIFY_LEFT);
      break;
  }

  gtk_box_pack_start_defaults (GTK_BOX (param), widget);
  if (!(param->pspec->flags & G_PARAM_WRITABLE))
    gtk_widget_set_sensitive (widget, FALSE);
  gtk_widget_show (widget);
}
