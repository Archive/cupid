/* GStreamer Recorder
 * (c) 2003 Ronald Bultje <rbultje@ronald.bitfreak.net>
 *
 * snapshot.h: snapshot save dialog
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GST_REC_SNAPSHOT_H__
#define __GST_REC_SNAPSHOT_H__

#include <gtk/gtk.h>

#define GST_REC_TYPE_SNAPSHOT \
  (gst_rec_snapshot_get_type())
#define GST_REC_SNAPSHOT(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_REC_TYPE_SNAPSHOT, \
			      GstRecSnapshot))
#define GST_REC_SNAPSHOT_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass), GST_REC_TYPE_SNAPSHOT, \
			   GstRecSnapshotClass))
#define GST_REC_IS_SNAPSHOT(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_REC_TYPE_SNAPSHOT))
#define GST_REC_IS_SNAPSHOT_CLASS(obj) \
  (G_TYPE_CHECK_CLASS_TYPE((klass), GST_REC_TYPE_SNAPSHOT))

typedef struct _GstRecSnapshot GstRecSnapshot;

struct _GstRecSnapshot
{
  GtkDialog parent;

  /* the pixbuf */
  GdkPixbuf *pixbuf;

  /* location */
  GtkImage *image;
  GtkEntry *nameentry;
  GtkFileChooserButton *dirbutton;
};

typedef struct _GstRecSnapshotClass
{
  GtkDialogClass parent;
} GstRecSnapshotClass;

GType gst_rec_snapshot_get_type (void);

/*
 * Create new snapshot dialog.
 */
GtkWidget *gst_rec_snapshot_new (GdkPixbuf * pixbuf);

/*
 * Change pixbuf in dialog.
 */
void gst_rec_snapshot_set_pixbuf (GstRecSnapshot * snap, GdkPixbuf * pixbuf);

#endif /* __GST_REC_SNAPSHOT_H__ */
