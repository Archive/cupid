/* GStreamer Recorder
 * (c) 2003 Ronald Bultje <rbultje@ronald.bitfreak.net>
 *
 * sliders.h: a floating window with volume/colorbalance sliders
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gtk/gtk.h>
#include "configuration.h"

#ifndef __GST_REC_SLIDERS_H__
#define __GST_REC_SLIDERS_H__

#define GST_REC_TYPE_SLIDERS \
  (gst_rec_sliders_get_type())
#define GST_REC_SLIDERS(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_REC_TYPE_SLIDERS, \
			      GstRecSliders))
#define GST_REC_SLIDERS_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass), GST_REC_TYPE_SLIDERS, \
			   GstRecSlidersClass))
#define GST_REC_IS_SLIDERS(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_REC_TYPE_SLIDERS))
#define GST_REC_IS_SLIDERS_CLASS(obj) \
  (G_TYPE_CHECK_CLASS_TYPE((klass), GST_REC_TYPE_SLIDERS))

typedef struct _GstRecSliders GstRecSliders;

struct _GstRecSliders
{
  GtkWindow parent;

  GstElement *colorbalance, *audiovolume;

  GList *volumetracks, *colorbalancechannels;

  gboolean floating;
};

typedef struct _GstRecSlidersClass
{
  GtkWindowClass parent;
} GstRecSlidersClass;

GType gst_rec_sliders_get_type (void);

/*
 * Creates the window. Floating means that it will behave like
 * a menu, with no window borders etc.
 */
GtkWidget *gst_rec_sliders_new (GstElement * colorbalance,
    GstElement * audiovolume, gboolean floating);

/*
 * Position the window below/sideways of an existing widget.
 */
void gst_rec_sliders_attach (GstRecSliders * slider, GtkWidget * widget);

/*
 * Runs it and grabs input. Is destroyed on external mouseclick
 * if the window is floating...
 */
void gst_rec_sliders_run (GstRecSliders * sliders);

#endif /* __GST_REC_SLIDERS_H__ */
