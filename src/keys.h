/* GStreamer Recorder
 * (c) 2003 Ronald Bultje <rbultje@ronald.bitfreak.net>
 *
 * keys.h: macros for GConf keys
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GST_REC_KEYS_H__
#define __GST_REC_KEYS_H__

#define GST_REC_GCONF_KEY(key)     "/apps/" PACKAGE key
#define GST_REC_UI_GCONF_KEY(key)  GST_REC_GCONF_KEY ("/ui" key)
#define GST_REC_GST_GCONF_KEY(key) GST_REC_GCONF_KEY ("/gst" key)

/* base dir */
#define GST_REC_KEY_BASEDIR \
  GST_REC_GCONF_KEY ("")
#define GST_REC_KEY_UIDIR \
  GST_REC_UI_GCONF_KEY ("")
#define GST_REC_KEY_GSTDIR \
  GST_REC_GST_GCONF_KEY ("")

/* first-time setup key */
#define GST_REC_KEY_SETUP \
  GST_REC_GCONF_KEY ("/setup")

/* UI GConf key definitions */
#define GST_REC_KEY_MINIMAL_MODE \
  GST_REC_UI_GCONF_KEY ("/minimal_mode")
#define GST_REC_KEY_STATISTICS_VISIBILITY \
  GST_REC_UI_GCONF_KEY ("/statistics_visibility")

/* Gst backend GConf key definitions */
#define GST_REC_KEY_VIDEO_SOURCE \
  GST_REC_GST_GCONF_KEY ("/video_source")
#define GST_REC_KEY_AUDIO_SOURCE \
  GST_REC_GST_GCONF_KEY ("/audio_source")
#define GST_REC_KEY_VIDEO_ENCODER \
  GST_REC_GST_GCONF_KEY ("/video_encoder")
#define GST_REC_KEY_AUDIO_ENCODER \
  GST_REC_GST_GCONF_KEY ("/audio_encoder")
#define GST_REC_KEY_VIDEO_FILTER \
  GST_REC_GST_GCONF_KEY ("/video_filter")
#define GST_REC_KEY_AUDIO_FILTER \
  GST_REC_GST_GCONF_KEY ("/audio_filter")
#define GST_REC_KEY_MUXER \
  GST_REC_GST_GCONF_KEY ("/muxer")

/* basic pipeline configuration */
#define GST_REC_KEY_VIDEO_NORM \
  GST_REC_GST_GCONF_KEY ("/video_norm")
#define GST_REC_KEY_VIDEO_INPUT \
  GST_REC_GST_GCONF_KEY ("/video_input")
#define GST_REC_KEY_VIDEO_WIDTH \
  GST_REC_GST_GCONF_KEY ("/video_width")
#define GST_REC_KEY_VIDEO_HEIGHT \
  GST_REC_GST_GCONF_KEY ("/video_height")
#define GST_REC_KEY_VIDEO_FRAMERATE \
  GST_REC_GST_GCONF_KEY ("/video_framerate")
#define GST_REC_KEY_VIDEO_EMUMODE \
  GST_REC_GST_GCONF_KEY ("/video_emumode")

/* element interface-extensions */
#define GST_REC_KEY_AUDIO_INPUT \
  GST_REC_GST_GCONF_KEY ("/audio_input")
#define GST_REC_KEY_AUDIO_SAMPLERATE \
  GST_REC_GST_GCONF_KEY ("/audio_samplerate")
#define GST_REC_KEY_AUDIO_CHANNELS \
  GST_REC_GST_GCONF_KEY ("/audio_channels")

/* metadata */
#define GST_REC_KEY_METADATA \
  GST_REC_GST_GCONF_KEY ("/metadata_")

#endif /* __GST_REC_KEYS_H__ */
