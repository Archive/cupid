/* GStreamer Recorder
 * (c) 2004 Ronald Bultje <rbultje@ronald.bitfreak.net>
 *
 * convert.h: convert between colorspace formats
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GST_REC_CONVERT_H__
#define __GST_REC_CONVERT_H__

#include <gst/gst.h>

G_BEGIN_DECLS

GstBuffer * gst_rec_convert (GstBuffer * buf, GstCaps * from, GstCaps * to);

G_END_DECLS

#endif /* __GST_REC_CONVERT_H__ */
