/* GStreamer Recorder
 * (c) 2003 Ronald Bultje <rbultje@ronald.bitfreak.net>
 *
 * list.h: definition of lists of elements
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GST_REC_LIST_H__
#define __GST_REC_LIST_H__

#include <glib.h>
#include <gst/gst.h>

#include "record.h"

G_BEGIN_DECLS
/*
 * These are basically element templates, but there's some
 * application-specific information added to them with
 * respect to the default GStreamer element factories.
 */
#define GST_REC_TYPE_SOURCE \
  (gst_rec_source_get_type())
#define GST_REC_SOURCE(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_REC_TYPE_SOURCE, GstRecSource))
#define GST_REC_VIDEO_SOURCE(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_REC_TYPE_SOURCE, GstRecVideoSource))
#define GST_REC_AUDIO_SOURCE(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_REC_TYPE_SOURCE, GstRecAudioSource))
#define GST_REC_IS_SOURCE(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_REC_TYPE_SOURCE))
    typedef struct _GstRecSource
{
  GObject parent;

  /* descriptive name of the video source */
  gchar *name;

  /* the element's factory to create instances */
  GstElementFactory *factory;

  /* the device that belongs to it. Can be NULL */
  gchar *device;

  /* template caps */
  GstCaps *caps;
} GstRecSource, GstRecVideoSource, GstRecAudioSource;

#define GST_REC_TYPE_ENCODER \
  (gst_rec_encoder_get_type())
#define GST_REC_ENCODER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_REC_TYPE_ENCODER, GstRecEncoder))
#define GST_REC_VIDEO_ENCODER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_REC_TYPE_ENCODER, GstRecVideoEncoder))
#define GST_REC_AUDIO_ENCODER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_REC_TYPE_ENCODER, GstRecAudioEncoder))
#define GST_REC_IS_ENCODER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_REC_TYPE_ENCODER))

typedef struct _GstRecEncoder
{
  GObject parent;

  /* descriptive name of the audio source */
  gchar *name;

  /* the element's factory to create instances, if any */
  GstElementFactory *factory;

  /* filter caps, if any */
  GstCaps *filter;
} GstRecEncoder, GstRecVideoEncoder, GstRecAudioEncoder;

#define GST_REC_TYPE_MUXER \
  (gst_rec_muxer_get_type())
#define GST_REC_MUXER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_REC_TYPE_MUXER, GstRecMuxer))
#define GST_REC_IS_MUXER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_REC_TYPE_MUXER))

typedef struct _GstRecMuxer
{
  GObject parent;

  /* descriptive name of the audio source */
  gchar *name;

  /* the element's factory to create instances, if any */
  GstElementFactory *factory;
} GstRecMuxer;

GType gst_rec_source_get_type (void);
GType gst_rec_encoder_get_type (void);
GType gst_rec_muxer_get_type (void);

/*
 * Enumerate different possible elements (plus, optionally,
 * devices associated with specific instances).
 * Free using gst_rec_list_replace (&list, NULL).
 */
GList *gst_rec_list_video_sources (void);
GList *gst_rec_list_audio_sources (void);
GList *gst_rec_list_muxers (GstElement * video_source,
    GstElement * audio_source);
GList *gst_rec_list_video_encoders (GstElement * video_source,
    GstElement * muxer);
GList *gst_rec_list_audio_encoders (GstElement * audio_source,
    GstElement * muxer);

/*
 * Get rid of memory.
 */
void gst_rec_list_replace (GList ** old, GList * new);

/*
 * Create config entries ready to be written to (e.g.) GConf.
 */
gchar *gst_rec_source_to_config (GstRecSource * templ);
gchar *gst_rec_encoder_to_config (GstRecEncoder * templ);
gchar *gst_rec_muxer_to_config (GstRecMuxer * templ);
gchar *gst_rec_filter_to_config (GstRecEncoder * templ);

#define gst_rec_video_source_to_config(templ) \
  gst_rec_source_to_config (templ)
#define gst_rec_audio_source_to_config(templ) \
  gst_rec_source_to_config (templ)

#define gst_rec_video_encoder_to_config(templ) \
  gst_rec_encoder_to_config (templ)
#define gst_rec_audio_encoder_to_config(templ) \
  gst_rec_encoder_to_config (templ)

#define gst_rec_video_filter_to_config(templ) \
  gst_rec_filter_to_config (templ)
#define gst_rec_audio_filter_to_config(templ) \
  gst_rec_filter_to_config (templ)

/*
 * Create an element based on a GConf-sort config entry.
 */
GstElement *gst_rec_config_to_source (const gchar * config, gboolean audio);
GstElement *gst_rec_config_to_encoder (const gchar * config, gboolean audio);
GstElement *gst_rec_config_to_muxer (const gchar * config);
GstCaps *gst_rec_config_to_filter (const gchar * config, gboolean audio);

#define gst_rec_config_to_video_source(config) \
  gst_rec_config_to_source (config, FALSE)
#define gst_rec_config_to_audio_source(config) \
  gst_rec_config_to_source (config, TRUE)

#define gst_rec_config_to_video_encoder(config) \
  gst_rec_config_to_encoder (config, FALSE)
#define gst_rec_config_to_audio_encoder(config) \
  gst_rec_config_to_encoder (config, TRUE)

#define gst_rec_config_to_video_filter(config) \
  gst_rec_config_to_filter (config, FALSE)
#define gst_rec_config_to_audio_filter(config) \
  gst_rec_config_to_filter (config, TRUE)

/*
 * Get the GstRec{Muxer,{Video,Audio}{Source,Encoder,Filter}}
 * that makes for the currently selected one.
 */
GstRecSource *gst_rec_list_find_source (GstRec * rec,
    GList * list, GstElement * current);
GstRecEncoder *gst_rec_list_find_encoder (GstRec * rec,
    GList * list, GstElement * current, GstCaps * filter);
GstRecMuxer *gst_rec_list_find_muxer (GstRec * rec, GList * list);

#define gst_rec_list_find_video_source(rec,list) \
  ((GstRecVideoSource *) gst_rec_list_find_source (rec, list, \
				  gst_rec_get_video_source (rec)))
#define gst_rec_list_find_audio_source(rec,list) \
  ((GstRecAudioSource *) gst_rec_list_find_source (rec, list, \
				  gst_rec_get_audio_source (rec)))

#define gst_rec_list_find_video_encoder(rec,list) \
  ((GstRecVideoEncoder *) gst_rec_list_find_encoder (rec, list, \
				  gst_rec_get_video_encoder (rec), \
				  gst_rec_get_video_filter (rec)))
#define gst_rec_list_find_audio_encoder(rec,list) \
  ((GstRecAudioEncoder *) gst_rec_list_find_encoder (rec, list, \
				  gst_rec_get_audio_encoder (rec), \
				  gst_rec_get_audio_filter (rec)))

/*
 * Internal init call. Don't touch.
 */
void gst_rec_lists_init (void);

G_END_DECLS
#endif /* __GST_REC_LIST_H__ */
