/* GStreamer Recorder
 * (c) 2005 Ronald Bultje <rbultje@ronald.bitfreak.net>
 *
 * emulator.h: interface (xoverlay, volume [+ audio])
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GST_REC_EMULATOR_H__
#define __GST_REC_EMULATOR_H__

#include <gst/gst.h>

#define GST_REC_TYPE_EMULATOR \
  (gst_rec_emulator_get_type())
#define GST_REC_EMULATOR(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_REC_TYPE_EMULATOR, \
			      GstRecEmulator))
#define GST_REC_EMULATOR_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass), GST_REC_TYPE_EMULATOR, \
			   GstRecEmulatorClass))
#define GST_REC_IS_EMULATOR(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_REC_TYPE_EMULATOR))
#define GST_REC_IS_EMULATOR_CLASS(obj) \
  (G_TYPE_CHECK_CLASS_TYPE((klass), GST_REC_TYPE_EMULATOR))
#define GST_REC_EMULATOR_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), GST_REC_TYPE_EMULATOR, \
                              GstRecEmulatorClass))

/**
 * GstRecEmulatorMode:
 * @GST_REC_EMU_AUTODETECT: if the source supports the interface,
 * don't emulate, otherwise do emulate (default).
 * @GST_REC_EMU_ALWAYS: always emulate, even if the source supports
 * the interface (allows snapshotting and filters).
 * @GST_REC_EMU_NEVER: never emulate, even if the source does not
 * support the interface (less CPU use).
 *
 * Selects in what mode to do interface emulation on the source.
 */
typedef enum {
  GST_REC_EMU_AUTODETECT,
  GST_REC_EMU_ALWAYS,
  GST_REC_EMU_NEVER
} GstRecEmulatorMode;

typedef struct _GstRecEmulator
{
  GstBin parent;

  /* ghost pad for the last element's src pad */
  GstPad *srcpad;

  /* our assistant */
  GstElement *source;			/* the producer of data */
  GstElement *sourcepeer;		/* the element that is connected to the source */
  GstElement *queue, *thread, *dcb, *bin;
  GstElement *tee;			/* the actual tee */
  GstPad *teeemupad;			/* pad from tee */
  GstCaps *filter;

  /* keep frames */
  GstBuffer *frame;

  /* emulator mode */
  GstRecEmulatorMode mode;
} GstRecEmulator;

typedef struct _GstRecEmulatorClass
{
  GstBinClass parent;

  GList *factories;
} GstRecEmulatorClass;

GType gst_rec_emulator_get_type (void);
GstElement * gst_rec_emulator_get_by_interface (GstRecEmulator * emulator,
    GType type);
void gst_rec_emulator_set_filter (GstRecEmulator *emulator, GstCaps * filter);

#endif /* __GST_REC_EMULATOR_H__ */
