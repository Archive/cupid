/* GStreamer Recorder
 * (c) 2003 Ronald Bultje <rbultje@ronald.bitfreak.net>
 *
 * list.c: lists of elements (as template)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <string.h>

#include <glib.h>
#include <gst/gst.h>
#include <gst/propertyprobe/propertyprobe.h>

#include "debug.h"
#include "list.h"
#include "record.h"

/*
 * Internal lists of sources/encoders/muxers.
 */

static GList *video_source_factories = NULL,
    *audio_source_factories = NULL,
    *video_encoder_factories = NULL,
    *audio_encoder_factories = NULL, *muxer_factories = NULL;

/*
 * Types
 */

static GObjectClass *src_parent_class = NULL,
    *enc_parent_class = NULL, *mux_parent_class = NULL;

static void
gst_rec_source_init (GstRecSource * src)
{
  src->name = NULL;
  src->factory = NULL;
  src->device = NULL;
  src->caps = NULL;
}

static void
gst_rec_source_dispose (GObject * obj)
{
  GstRecSource *src = GST_REC_SOURCE (obj);

  g_free (src->name);
  src->name = NULL;
  g_free (src->device);
  src->device = NULL;
  if (src->caps) {
    gst_caps_free (src->caps);
    src->caps = NULL;
  }
}

static void
gst_rec_source_class_init (GObjectClass * klass)
{
  src_parent_class = g_type_class_ref (G_TYPE_OBJECT);
  klass->dispose = gst_rec_source_dispose;
}

GType
gst_rec_source_get_type (void)
{
  static GType gst_rec_source_type = 0;

  if (!gst_rec_source_type) {
    static const GTypeInfo gst_rec_source_info = {
      sizeof (GObjectClass),
      NULL,
      NULL,
      (GClassInitFunc) gst_rec_source_class_init,
      NULL,
      NULL,
      sizeof (GstRecSource),
      0,
      (GInstanceInitFunc) gst_rec_source_init,
      NULL
    };

    gst_rec_source_type =
        g_type_register_static (G_TYPE_OBJECT,
        "GstRecSource", &gst_rec_source_info, 0);
  }

  return gst_rec_source_type;
}

static GstRecSource *
gst_rec_source_new (gchar * name, GstElementFactory * factory,
    gchar * device, GstCaps * caps)
{
  GstRecSource *src = g_object_new (GST_REC_TYPE_SOURCE, NULL);

  src->name = name;
  src->factory = factory;
  src->device = device;
  src->caps = caps;

  return src;
}

static void
gst_rec_encoder_init (GstRecEncoder * enc)
{
  enc->name = NULL;
  enc->factory = NULL;
  enc->filter = NULL;
}

static void
gst_rec_encoder_dispose (GObject * obj)
{
  GstRecEncoder *enc = GST_REC_ENCODER (obj);

  g_free (enc->name);
  gst_caps_replace (&enc->filter, NULL);
}

static void
gst_rec_encoder_class_init (GObjectClass * klass)
{
  enc_parent_class = g_type_class_ref (G_TYPE_OBJECT);
  klass->dispose = gst_rec_encoder_dispose;
}

GType
gst_rec_encoder_get_type (void)
{
  static GType gst_rec_encoder_type = 0;

  if (!gst_rec_encoder_type) {
    static const GTypeInfo gst_rec_encoder_info = {
      sizeof (GObjectClass),
      NULL,
      NULL,
      (GClassInitFunc) gst_rec_encoder_class_init,
      NULL,
      NULL,
      sizeof (GstRecEncoder),
      0,
      (GInstanceInitFunc) gst_rec_encoder_init,
      NULL
    };

    gst_rec_encoder_type =
        g_type_register_static (G_TYPE_OBJECT,
        "GstRecEncoder", &gst_rec_encoder_info, 0);
  }

  return gst_rec_encoder_type;
}

static GstRecEncoder *
gst_rec_encoder_new (gchar * name,
    GstElementFactory * factory, GstCaps * filter)
{
  GstRecEncoder *enc = g_object_new (GST_REC_TYPE_ENCODER, NULL);

  enc->name = name;
  enc->factory = factory;
  enc->filter = filter;

  return enc;
}

static void
gst_rec_muxer_dispose (GObject * obj)
{
  GstRecMuxer *mux = GST_REC_MUXER (obj);

  g_free (mux->name);
}

static void
gst_rec_muxer_class_init (GObjectClass * klass)
{
  mux_parent_class = g_type_class_ref (G_TYPE_OBJECT);
  klass->dispose = gst_rec_muxer_dispose;
}

static void
gst_rec_muxer_init (GstRecMuxer * mux)
{
  mux->name = NULL;
  mux->factory = NULL;
}

GType
gst_rec_muxer_get_type (void)
{
  static GType gst_rec_muxer_type = 0;

  if (!gst_rec_muxer_type) {
    static const GTypeInfo gst_rec_muxer_info = {
      sizeof (GObjectClass),
      NULL,
      NULL,
      (GClassInitFunc) gst_rec_muxer_class_init,
      NULL,
      NULL,
      sizeof (GstRecMuxer),
      0,
      (GInstanceInitFunc) gst_rec_muxer_init,
      NULL
    };

    gst_rec_muxer_type =
        g_type_register_static (G_TYPE_OBJECT,
        "GstRecMuxer", &gst_rec_muxer_info, 0);
  }

  return gst_rec_muxer_type;
}

static GstRecMuxer *
gst_rec_muxer_new (gchar * name, GstElementFactory * factory)
{
  GstRecMuxer *mux = g_object_new (GST_REC_TYPE_MUXER, NULL);

  mux->name = name;
  mux->factory = factory;

  return mux;
}

/*
 * Lists.
 */

static GList *
gst_rec_list_any_sources (GList * factories)
{
  GstRecSource *source;
  GList *sources = NULL, *item;

  GST_DEBUG ("Finding sources");

  for (item = factories; item != NULL; item = item->next) {
    GstElementFactory *factory = item->data;
    GstElement *element;
    GstPropertyProbe *probe;
    const GParamSpec *pspec;
    GObjectClass *klass;
    const gchar *longname;

    /* first see if we can actually create a device here */
    element = gst_element_factory_create (factory, "test");
    longname = gst_element_factory_get_longname (factory);
    if (!element) {
      GST_WARNING ("Failed to create instance of factory '%s' (%s)",
          longname, GST_PLUGIN_FEATURE (factory)->name);
      continue;
    }
    klass = G_OBJECT_GET_CLASS (element);

    /* do we have a "device" property? */
    if (!g_object_class_find_property (klass, "device") ||
        !GST_IS_PROPERTY_PROBE (element) ||
        !(probe = GST_PROPERTY_PROBE (element)) ||
        !(pspec = gst_property_probe_get_property (probe, "device"))) {
      GST_DEBUG ("Found source '%s' (%s) - no device",
          longname, GST_PLUGIN_FEATURE (factory)->name);

      source = gst_rec_source_new (g_strdup (longname), factory, NULL, NULL);
      sources = g_list_append (sources, source);
    } else {
      gint n;
      gchar *name;
      GValueArray *array;

      array = gst_property_probe_probe_and_get_values (probe, pspec);
      if (array != NULL) {
        for (n = 0; n < array->n_values; n++) {
          GValue *device;
          GstCaps *caps;

          device = g_value_array_get_nth (array, n);
          g_object_set_property (G_OBJECT (element), "device", device);

          if (gst_element_set_state (element, GST_STATE_READY) !=
              GST_STATE_SUCCESS) {
            GST_WARNING
                ("Found source '%s' (%s) - device %s failed to open", longname,
                GST_PLUGIN_FEATURE (factory)->name,
                g_value_get_string (device));
            continue;
          }

          g_object_get (G_OBJECT (element), "device-name", &name, NULL);
          caps = gst_pad_get_caps (gst_element_get_pad (element, "src"));
          gst_element_set_state (element, GST_STATE_NULL);

          GST_DEBUG ("Found source '%s' (%s) - device %s '%s'",
              longname, GST_PLUGIN_FEATURE (factory)->name,
              g_value_get_string (device), name);

          source = gst_rec_source_new (g_strdup_printf ("%s [%s]", name,
                  longname), factory, g_strdup (g_value_get_string (device)),
                  caps);
          sources = g_list_append (sources, source);
        }
      }
    }

    gst_object_unref (GST_OBJECT (element));
  }

  /* An empty one ("None") */
  source = gst_rec_source_new (g_strdup ("None"), NULL, NULL, NULL);
  sources = g_list_append (sources, source);

  /* FIXME: sort, based on ranks (another FIXME. ;)). */

  return sources;
}

GList *
gst_rec_list_video_sources (void)
{
  /* caching increases speed */
  static GList *video_sources = NULL;

  if (!video_sources)
    video_sources = gst_rec_list_any_sources (video_source_factories);

  g_list_foreach (video_sources, (GFunc) g_object_ref, NULL);

  return g_list_copy (video_sources);
}

GList *
gst_rec_list_audio_sources (void)
{
  /* caching increases speed */
  static GList *audio_sources = NULL;

  if (!audio_sources)
    audio_sources = gst_rec_list_any_sources (audio_source_factories);

  g_list_foreach (audio_sources, (GFunc) g_object_ref, NULL);

  return g_list_copy (audio_sources);
}

GList *
gst_rec_list_muxers (GstElement * video_source, GstElement * audio_source)
{
  GstRecMuxer *muxer;
  GList *muxers = NULL, *item;
  GstElementFactory *video_factory = NULL, *audio_factory = NULL;

  /* shortcuts */
  if (video_source)
    video_factory = gst_element_get_factory (video_source);
  if (audio_source)
    audio_factory = gst_element_get_factory (audio_source);

  GST_DEBUG ("Finding muxers for video source '%s' (%s) "
      "and audio source '%s' (%s)",
      (video_factory != NULL) ?
      gst_element_factory_get_longname (video_factory) : "NULL",
      (video_factory != NULL) ?
      GST_PLUGIN_FEATURE (video_factory)->name : "none",
      (audio_factory != NULL) ?
      gst_element_factory_get_longname (audio_factory) : "NULL",
      (audio_factory != NULL) ?
      GST_PLUGIN_FEATURE (audio_factory)->name : "none");

  for (item = muxer_factories; item != NULL; item = item->next) {
    GstElementFactory *factory = item->data;

    /* FIXME: check whether the given muxers fits at all
     * (i.e., check against given sources and against all
     * known encoders) */

    GST_DEBUG ("Found muxer '%s' (%s)",
        gst_element_factory_get_longname (factory),
        GST_PLUGIN_FEATURE (factory)->name);

    muxer =
        gst_rec_muxer_new (g_strdup (gst_element_factory_get_longname
            (factory)), factory);
    muxers = g_list_append (muxers, muxer);
  }

  /* FIXME: sort, based on ranks (another FIXME. ;)). */

  return muxers;
}

/*
 * Try to make a nice human-readable string from the caps.
 */

static gchar *
gst_rec_caps_format (GstCaps * caps, guint num)
{
  GstStructure *structure = gst_caps_get_structure (caps, num);
  const gchar *mime = gst_structure_get_name (structure);
  gchar *name = NULL;

  if (!strcmp (mime, "video/x-raw-yuv")) {
    guint32 fcc;

    gst_structure_get_fourcc (structure, "format", &fcc);

    switch (fcc) {
      case GST_MAKE_FOURCC ('Y', 'U', 'Y', '2'):
        name = g_strdup ("Raw packed YUV-4:2:2 (YUY2)");
        break;
      case GST_MAKE_FOURCC ('I', '4', '2', '0'):
        name = g_strdup ("Raw planar YUV-4:2:0 (I420)");
        break;
      default:
        break;
    }
  } else if (!strcmp (mime, "video/x-raw-rgb")) {
    gint depth;

    gst_structure_get_int (structure, "depth", &depth);

    switch (depth) {
      case 24:
      case 32:
        name = g_strdup_printf ("Raw %d-bits RGB", depth);
        break;
      default:
        break;
    }
  } else if (!strcmp (mime, "image/jpeg")) {
    name = g_strdup ("Hardware Motion-JPEG");
  } else if (!strcmp (mime, "video/x-dv")) {
    name = g_strdup ("Digital Video (DV)");
  } else if (!strcmp (mime, "audio/x-raw-int")) {
    gboolean sign;
    gint depth, width;

    gst_structure_get_boolean (structure, "signed", &sign);
    gst_structure_get_int (structure, "depth", &depth);
    gst_structure_get_int (structure, "width", &width);

    if (width == depth) {
      if (width == 16 && sign) {
        name = g_strdup ("Raw 16-bits signed audio");
      } else if (width == 8 && !sign) {
        name = g_strdup ("Raw 8-bits unsigned audio");
      }
    }
  } else if (!strcmp (mime, "audio/x-raw-float")) {
    gint width;

    gst_structure_get_int (structure, "width", &width);
    switch (width) {
      case 32:
        name = g_strdup ("Raw single-precision float audio");
        break;
      case 64:
        name = g_strdup ("Raw double-precision float audio");
        break;
    }
  }

  return name;
}

static GList *
gst_rec_list_any_encoders (GstElement * source,
    GstElement * muxer, GList * in_encoderslist)
{
  GList *muxtempllist, *encoderslist;
  GList *encoders = NULL;
  GstRecEncoder *encoder;
  GstCaps *source_caps;
  GstElementFactory *source_factory, *muxer_factory;
  gboolean raw = FALSE;
  gint n;

  if (!source || !muxer)
    return NULL;

  /* factory shortcuts */
  source_factory = gst_element_get_factory (source);
  muxer_factory = gst_element_get_factory (muxer);

  GST_DEBUG ("Making encoder list for source '%s' (%s) "
      "and muxer '%s' (%s)",
      gst_element_factory_get_longname (source_factory),
      GST_PLUGIN_FEATURE (source_factory)->name,
      gst_element_factory_get_longname (muxer_factory),
      GST_PLUGIN_FEATURE (muxer_factory)->name);

  /* get pad (usually template) caps */
  source_caps = g_object_get_data (G_OBJECT (source), "template");
  if (source_caps) {
    source_caps = gst_caps_copy (source_caps);
  } else {
    source_caps = gst_pad_get_caps (gst_element_get_pad (source, "src"));
  }
  if (gst_caps_is_any (source_caps)) {
    GST_WARNING ("Source '%s' (%s) gave ANY caps",
        gst_element_factory_get_longname (source_factory),
        GST_PLUGIN_FEATURE (source_factory)->name);
    return NULL;
  }

  /* check for raw caps */
  for (n = 0; n < gst_caps_get_size (source_caps); n++) {
    GstStructure *structure = gst_caps_get_structure (source_caps, n);
    const gchar *mime = gst_structure_get_name (structure);

    if (!strcmp (mime, "video/x-raw-yuv") ||
        !strcmp (mime, "video/x-raw-rgb") ||
        !strcmp (mime, "audio/x-raw-float") ||
        !strcmp (mime, "audio/x-raw-int"))
      raw = TRUE;
  }

  /* go through all template video sink pads of the muxer */
  for (muxtempllist = muxer_factory->padtemplates;
      muxtempllist != NULL; muxtempllist = muxtempllist->next) {
    GstPadTemplate *muxer_template = muxtempllist->data;

    if (muxer_template->direction == GST_PAD_SINK) {
      const GstCaps *muxer_caps = gst_pad_template_get_caps (muxer_template);
      GstCaps *return_caps, *t;

      /* make sure we've got something to calculate with */
      if (!muxer_caps)
        continue;

      /* check for direct source->muxer connection. */
      if ((return_caps = gst_caps_intersect (source_caps, muxer_caps))) {
        for (n = 0; n < gst_caps_get_size (return_caps); n++) {
          GstStructure *structure = gst_caps_get_structure (return_caps, n);

          if (gst_structure_has_field (structure, "width") &&
              gst_structure_has_field (structure, "height") &&
              gst_structure_has_field (structure, "framerate")) {
            gst_structure_remove_fields (structure,
                "width", "height", "framerate", NULL);
          } else if (gst_structure_has_field (structure, "norm")) {
            gst_structure_remove_field (structure, "norm");
          } else if (gst_structure_has_field (structure, "rate") &&
              gst_structure_has_field (structure, "channels")) {
            gst_structure_remove_fields (structure, "rate", "channels", NULL);
          }
        }
        t = gst_caps_normalize (return_caps);
        gst_caps_free (return_caps);
        return_caps = t;
        for (n = 0; n < gst_caps_get_size (return_caps); n++) {
          gchar *str1 = gst_rec_caps_format (return_caps, n);

          if (str1 != NULL) {
            GstStructure *structure = gst_caps_get_structure (return_caps, n);
            GstCaps *one =
                gst_caps_new_full (gst_structure_copy (structure), NULL);
            gchar *str2 = gst_caps_to_string (one);

            /* we've got a hit! */
            GST_DEBUG ("Adding filter '%s' (%s) to list", str1, str2);
            g_free (str2);

            /* add to list */
            encoder = gst_rec_encoder_new (str1, NULL, one);
            encoders = g_list_append (encoders, encoder);
          }
        }

        gst_caps_free (return_caps);
      }

      /* check for raw caps */
      if (!raw)
        continue;

      /* traverse list */
      for (encoderslist = in_encoderslist;
          encoderslist != NULL; encoderslist = encoderslist->next) {
        GstElementFactory *encoder_factory = encoderslist->data;
        GList *enctempllist;
        GstPadTemplate *encoder_src_template = NULL,
            *encoder_sink_template = NULL;
        const GstCaps *encoder_src_caps;
        GstCaps *return_src_caps = NULL;

        /* find the proper src/sink template */
        for (enctempllist = encoder_factory->padtemplates;
            enctempllist != NULL; enctempllist = enctempllist->next) {
          GstPadTemplate *template = enctempllist->data;

          if (template->direction == GST_PAD_SRC) {
            encoder_src_template = template;
          } else {
            encoder_sink_template = template;
          }
        }

        /* make sure we filled in everything */
        if (!encoder_src_template || !encoder_sink_template)
          continue;

        /* fill in caps */
        encoder_src_caps = gst_pad_template_get_caps (encoder_src_template);

        if (encoder_src_caps != NULL &&
            !gst_caps_is_empty (return_src_caps =
                gst_caps_intersect (encoder_src_caps, muxer_caps))) {
          /* we've got a hit! */
          GST_DEBUG ("Adding encoder '%s' (%s) to list",
              gst_element_factory_get_longname (encoder_factory),
              GST_PLUGIN_FEATURE (encoder_factory)->name);

          /* add to list */
          encoder =
              gst_rec_encoder_new (g_strdup (gst_element_factory_get_longname
                  (encoder_factory)), encoder_factory, NULL);
          encoders = g_list_append (encoders, encoder);
        }

        /* free memory */
        if (return_src_caps)
          gst_caps_free (return_src_caps);
      }
    }
  }

  /* free last memory */
  gst_caps_free (source_caps);

  /* FIXME: sort, based on ranks (another FIXME. ;)). */

  /* and return our list */
  return encoders;
}

GList *
gst_rec_list_video_encoders (GstElement * video_source, GstElement * muxer)
{
  return gst_rec_list_any_encoders (video_source, muxer,
      video_encoder_factories);
}

GList *
gst_rec_list_audio_encoders (GstElement * audio_source, GstElement * muxer)
{
  return gst_rec_list_any_encoders (audio_source, muxer,
      audio_encoder_factories);
}

/*
 * Free returned lists.
 */

void
gst_rec_list_replace (GList ** old, GList * new)
{
  if (*old) {
    g_list_foreach (*old, (GFunc) g_object_unref, NULL);
    g_list_free (*old);
  }

  *old = new;
}

/*
 * Config entries.
 */

gchar *
gst_rec_source_to_config (GstRecSource * templ)
{
  if (!templ->factory)
    return g_strdup ("");

  if (templ->device)
    return g_strdup_printf ("%s device=%s",
        GST_PLUGIN_FEATURE (templ->factory)->name, templ->device);

  return g_strdup (GST_PLUGIN_FEATURE (templ->factory)->name);
}

gchar *
gst_rec_encoder_to_config (GstRecEncoder * templ)
{
  if (!templ->factory)
    return g_strdup ("");

  return g_strdup (GST_PLUGIN_FEATURE (templ->factory)->name);
}

gchar *
gst_rec_muxer_to_config (GstRecMuxer * templ)
{
  if (!templ->factory)
    return g_strdup ("");

  return g_strdup (GST_PLUGIN_FEATURE (templ->factory)->name);
}

gchar *
gst_rec_filter_to_config (GstRecEncoder * templ)
{
  if (!templ->filter)
    return g_strdup ("");

  return gst_caps_to_string (templ->filter);
}

/*
 * Elements.
 */

static GstElement *
gst_rec_config_to_element (const gchar * config, const gchar * name)
{
  GstElement *element;
  GError *error = NULL;

  if (!config || config[0] == '\0')
    return NULL;

  element = gst_parse_launch (config, &error);
  if (error != NULL) {
    g_warning ("Unable to parse GStreamer pipeline %s: %s",
        config, error->message);
    g_error_free (error);
    return NULL;
  }

  /* Users might do really clumsy things, like giving multiple
   * elements and assuming we like that. Well, we **DON'T**.
   * Stay off this GConf config stuff, guys, it's not meant
   * to be changed into random sequences of characters to see
   * whether the application will survive. */
  if (!element || GST_IS_BIN (element)) {
    if (element)
      gst_object_unref (GST_OBJECT (element));
    return NULL;
  }

  gst_element_set_name (element, name);

  return element;
}

GstElement *
gst_rec_config_to_source (const gchar * config, gboolean audio)
{
  static guint audio_counter = 0, video_counter = 0;
  gchar *name;
  GstElement *element;
  GstRecVideoSource *src = NULL;
  GList *item;

  if (audio) {
    name = g_strdup_printf ("audio-source-%u", audio_counter++);
  } else {
    name = g_strdup_printf ("video-source-%u", video_counter++);

    for (item = gst_rec_list_video_sources ();
         item != NULL; item = item->next) {
      gchar *tmp;
      gboolean match;

      tmp = gst_rec_source_to_config (item->data);
      match = !strcmp (tmp, config);
      g_free (tmp);

      if (match) {
        src = item->data;
        break;
      }
    }
  }
  element = gst_rec_config_to_element (config, name);
  if (element && src) {
    GObjectClass *oclass = G_OBJECT_GET_CLASS (element);

    if (g_object_class_find_property (oclass, "autoprobe")) {
      g_object_set (G_OBJECT (element), "autoprobe", FALSE, NULL);
    }
    if (g_object_class_find_property (oclass, "auroprobe-fps")) {
      g_object_set (G_OBJECT (element), "autoprobe-fps", FALSE, NULL);
    }
    if (g_object_class_find_property (oclass, "sync-mode")) {
      g_object_set (G_OBJECT (element), "sync-mode", 2, NULL);
    }
    if (g_object_class_find_property (oclass, "sync")) {
      g_object_set (G_OBJECT (element), "sync", TRUE, NULL);
    }
    if (g_object_class_find_property (oclass, "use-fixed-fps")) {
      g_object_set (G_OBJECT (element), "use-fixed-fps", TRUE, NULL);
    }
    g_object_set_data (G_OBJECT (element), "template", src->caps);
  }
  g_free (name);

  return element;
}

GstElement *
gst_rec_config_to_encoder (const gchar * config, gboolean audio)
{
  static guint audio_counter = 0, video_counter = 0;
  gchar *name;
  GstElement *element;

  if (audio) {
    name = g_strdup_printf ("audio-encoder-%u", audio_counter++);
  } else {
    name = g_strdup_printf ("video-encoder-%u", video_counter++);
  }
  element = gst_rec_config_to_element (config, name);
  g_free (name);

  return element;
}

GstElement *
gst_rec_config_to_muxer (const gchar * config)
{
  static guint counter = 0;
  gchar *name;
  GstElement *element;

  name = g_strdup_printf ("muxer-%u", counter++);
  element = gst_rec_config_to_element (config, name);
  g_free (name);

  return element;
}

GstCaps *
gst_rec_config_to_filter (const gchar * config, gboolean audio)
{
  return gst_caps_from_string ((gchar *) config);
}

/*
 * Get the GstRec{Muxer,{Video,Audio}{Source,Encoder,Filter}}
 * that makes for the currently selected one.
 */

GstRecSource *
gst_rec_list_find_source (GstRec * rec, GList * list, GstElement * current)
{
  GstElementFactory *factory = NULL;

  if (current)
    factory = gst_element_get_factory (current);

  for (; list != NULL; list = list->next) {
    GstRecSource *source = list->data;

    if (source->factory != factory)
      continue;

    if (source->device) {
      gchar *device;

      g_object_get (G_OBJECT (current), "device", &device, NULL);
      if (strcmp (source->device, device))
        continue;
    }

    return source;
  }

  return NULL;
}

GstRecEncoder *
gst_rec_list_find_encoder (GstRec * rec,
    GList * list, GstElement * current, GstCaps * filter)
{
  GstElementFactory *factory = NULL;

  if (current)
    factory = gst_element_get_factory (current);

  for (; list != NULL; list = list->next) {
    GstRecEncoder *encoder = list->data;

    if (encoder->factory != factory)
      continue;
    if (!encoder->filter && !filter)
      return encoder;
    if ((!encoder->filter && filter) || (encoder->filter && !filter))
      continue;
    if (gst_caps_is_equal (encoder->filter, filter))
      return encoder;
  }

  return NULL;
}

GstRecMuxer *
gst_rec_list_find_muxer (GstRec * rec, GList * list)
{
  GstElement *current = gst_rec_get_muxer (rec);
  GstElementFactory *factory = NULL;

  if (current)
    factory = gst_element_get_factory (current);

  for (; list != NULL; list = list->next) {
    GstRecMuxer *muxer = list->data;

    if (muxer->factory == factory)
      return muxer;
  }

  return NULL;
}

/*
 * Initialize internal lists of sources/codecs/muxers.
 */

void
gst_rec_lists_init (void)
{
  const GList *elements;
  GstElementFactory *factory;
  GList **addlist;
  const gchar *klass;

  GST_DEBUG ("Building initial list of typed elements");

  /* build up a tree of elements internally */
  elements = gst_registry_pool_feature_list (GST_TYPE_ELEMENT_FACTORY);
  while (elements != NULL) {
    factory = (GstElementFactory *) elements->data;
    klass = gst_element_factory_get_klass (factory);

    if (!strncmp (klass, "Source/Video", 12)) {
      addlist = &video_source_factories;
    } else if (!strncmp (klass, "Source/Audio", 12)) {
      addlist = &audio_source_factories;
    } else if (!strncmp (klass, "Codec/Encoder/Audio", 19)) {
      addlist = &audio_encoder_factories;
    } else if (!strncmp (klass, "Codec/Encoder/Video", 19)) {
      addlist = &video_encoder_factories;
    } else if (!strncmp (klass, "Codec/Muxer", 11)) {
      addlist = &muxer_factories;
    } else {
      goto next;
    }

    g_object_ref (G_OBJECT (factory));
    *addlist = g_list_append (*addlist, factory);

  next:
    elements = elements->next;
  }
}
