/* GStreamer Recorder
 * (c) 2003 Ronald Bultje <rbultje@ronald.bitfreak.net>
 *
 * emulator.c: interface emulator
 *
 * This element emulates the xoverlay interface and the volume
 * interface (including audio output). Both are handled by
 * putting the source in a separate thread, with a tee and
 * queue attached. The queue is attached to the ghostpad,
 * which is part of the actual pipeline.
 *
 * .-------------------------------------------.
 * |thread                                     |------------.
 * |                         / video/audiosink |emulatorbin |
 * | source ! [volume !] tee                   |            |
 * |                         \-----------------|-queue----ghost
 * '-------------------------------------------|            |
 *                                             '------------'
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <string.h>

#include <gst/gst.h>
#include <gst/xoverlay/xoverlay.h>
#include <gst/mixer/mixer.h>
#include <gst/gconf/gconf.h>

#include "debug.h"
#include "emulator.h"

enum {
  ARG_0,
  ARG_FRAME,
  ARG_FORMAT,
  ARG_MODE
};

static void gst_rec_emulator_class_init (GstRecEmulatorClass * klass);
static void gst_rec_emulator_base_init (GstRecEmulatorClass * klass);
static void gst_rec_emulator_init (GstRecEmulator * emulator);
static void gst_rec_emulator_dispose (GObject * obj);

static void gst_rec_emulator_child_add (GstBin * bin, GstElement * child);
static void gst_rec_emulator_child_del (GstBin * bin, GstElement * child);
static GstElementStateReturn
    gst_rec_emulator_change_state (GstElement * element);
static void gst_rec_emulator_get_property (GObject * object,
    guint prop_id, GValue * value, GParamSpec * pspec);
static void gst_rec_emulator_set_property (GObject * object,
    guint prop_id, const GValue * value, GParamSpec * pspec);

static GstBinClass *parent_class = NULL;

GType
gst_rec_emulator_get_type (void)
{
  static GType gst_rec_emulator_type = 0;

  if (!gst_rec_emulator_type) {
    static const GTypeInfo gst_rec_emulator_info = {
      sizeof (GstRecEmulatorClass),
      (GBaseInitFunc) gst_rec_emulator_base_init,
      NULL,
      (GClassInitFunc) gst_rec_emulator_class_init,
      NULL,
      NULL,
      sizeof (GstRecEmulator),
      0,
      (GInstanceInitFunc) gst_rec_emulator_init,
      NULL
    };

    gst_rec_emulator_type =
	g_type_register_static (GST_TYPE_BIN,
	"GstRecEmulator", &gst_rec_emulator_info, 0);
  }

  return gst_rec_emulator_type;
}

static GType
gst_rec_emulator_mode_get_type (void)
{
  static GType emulator_mode_type = 0;

  if (!emulator_mode_type) {
    static GEnumValue emulator_mode_types[] = {
      {GST_REC_EMU_AUTODETECT, "Autodetect",
           "Autodetect - only if source does not support interface"},
      {GST_REC_EMU_ALWAYS, "Always",
           "Always emulate - allows filters/snapshotting"},
      {GST_REC_EMU_NEVER, "Never", "Never emulate - saves CPU"},
      {0, NULL, NULL},
    };
                                                                                
    emulator_mode_type =
        g_enum_register_static ("GstRecEmulatorMode", emulator_mode_types);
  }

  return emulator_mode_type;
}

static void
gst_rec_emulator_base_init (GstRecEmulatorClass * klass)
{
  GstElementClass *element_class = GST_ELEMENT_CLASS (klass);
  static GstElementDetails gst_rec_emulator_details =
      GST_ELEMENT_DETAILS ("Emulating bin",
      "Generic/Bin",
      "Emulates volume and xoverlay interface",
      "Ronald Bultje <rbultje@ronald.bitfreak.net>");

  gst_element_class_set_details (element_class, &gst_rec_emulator_details);
}

/*
 * This function is called by the registry loader. Its return value
 * (TRUE or FALSE) decides whether the given feature will be included
 * in the list that we're generating further down.
 */

static gboolean
cb_feature_filter (GstPluginFeature * feature, gpointer data)
{
  const gchar *klass;
  guint rank;

  /* we only care about element factories */
  if (!GST_IS_ELEMENT_FACTORY (feature))
    return FALSE;

  /* only parsers, demuxers and decoders */
  klass = gst_element_factory_get_klass (GST_ELEMENT_FACTORY (feature));
  if (g_strrstr (klass, "Demux") == NULL &&
      (g_strrstr (klass, "Decoder") == NULL ||
       (g_strrstr (klass, "Video") == NULL &&
        g_strrstr (klass, "Image") == NULL)) &&
      (g_strrstr (klass, "Parse") == NULL ||
       g_strrstr (klass, "Video") == NULL))
    return FALSE;

  /* only select elements with autoplugging rank */
  rank = gst_plugin_feature_get_rank (feature);
  if (rank < GST_RANK_MARGINAL)
    return FALSE;

  GST_DEBUG ("Enabling element %s", feature->name);

  return TRUE;
}

/*
 * This function is called to sort features by rank.
 */

static gint
cb_compare_ranks (GstPluginFeature *f1,
		  GstPluginFeature *f2)
{
  return gst_plugin_feature_get_rank (f2) - gst_plugin_feature_get_rank (f1);
}

static void
gst_rec_emulator_class_init (GstRecEmulatorClass * klass)
{
  GstBinClass *bin_class = GST_BIN_CLASS (klass);
  GstElementClass *element_class = GST_ELEMENT_CLASS (klass);
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  parent_class = g_type_class_ref (GST_TYPE_BIN);

  g_object_class_install_property (object_class, ARG_FRAME,
      g_param_spec_boxed ("frame", "Frame",
          "The last frame (NULL = no video available)",
          GST_TYPE_BUFFER, G_PARAM_READABLE));
  g_object_class_install_property (object_class, ARG_FORMAT,
      g_param_spec_boxed ("format", "Format",
          "The format of the frame in in the 'frame' property",
          GST_TYPE_CAPS, G_PARAM_READABLE));
  g_object_class_install_property (object_class, ARG_MODE,
      g_param_spec_enum ("mode", "Mode", "Emulation mode",
          gst_rec_emulator_mode_get_type (), GST_REC_EMU_AUTODETECT,
          G_PARAM_READWRITE));

  object_class->get_property = gst_rec_emulator_get_property;
  object_class->set_property = gst_rec_emulator_set_property;
  object_class->dispose = gst_rec_emulator_dispose;
  element_class->change_state = gst_rec_emulator_change_state;
  bin_class->add_element = gst_rec_emulator_child_add;
  bin_class->remove_element = gst_rec_emulator_child_del;

  /* first filter out the interesting element factories */
  klass->factories = gst_registry_pool_feature_filter (
      (GstPluginFeatureFilter) cb_feature_filter, FALSE, NULL);
  klass->factories = g_list_sort (klass->factories,
      (GCompareFunc) cb_compare_ranks);
}

static void
gst_rec_emulator_init (GstRecEmulator * emulator)
{
  emulator->thread = NULL;
  emulator->bin = NULL;
  emulator->dcb = NULL;
  emulator->source = NULL;
  emulator->frame = NULL;
  emulator->mode = GST_REC_EMU_AUTODETECT;
  emulator->filter = NULL;
  emulator->teeemupad = NULL;
}

static void
gst_rec_emulator_dispose (GObject * obj)
{
  GstRecEmulator *emulator = GST_REC_EMULATOR (obj);

  if (emulator->filter) {
    gst_caps_free (emulator->filter);
    emulator->filter = NULL;
  }

  G_OBJECT_CLASS (parent_class)->dispose (obj);
}

static void
cb_error (GstElement * thread, GstElement * _source,
    GError * error, gchar * debug, gpointer data)
{
  GstObject *source, *parent;

  source = GST_OBJECT (_source);
  parent = GST_OBJECT (data);

  /* don´t forward if we´re testing interface workingness */
  if (GST_STATE (parent) == GST_STATE_NULL &&
      GST_STATE_PENDING (parent) == GST_STATE_VOID_PENDING)
    return;

  /* tell ourselves */
  gst_object_ref (source);
  gst_object_ref (parent);
  GST_DEBUG ("forwarding error \"%s\" from %s to %s", error->message,
      GST_ELEMENT_NAME (source), GST_OBJECT_NAME (parent));

  g_signal_emit_by_name (G_OBJECT (parent), "error", source, error, debug);

  GST_DEBUG ("forwarded error \"%s\" from %s to %s", error->message,
      GST_ELEMENT_NAME (source), GST_OBJECT_NAME (parent));
  gst_object_unref (source);
  gst_object_unref (parent);
}

static gboolean
cb_probe (GstProbe * probe, GstData ** d, gpointer data)
{
  GstRecEmulator *emulator = data;

  if (GST_IS_BUFFER (*d)) {
    if (emulator->frame) {
      gst_buffer_unref (emulator->frame);
    }
    emulator->frame = gst_buffer_ref (*d);
  }

  /* pass-through */
  return TRUE;
}

static void
gst_rec_emulator_child_add (GstBin * bin, GstElement * child)
{
  GstRecEmulator *emulator = GST_REC_EMULATOR (bin);
  GstElementFactory *f;
  gboolean impl;

  g_assert (emulator->source == NULL);
  g_assert (GST_STATE (child) == GST_STATE_NULL);
  g_assert (GST_STATE (bin) == GST_STATE_NULL);

  GST_LOG_OBJECT (emulator, "Adding child %p (%s) using caps %p (%s)",
      child, GST_ELEMENT_NAME (child), emulator->filter,
      emulator->filter ? gst_structure_get_name (
      gst_caps_get_structure (emulator->filter, 0)) : "(null)");
  emulator->source = child;

  f = gst_element_get_factory (child);
  if (g_strrstr (gst_element_factory_get_klass (f), "Video") != NULL) {
    switch (emulator->mode) {
      default:
      case GST_REC_EMU_AUTODETECT:
        gst_element_set_state (child, GST_STATE_READY);
        impl = GST_IS_X_OVERLAY (child);
        gst_element_set_state (child, GST_STATE_NULL);
        break;
      case GST_REC_EMU_ALWAYS:
        impl = FALSE;
        break;
      case GST_REC_EMU_NEVER:
        impl = TRUE;
        break;
    }

    if (!impl) {
      GstElement *thread, *queue, *outbin, *csp, *scale, *sink;
      GstProbe *probe;

      /* create a bunch of elements */
      thread = gst_thread_new ("videoemulationthread");
      g_signal_connect (thread, "error", G_CALLBACK (cb_error), emulator);
      emulator->thread = thread;
      emulator->tee = gst_element_factory_make ("tee", "videoemulationtee");
      emulator->sourcepeer = emulator->tee;
      queue = gst_element_factory_make ("queue", NULL);
      emulator->queue = queue;
      outbin = gst_bin_new ("videooutputbin");
      gst_object_ref (GST_OBJECT (outbin));
      gst_object_sink (GST_OBJECT (outbin));
      emulator->bin = outbin;
      csp = gst_element_factory_make ("ffmpegcolorspace", NULL);
      scale = gst_element_factory_make ("videoscale", NULL);
      sink = gst_gconf_get_default_video_sink ();

      /* link together, create ghostpads + probes */
      gst_element_link_filtered (child, emulator->tee, emulator->filter);
      gst_pad_link (gst_element_get_pad (emulator->tee, "src0"),
          gst_element_get_pad (queue, "sink"));
      gst_element_add_ghost_pad (outbin,
          gst_element_get_pad (csp, "sink"), "sink");
      /*emulator->teeemupad = gst_element_get_pad (tee, "src1");
      gst_pad_link (emulator->teeemupad,
          gst_element_get_pad (outbin, "sink"));*/
      probe = gst_probe_new (FALSE, cb_probe, emulator);
      gst_pad_add_probe (
          GST_PAD_REALIZE (gst_element_get_pad (outbin, "sink")), probe);
      gst_element_link_many (csp, scale, sink, NULL);
      gst_bin_add_many (GST_BIN (outbin), csp, scale, sink, NULL);
      gst_bin_add_many (GST_BIN (thread), child, emulator->tee, outbin, NULL);
      parent_class->add_element (bin, queue);
      gst_element_add_ghost_pad (GST_ELEMENT (bin),
          gst_element_get_pad (queue, "src"), "src");
      gst_rec_emulator_set_filter (emulator, emulator->filter);

      GST_LOG_OBJECT (emulator,
          "Video interface emulation enabled");
    } else {
      parent_class->add_element (bin, child);
      gst_element_add_ghost_pad (GST_ELEMENT (bin),
          gst_element_get_pad (child, "src"), "src");

      GST_LOG_OBJECT (emulator,
          "Video interface emulation not needed");
    }
  } else {
    gst_element_set_state (child, GST_STATE_READY);
    impl = GST_IS_MIXER (child);
    gst_element_set_state (child, GST_STATE_NULL);

    if (!impl) {
      GstElement *thread, *vol, *queue, *conv, *scale, *sink;

      thread = gst_thread_new ("audioemulationthread");
      g_signal_connect (thread, "error", G_CALLBACK (cb_error), emulator);
      emulator->thread = thread;
      vol = gst_element_factory_make ("volume", NULL);
      emulator->sourcepeer = vol;
      emulator->tee = gst_element_factory_make ("tee", "audioemulationtee");
      queue = gst_element_factory_make ("queue", NULL);
      emulator->queue = queue;
      conv = gst_element_factory_make ("audioconvert", NULL);
      scale = gst_element_factory_make ("audioscale", NULL);
      sink = gst_gconf_get_default_audio_sink ();

      gst_element_link_filtered (child, vol, emulator->filter);
      gst_element_link (vol, emulator->tee);
      gst_pad_link (gst_element_get_pad (emulator->tee, "src0"),
          gst_element_get_pad (queue, "sink"));
      gst_pad_link (gst_element_get_pad (emulator->tee, "src1"),
          gst_element_get_pad (conv, "sink"));
      gst_element_link_many (conv, scale, sink, NULL);
      gst_bin_add_many (GST_BIN (thread), child, vol, emulator->tee,
          conv, scale, sink, NULL);
      parent_class->add_element (bin, queue);
      gst_element_add_ghost_pad (GST_ELEMENT (bin),
          gst_element_get_pad (queue, "src"), "src");

      GST_LOG_OBJECT (emulator,
          "Audio interface emulation enabled");
    } else {
      parent_class->add_element (bin, child);
      gst_element_add_ghost_pad (GST_ELEMENT (bin),
          gst_element_get_pad (child, "src"), "src");

      GST_LOG_OBJECT (emulator,
          "Audio interface emulation not needed");
    }
  }
}

static void
gst_rec_emulator_child_del (GstBin * bin, GstElement * child)
{
  GstRecEmulator *emulator = GST_REC_EMULATOR (bin);

  g_assert (emulator->source == child);
  g_assert (GST_STATE (bin) == GST_STATE_NULL);
  g_assert (GST_STATE (child) == GST_STATE_NULL);

  /* remove ghost pad */
  gst_element_remove_pad (GST_ELEMENT (bin),
      gst_element_get_pad (GST_ELEMENT (bin), "src"));

  /* remove thread, if any */
  if (emulator->thread) {
    //g_assert (GST_STATE (emulator->thread) == GST_STATE_NULL);
    gst_bin_remove (GST_BIN (emulator->thread), child);
    gst_object_unref (GST_OBJECT (emulator->thread));
    emulator->thread = NULL;

    parent_class->remove_element (bin, emulator->queue);
    emulator->queue = NULL;

    //gst_element_set_state (emulator->bin, GST_STATE_NULL);
    if (emulator->bin) {
      gst_object_unref (GST_OBJECT (emulator->bin));
      emulator->bin = NULL;
    }
    emulator->dcb = NULL;
  } else {
    parent_class->remove_element (bin, child);
  }
  emulator->sourcepeer = NULL;
  emulator->source = NULL;
  emulator->teeemupad = NULL;

  if (emulator->frame) {
    gst_buffer_unref (emulator->frame);
    emulator->frame = NULL;
  }

  GST_LOG_OBJECT (emulator, "Removed child %p", child);
}

/*
 * Simplified autoplugging.
 */

static void try_to_plug (GstRecEmulator * emulator,
    GstPad * pad, const GstCaps * caps);

static void
cb_newpad (GstElement * element, GstPad * pad, gpointer data)
{
  GstCaps *caps;

  caps = gst_pad_get_caps (pad);
  try_to_plug (GST_REC_EMULATOR (data), pad, caps);
  gst_caps_free (caps);
}

static void
close_link (GstRecEmulator * emulator, GstPad * srcpad,
    GstElement * sinkelement, const gchar * padname, const GList * templlist)
{
  gboolean has_dynamic_pads = FALSE;

  GST_DEBUG_OBJECT (emulator,
      "Plugging pad %s:%s to newly created %s:%s",
      GST_DEBUG_PAD_NAME (srcpad), GST_ELEMENT_NAME (sinkelement), padname);

  /* add the element to the pipeline and set correct state */
  //gst_element_set_state (sinkelement, GST_STATE_PAUSED);
  gst_pad_link (srcpad, gst_element_get_pad (sinkelement, padname));
  if (sinkelement == emulator->bin) {
    //gst_element_set_state (emulator->bin, GST_STATE_PAUSED);
    gst_bin_add (GST_BIN (emulator->thread), emulator->bin);
    gst_bin_sync_children_state (GST_BIN (emulator->thread));
  } else {
    gst_bin_add (GST_BIN (emulator->dcb), sinkelement);
    gst_bin_sync_children_state (GST_BIN (emulator->dcb));
  }

  /* if we have static source pads, link those. If we have dynamic
   * source pads, listen for new-pad signals on the element */
  for ( ; templlist != NULL; templlist = templlist->next) {
    GstPadTemplate *templ = GST_PAD_TEMPLATE (templlist->data);

    /* only sourcepads, no request pads */
    if (templ->direction != GST_PAD_SRC ||
        templ->presence == GST_PAD_REQUEST) {
      continue;
    }

    switch (templ->presence) {
      case GST_PAD_ALWAYS: {
        GstPad *pad = gst_element_get_pad (sinkelement, templ->name_template);
        GstCaps *caps = gst_pad_get_caps (pad);

        /* link */
        try_to_plug (emulator, pad, caps);
        gst_caps_free (caps);
        break;
      }
      case GST_PAD_SOMETIMES:
        has_dynamic_pads = TRUE;
        break;
      default:
        break;
    }
  }

  /* listen for newly created pads if this element supports that */
  if (has_dynamic_pads) {
    g_signal_connect (sinkelement, "new-pad",
        G_CALLBACK (cb_newpad), emulator);
  }
}

static void
try_to_plug (GstRecEmulator * emulator, GstPad * pad,
    const GstCaps * caps)
{
  const gchar *mime = NULL;
  const GList *item;
  GstCaps *res, *peercaps;

  /* if there's no bin to plug to, just exit */
  if (emulator->bin == NULL) {
    GST_DEBUG_OBJECT (emulator, "bin is NULL");
    return;
  }
  /* don't plug if we're already plugged */
  if (GST_PAD_IS_LINKED (gst_element_get_pad (emulator->bin, "sink"))) {
    GST_DEBUG_OBJECT (emulator,
        "Omitting plugging %s:%s because we're linked already",
        GST_DEBUG_PAD_NAME (pad));
    return;
  }

  /* video only */
  if (caps) {
    mime = gst_structure_get_name (gst_caps_get_structure (caps, 0));
    if (g_strrstr (mime, "audio")) {
      GST_DEBUG_OBJECT (emulator,
	  "Omitting plugging %s:%s because it's audio",
	  GST_DEBUG_PAD_NAME (pad));
      return;
    }
  }

  /* can it link to the audiopad? */
  peercaps = gst_pad_get_caps (gst_element_get_pad (emulator->bin, "sink"));
  if (caps) {
    res = gst_caps_intersect (caps, peercaps);
    gst_caps_free (peercaps);
  } else {
    res = peercaps;
  }

  if (res && !gst_caps_is_empty (res)) {
    GST_DEBUG_OBJECT (emulator, "Got raw caps (%s), finished plugging", mime);
    close_link (emulator, pad, emulator->bin, "sink", NULL);
    gst_caps_free (res);
    return;
  } else if (res) {
    gst_caps_free (res);
  }

  /* try to plug from our list */
  for (item = GST_REC_EMULATOR_GET_CLASS (emulator)->factories;
       item != NULL; item = item->next) {
    GstElementFactory *factory = GST_ELEMENT_FACTORY (item->data);
    const GList *pads;

    for (pads = gst_element_factory_get_pad_templates (factory);
         pads != NULL; pads = pads->next) {
      GstPadTemplate *templ = GST_PAD_TEMPLATE (pads->data);

      /* find the sink template - need an always pad*/
      if (templ->direction != GST_PAD_SINK ||
          templ->presence != GST_PAD_ALWAYS) {
        continue;
      }

      /* can it link? */
      res = gst_caps_intersect (caps, templ->caps);
      if (res && !gst_caps_is_empty (res)) {
        GstElement *element;
        gchar *ename, *pname;
        guint num = 0;

        /* close link and return */
        gst_caps_free (res);
        ename = g_strdup_printf ("decoder-%d", num++);
        /* Element loading replaces templates, so the memory will be
         * corrupted after that - therefore, copy the variables we need.
         * The factory will stay valid, though. */
        pname = g_strdup (templ->name_template);
        element = gst_element_factory_create (factory, ename);
        g_free (ename);
        close_link (emulator, pad, element, pname,
            gst_element_factory_get_pad_templates (factory));
        g_free (pname);
        return;
      } else if (res) {
        gst_caps_free (res);
      }

      /* we only check one sink template per factory, so move on to the
       * next factory now */
      break;
    }
  }

  /* if we get here, no item was found */
  GST_WARNING_OBJECT (emulator,
      "Found no compatible pad to decode %s on %s:%s",
      mime, GST_DEBUG_PAD_NAME (pad));
}

/*
 * Apply filter caps. Only interesting if we emulate...
 */

void
gst_rec_emulator_set_filter (GstRecEmulator * emulator, GstCaps * filter)
{
  GstPad *pad, *peer, *binpad = NULL, *binpeer = NULL;
  GstStructure *str;
  const gchar *mime = NULL;

  /* save copy */
  if (emulator->filter != filter) {
    if (emulator->filter)
      gst_caps_free (emulator->filter);
    if (filter)
      emulator->filter = gst_caps_copy (filter);
    else
      emulator->filter = NULL;
  }

  /* nothing needed for no-emulation */
  if (!emulator->thread || !emulator->source) {
    if (filter) {
      gst_caps_free (filter);
    }
    return;
  }

  /* relink, filtered */
  GST_DEBUG_OBJECT (emulator, "Unlinking old");

  pad = gst_element_get_pad (emulator->source, "src");
  peer = gst_element_get_pad (emulator->sourcepeer, "sink");
  if (emulator->bin) {
    binpad = gst_element_get_pad (emulator->bin, "sink");
    binpeer = emulator->teeemupad;
  }

  /* don mess with links while playing */
  if (GST_STATE (emulator) >= GST_STATE_READY &&
      GST_STATE (emulator->thread) == GST_STATE_PLAYING) {
    GST_DEBUG_OBJECT (emulator, "Setting to paused");
    gst_element_set_state (emulator->thread, GST_STATE_PAUSED);
  }

  /* get rid of old link info... */
  if (GST_PAD_PEER (pad) == peer && peer)
    gst_pad_unlink (pad, peer);
  if (emulator->bin) {
    GstPad *teepad;

    if (emulator->dcb) {
      teepad = emulator->teeemupad;
      gst_bin_remove (GST_BIN (emulator->thread), emulator->dcb);
      emulator->dcb = NULL;
    } else {
      teepad = binpeer;
      if (GST_PAD_PEER (binpad) == binpeer && binpeer)
        gst_pad_unlink (binpeer, binpad);
    }
    if (teepad) {
      gst_element_release_request_pad (emulator->sourcepeer, teepad);
    }
    if (GST_ELEMENT_PARENT (emulator->bin)) {
      gst_bin_remove (GST_BIN (emulator->thread), emulator->bin);
    }
  }

  /* relink */
  if (filter && gst_caps_is_empty (filter)) {
    GST_LOG_OBJECT (emulator, "Not relinking with empty filter");
    return;
  } else if (filter && !gst_caps_is_any (filter)) {
    str = gst_caps_get_structure (filter, 0);
    mime = gst_structure_get_name (str);
    GST_LOG_OBJECT (emulator, "Relinking source with filter %s", mime);
  } else {
    GST_LOG_OBJECT (emulator, "Relinking source with no (any) filter");
  }

  /* setup new link. */
  gst_pad_link_filtered (pad, peer, filter);
  emulator->dcb = gst_bin_new ("videodec");
  //gst_element_set_state (emulator->bin, GST_STATE_PAUSED);
  emulator->teeemupad =
      gst_element_get_pad (emulator->tee, "src1");
  gst_bin_add (GST_BIN (emulator->thread), emulator->dcb);
  gst_bin_sync_children_state (GST_BIN (emulator->thread));
  try_to_plug (emulator, emulator->teeemupad, filter);

  /* now we can play */
  if (GST_STATE (emulator) >= GST_STATE_READY) {
    GST_DEBUG_OBJECT (emulator, "Setting to playing");
    gst_element_set_state (emulator->thread, GST_STATE_PLAYING);
  }
}

/*
 * Reset emulation mode.
 */

static void
gst_rec_emulator_set_mode (GstRecEmulator * emulator, GstRecEmulatorMode mode)
{
  GstElement *kid;
  GstElementState state = GST_STATE (emulator);

  /* safety first */
  g_return_if_fail (state <= GST_STATE_READY);

  /* mode = mode, no change */
  if (mode == emulator->mode)
    return;
  emulator->mode = mode;

  /* no kids means no change */
  if (!(kid = emulator->source))
    return;

  /* get kid, take an extra reference because we want to reuse it */
  gst_element_set_state (GST_ELEMENT (emulator), GST_STATE_NULL);
  gst_object_ref (GST_OBJECT (kid));
  gst_bin_remove (GST_BIN (emulator), kid);

  /* finish */
  gst_bin_add (GST_BIN (emulator), kid);
  gst_element_set_state (GST_ELEMENT (emulator), state);
}

/*
 * make interface-element accessible.
 */

GstElement *
find_recursive (GstRecEmulator *emulator, GstBin * bin, GType type)
{
  const GList *kids;

  for (kids = gst_bin_get_list (bin); kids != NULL; kids = kids->next) {
    GstElement *kid = kids->data;

    if (GST_IS_BIN (kid)) {
      GstElement *found;

      found = find_recursive (emulator, GST_BIN (kid), type);
      if (found)
        return found;
    } else if (gst_element_implements_interface (kid, type) &&
               emulator->source != kid)
      return kid;
  }

  return NULL;
}

GstElement *
gst_rec_emulator_get_by_interface (GstRecEmulator * emulator, GType type)
{
  if (emulator->mode != GST_REC_EMU_ALWAYS &&
      gst_element_implements_interface (emulator->source, type))
    return emulator->source;
  else if (emulator->thread)
    return find_recursive (emulator, GST_BIN (emulator->thread), type);

  return NULL;
}

static GstElementStateReturn
gst_rec_emulator_change_state (GstElement * element)
{
  GstRecEmulator *emulator = GST_REC_EMULATOR (element);
  GstElementStateReturn res;
  GstElementState pending = GST_STATE_PENDING (element),
      state = GST_STATE (element);
  gint transition = GST_STATE_TRANSITION (element);

  switch (transition) {
    case GST_STATE_NULL_TO_READY:
      if (emulator->thread) {
        GObjectClass *klass = G_OBJECT_GET_CLASS (emulator->source);

        GST_LOG_OBJECT (emulator, "Enabling interface emulation thread");
        if (gst_element_set_state (emulator->thread, GST_STATE_READY) !=
                GST_STATE_SUCCESS) {
          GST_DEBUG_OBJECT (emulator, "Emulation READY failed");
          return GST_STATE_FAILURE;
        }
        if (g_object_class_find_property (klass, "num-buffers")) {
          gint nbufs;

          g_object_get (G_OBJECT (emulator->source),
              "num-buffers", &nbufs, NULL);
        
          if (nbufs < 8 &&
              g_object_class_find_property (klass, "copy-mode")) {
            g_object_set (G_OBJECT (emulator->source),
                "copy-mode", TRUE, NULL);
          }
        }
      }
      break;
    default:
      break;
  }

  res = GST_ELEMENT_CLASS (parent_class)->change_state (element);
  if (res != GST_STATE_SUCCESS) {
    GST_DEBUG_OBJECT (emulator, "Failed parent state-change");
    if (transition == GST_STATE_NULL_TO_READY && emulator->thread) {
      gst_element_set_state (emulator->thread, GST_STATE_NULL);
    }
    return res;
  }

  switch (transition) {
    case GST_STATE_NULL_TO_READY:
      if (emulator->thread) {
        if (gst_element_set_state (emulator->thread, GST_STATE_PLAYING) !=
                GST_STATE_SUCCESS) {
          GST_DEBUG_OBJECT (emulator, "Failed to turn on emulation, NULL'ing");
          gst_element_set_state (emulator->thread, GST_STATE_NULL);
          gst_element_set_state (element, GST_STATE_NULL);
          return GST_STATE_FAILURE;
        }
      }
      break;
    case GST_STATE_READY_TO_NULL:
      if (emulator->thread) {
        GST_LOG_OBJECT (emulator, "Stopping interface emulation thread");
        gst_element_set_state (emulator->thread, GST_STATE_NULL);
      }
      break;
    default:
      break;
  }

  /* make sure pads of queue are deactive before PLAYING */
  if (emulator->queue) {
    GstPad *qpad = gst_element_get_pad (emulator->queue, "sink");

    if (pending != GST_STATE_PLAYING) {
      gst_pad_set_active (qpad, FALSE);
      gst_pad_set_active (GST_PAD_PEER (qpad), FALSE);
    } else {
      gst_pad_set_active (qpad, TRUE);
      gst_pad_set_active (GST_PAD_PEER (qpad), TRUE);
    }
  }

  GST_DEBUG_OBJECT (emulator, "state change succeeded (%d to %d)",
      state, pending);

  return res;
}

static void
gst_rec_emulator_get_property (GObject * object,
    guint prop_id, GValue * value, GParamSpec * pspec)
{
  GstRecEmulator *emulator = GST_REC_EMULATOR (object);

  switch (prop_id) {
    case ARG_FRAME:
      g_value_set_boxed (value, emulator->frame);
      break;
    case ARG_FORMAT:
      g_value_set_boxed (value,
          GST_PAD_CAPS (gst_element_get_pad (emulator->bin, "sink")));
      break;
    case ARG_MODE:
      g_value_set_enum (value, emulator->mode);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
gst_rec_emulator_set_property (GObject * object,
    guint prop_id, const GValue * value, GParamSpec * pspec)
{
  GstRecEmulator *emulator = GST_REC_EMULATOR (object);

  switch (prop_id) {
    case ARG_MODE:
      gst_rec_emulator_set_mode (emulator, g_value_get_enum (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}
