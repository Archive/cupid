/* GStreamer Recorder
 * (c) 2003 Ronald Bultje <rbultje@ronald.bitfreak.net>
 *
 * record.h: recording backend wrapper definition
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gst/gst.h>

#ifndef __GST_REC_H__
#define __GST_REC_H__

#define GST_TYPE_REC \
  (gst_rec_get_type())
#define GST_REC(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_REC, GstRec))
#define GST_REC_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass), GST_TYPE_REC, GstRecClass))
#define GST_IS_REC(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_TYPE_REC))
#define GST_IS_REC_CLASS(obj) \
  (G_TYPE_CHECK_CLASS_TYPE((klass), GST_TYPE_REC))

typedef struct _GstRec
{
  /* as we're a GstElement, we inherit all signals etc. */
  GstThread parent;

  /* we keep track of all elements here. Never gst_bin_add ()
   * an element to our bin, it'll break horribly. Use the
   * wrapper functions below instead. */
  GstElement *video_source,
      *audio_source, *video_encoder, *audio_encoder, *muxer, *output;
  GstCaps *video_filter, *audio_filter;

  /* Several "filter" options that can be used for format
   * setting on audio/video source. These are considered
   * "global" for each and every audio/video format. */
  guint video_width, video_height, audio_samplerate, audio_channels;
  gdouble video_fps;

  /* The rest is private elements, we use these for everything
   * that we need to connect elements with each other. */
  GstElement *video_source_thread,
      *audio_source_thread, *video_encoder_thread, *audio_encoder_thread;
  GstElement *video_source_queue,
      *audio_source_queue, *video_encoder_queue, *audio_encoder_queue;
  GstElement *video_conv, *audio_conv;

  /* a manager handles private events. */
  GstElement *video_source_manager, *audio_source_manager;
  GstElement *video_source_emulator, *audio_source_emulator;

  /* Has any buffer passed yet? */
  gboolean started;

  /* clock */
  GstClock *clock;

  /* keep track of lost/ins/del */
  guint lost_frames, ins_frames, del_frames;

  /* metadata */
  GstTagList *taglist;

  /* error tracking */
  gboolean in_error;
} GstRec;

typedef struct _GstRecClass
{
  GstThreadClass parent;

  /* signals */
  void (*statistics) (GstRec * rec,
      GstClockTime recording_time,
      guint64 video_frames,
      guint64 audio_samples,
      guint lost_frames, guint ins_frames, guint del_frames);

  void (*video_source_changed) (GstRec * rec, GstElement * video_source);
  void (*audio_source_changed) (GstRec * rec, GstElement * audio_source);
  void (*video_encoder_changed) (GstRec * rec, GstElement * video_encoder);
  void (*audio_encoder_changed) (GstRec * rec, GstElement * audio_encoder);
  void (*video_filter_changed) (GstRec * rec, const GstCaps * video_filter);
  void (*audio_filter_changed) (GstRec * rec, const GstCaps * audio_filter);
  void (*muxer_changed) (GstRec * rec, GstElement * muxer);
  void (*output_changed) (GstRec * rec, GstElement * output);

  void (*video_width_changed) (GstRec * rec, guint video_width);
  void (*video_height_changed) (GstRec * rec, guint video_height);
  void (*video_fps_changed) (GstRec * rec, gdouble video_fps);
  void (*audio_samplerate_changed) (GstRec * rec, guint audio_samplerate);
  void (*audio_channels_changed) (GstRec * rec, guint audio_channels);
} GstRecClass;

GType gst_rec_get_type (void);

/*
 * Create an instance. Wonderful macro.
 */
#define gst_rec_new() ((GstRec *) g_object_new (GST_TYPE_REC, NULL))

/*
 * Interface retrieval...
 */
GstElement * gst_rec_video_get_by_interface (GstRec * rec, GType type);
GstElement * gst_rec_audio_get_by_interface (GstRec * rec, GType type);

/*
 * You normally use gst_element_set_state () for state
 * handling. However, to stop recording and properly
 * handle EOS, please use this instead.
 * (FIXME: does anyone know a nicer way to do this?)
 *
 * Preferred states:
 *  - GST_STATE_NULL for stop
 *  - GST_STATE_{PAUSED,PLAYING} for pause/playing
 */
GstElementStateReturn gst_rec_set_state (GstRec * rec, GstElementState state);

/*
 * wrapper functions for control of various things here.
 * (that's a clear explanation, isn't it? :) )
 */
void gst_rec_set_video_source (GstRec * rec, GstElement * video_source);
void gst_rec_set_audio_source (GstRec * rec, GstElement * audio_source);
void gst_rec_set_video_encoder (GstRec * rec, GstElement * video_encoder);
void gst_rec_set_audio_encoder (GstRec * rec, GstElement * audio_encoder);
void gst_rec_set_muxer (GstRec * rec, GstElement * muxer);
void gst_rec_set_output (GstRec * rec, GstElement * output);
void gst_rec_set_video_filter (GstRec * rec, GstCaps * video_filter);
void gst_rec_set_audio_filter (GstRec * rec, GstCaps * audio_filter);
void gst_rec_set_video_width (GstRec * rec, guint width);
void gst_rec_set_video_height (GstRec * rec, guint height);
void gst_rec_set_video_fps (GstRec * rec, gdouble fps);
void gst_rec_set_audio_samplerate (GstRec * rec, guint samplerate);
void gst_rec_set_audio_channels (GstRec * rec, guint channels);

/*
 * Corresponding _get () functions.
 */
GstElement *gst_rec_get_video_source (GstRec * rec);
GstElement *gst_rec_get_audio_source (GstRec * rec);
GstElement *gst_rec_get_video_encoder (GstRec * rec);
GstElement *gst_rec_get_audio_encoder (GstRec * rec);
GstElement *gst_rec_get_muxer (GstRec * rec);
GstElement *gst_rec_get_output (GstRec * rec);
GstCaps *gst_rec_get_video_filter (GstRec * rec);
GstCaps *gst_rec_get_audio_filter (GstRec * rec);
guint gst_rec_get_video_width (GstRec * rec);
guint gst_rec_get_video_height (GstRec * rec);
gdouble gst_rec_get_video_fps (GstRec * rec);
guint gst_rec_get_audio_samplerate (GstRec * rec);
guint gst_rec_get_audio_channels (GstRec * rec);

/*
 * Snapshot support.
 */
gboolean gst_rec_get_snapshot (GstRec * rec, GstBuffer ** buffer,
    GstCaps ** caps);

/*
 * Metadata.
 */

const gchar *gst_rec_get_tag (GstRec * rec, const gchar * tag);
void gst_rec_set_tag (GstRec * rec, const gchar * tag, const gchar * value);

#endif /* __GST_REC_H__ */
