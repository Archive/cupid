/* GStreamer Recorder
 * (c) 2003 Ronald Bultje <rbultje@ronald.bitfreak.net>
 *
 * record.c: recording backend wrapper
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Note for anyone trying to read and understand this: this stuff
 * is actually *hard*. I know that, I wrote it. ;). For a detailed
 * description of what kind of evil magic foo is going on here,
 * please have a look at docs/pipeline.txt. Be sure to have that
 * at hand when reading this, or you won't understand a bit. It
 * explains the reason for each element in the pipeline, how it is
 * built up and why it is built up like that.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <string.h>

#include <gst/mixer/mixer.h>
#include <gst/colorbalance/colorbalance.h>
#include <gst/tuner/tuner.h>
#include <gst/xoverlay/xoverlay.h>

#include "debug.h"
#include "emulator.h"
#include "marshal.h"
#include "record.h"

/* signals */
enum
{
  SIGNAL_STATISTICS,
  SIGNAL_VIDEO_SOURCE_CHANGED,
  SIGNAL_AUDIO_SOURCE_CHANGED,
  SIGNAL_VIDEO_ENCODER_CHANGED,
  SIGNAL_AUDIO_ENCODER_CHANGED,
  SIGNAL_VIDEO_FILTER_CHANGED,
  SIGNAL_AUDIO_FILTER_CHANGED,
  SIGNAL_MUXER_CHANGED,
  SIGNAL_OUTPUT_CHANGED,
  SIGNAL_VIDEO_WIDTH_CHANGED,
  SIGNAL_VIDEO_HEIGHT_CHANGED,
  SIGNAL_VIDEO_FPS_CHANGED,
  SIGNAL_AUDIO_SAMPLERATE_CHANGED,
  SIGNAL_AUDIO_CHANNELS_CHANGED,
  LAST_SIGNAL
};

static void gst_rec_init (GstRec * stats);
static void gst_rec_class_init (GstRecClass * klass);
static void gst_rec_dispose (GObject * object);

static GstElementStateReturn gst_rec_change_state (GstElement * element);
static void gst_rec_error (GstElement * element,
    GstElement * source, GError * error, gchar * debug);
static void gst_rec_eos (GstElement * element);
static gboolean gst_rec_iterate (GstBin * bin, gpointer data);
static void gst_rec_set_clock (GstElement * element, GstClock * clock);

static GstThreadClass *parent_class = NULL;
static guint signals[LAST_SIGNAL] = { 0 };

GType
gst_rec_get_type (void)
{
  static GType gst_rec_type = 0;

  if (!gst_rec_type) {
    static const GTypeInfo gst_rec_info = {
      sizeof (GstRecClass),
      NULL,
      NULL,
      (GClassInitFunc) gst_rec_class_init,
      NULL,
      NULL,
      sizeof (GstRec),
      0,
      (GInstanceInitFunc) gst_rec_init,
      NULL
    };

    gst_rec_type = g_type_register_static (GST_TYPE_THREAD,
        "GstRec", &gst_rec_info, 0);
  }

  return gst_rec_type;
}

static void
gst_rec_class_init (GstRecClass * klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GstElementClass *gstelement_class = GST_ELEMENT_CLASS (klass);

  parent_class = g_type_class_ref (GST_TYPE_THREAD);

  /* signal definitions */
  signals[SIGNAL_STATISTICS] =
      g_signal_new ("statistics",
      G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST,
      G_STRUCT_OFFSET (GstRecClass, statistics),
      NULL, NULL,
      gst_rec_marshal_VOID__UINT64_UINT64_UINT64_UINT_UINT_UINT,
      G_TYPE_NONE, 6, G_TYPE_UINT64, G_TYPE_UINT64,
      G_TYPE_UINT64, G_TYPE_UINT, G_TYPE_UINT, G_TYPE_UINT);

  signals[SIGNAL_VIDEO_SOURCE_CHANGED] =
      g_signal_new ("video_source_changed",
      G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST,
      G_STRUCT_OFFSET (GstRecClass, video_source_changed),
      NULL, NULL, g_cclosure_marshal_VOID__OBJECT,
      G_TYPE_NONE, 1, GST_TYPE_ELEMENT);
  signals[SIGNAL_AUDIO_SOURCE_CHANGED] =
      g_signal_new ("audio_source_changed",
      G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST,
      G_STRUCT_OFFSET (GstRecClass, audio_source_changed),
      NULL, NULL, g_cclosure_marshal_VOID__OBJECT,
      G_TYPE_NONE, 1, GST_TYPE_ELEMENT);

  signals[SIGNAL_VIDEO_ENCODER_CHANGED] =
      g_signal_new ("video_encoder_changed",
      G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST,
      G_STRUCT_OFFSET (GstRecClass, video_encoder_changed),
      NULL, NULL, g_cclosure_marshal_VOID__OBJECT,
      G_TYPE_NONE, 1, GST_TYPE_ELEMENT);
  signals[SIGNAL_AUDIO_ENCODER_CHANGED] =
      g_signal_new ("audio_encoder_changed",
      G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST,
      G_STRUCT_OFFSET (GstRecClass, audio_encoder_changed),
      NULL, NULL, g_cclosure_marshal_VOID__OBJECT,
      G_TYPE_NONE, 1, GST_TYPE_ELEMENT);

  signals[SIGNAL_VIDEO_FILTER_CHANGED] =
      g_signal_new ("video_filter_changed",
      G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST,
      G_STRUCT_OFFSET (GstRecClass, video_filter_changed),
      NULL, NULL, g_cclosure_marshal_VOID__POINTER,
      G_TYPE_NONE, 1, G_TYPE_POINTER);
  signals[SIGNAL_AUDIO_FILTER_CHANGED] =
      g_signal_new ("audio_filter_changed",
      G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST,
      G_STRUCT_OFFSET (GstRecClass, audio_filter_changed),
      NULL, NULL, g_cclosure_marshal_VOID__POINTER,
      G_TYPE_NONE, 1, G_TYPE_POINTER);

  signals[SIGNAL_MUXER_CHANGED] =
      g_signal_new ("muxer_changed",
      G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST,
      G_STRUCT_OFFSET (GstRecClass, muxer_changed),
      NULL, NULL, g_cclosure_marshal_VOID__OBJECT,
      G_TYPE_NONE, 1, GST_TYPE_ELEMENT);

  signals[SIGNAL_OUTPUT_CHANGED] =
      g_signal_new ("output_changed",
      G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST,
      G_STRUCT_OFFSET (GstRecClass, output_changed),
      NULL, NULL, g_cclosure_marshal_VOID__OBJECT,
      G_TYPE_NONE, 1, GST_TYPE_ELEMENT);

  signals[SIGNAL_VIDEO_WIDTH_CHANGED] =
      g_signal_new ("video_width_changed",
      G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST,
      G_STRUCT_OFFSET (GstRecClass, video_width_changed),
      NULL, NULL, g_cclosure_marshal_VOID__UINT, G_TYPE_NONE, 1, G_TYPE_UINT);
  signals[SIGNAL_VIDEO_HEIGHT_CHANGED] =
      g_signal_new ("video_height_changed",
      G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST,
      G_STRUCT_OFFSET (GstRecClass, video_height_changed),
      NULL, NULL, g_cclosure_marshal_VOID__UINT, G_TYPE_NONE, 1, G_TYPE_UINT);
  signals[SIGNAL_VIDEO_FPS_CHANGED] =
      g_signal_new ("video_fps_changed",
      G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST,
      G_STRUCT_OFFSET (GstRecClass, video_fps_changed),
      NULL, NULL, g_cclosure_marshal_VOID__DOUBLE, G_TYPE_NONE, 1,
      G_TYPE_DOUBLE);

  signals[SIGNAL_AUDIO_SAMPLERATE_CHANGED] =
      g_signal_new ("audio_samplerate_changed",
      G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST,
      G_STRUCT_OFFSET (GstRecClass, audio_samplerate_changed),
      NULL, NULL, g_cclosure_marshal_VOID__UINT, G_TYPE_NONE, 1, G_TYPE_UINT);
  signals[SIGNAL_AUDIO_CHANNELS_CHANGED] =
      g_signal_new ("audio_channels_changed",
      G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST,
      G_STRUCT_OFFSET (GstRecClass, audio_channels_changed),
      NULL, NULL, g_cclosure_marshal_VOID__UINT, G_TYPE_NONE, 1, G_TYPE_UINT);

  gobject_class->dispose = gst_rec_dispose;
  gstelement_class->change_state = gst_rec_change_state;
  gstelement_class->eos = gst_rec_eos;
  gstelement_class->error = gst_rec_error;
  gstelement_class->set_clock = gst_rec_set_clock;
}

static void
cb_error (GstElement * emulator, GstElement * _source,
    GError * error, gchar * debug, gpointer data)
{
  GstObject *source, *parent;

  if (GST_STATE (data) != GST_STATE_NULL)
    return;

  source = GST_OBJECT (_source);
  parent = GST_OBJECT (data);

  /* tell ourselves */
  gst_object_ref (source);
  gst_object_ref (parent);
  GST_DEBUG ("forwarding error \"%s\" from %s to %s", error->message,
      GST_ELEMENT_NAME (source), GST_OBJECT_NAME (parent));

  g_signal_emit_by_name (G_OBJECT (parent), "error", source, error, debug);

  GST_DEBUG ("forwarded error \"%s\" from %s to %s", error->message,
      GST_ELEMENT_NAME (source), GST_OBJECT_NAME (parent));
  gst_object_unref (source);
  gst_object_unref (parent);
}

static void
gst_rec_init (GstRec * rec)
{
  GstElement *e[14];
  gint n;
  GstElementFactory *qf = gst_element_factory_find ("queue"),
      *mf = gst_element_factory_find ("manager"),
      *em = gst_element_factory_find ("emulator");

  gst_object_set_name (GST_OBJECT (rec), "gst-rec-pipeline");

  /* public/external elements */
  rec->video_source = NULL;
  rec->audio_source = NULL;
  rec->video_encoder = NULL;
  rec->audio_encoder = NULL;
  rec->muxer = NULL;
  rec->output = NULL;
  rec->video_filter = NULL;
  rec->audio_filter = NULL;

  rec->video_width = 384;
  rec->video_height = 288;
  rec->video_fps = 25.;

  rec->audio_samplerate = 44100;
  rec->audio_channels = 1;

  rec->taglist = gst_tag_list_new ();

  rec->clock = NULL;
  rec->in_error = FALSE;

  /* FIXME: thread priority works for root only */

  /* private/internal elements */
  e[0] = rec->video_source_thread = gst_thread_new ("video-source-thread");
  /*gst_thread_set_priority (GST_THREAD (rec->video_source_thread),
     G_THREAD_PRIORITY_HIGH); */
  e[1] = rec->audio_source_thread = gst_thread_new ("audio-source-thread");
  /*gst_thread_set_priority (GST_THREAD (rec->audio_source_thread),
     G_THREAD_PRIORITY_HIGH); */
  e[2] = rec->video_encoder_thread = gst_thread_new ("video-encoder-thread");
  e[3] = rec->audio_encoder_thread = gst_thread_new ("audio-encoder-thread");
  e[4] = rec->video_source_queue =
      gst_element_factory_create (qf, "video-source-queue");
  e[5] = rec->audio_source_queue =
      gst_element_factory_create (qf, "audio-source-queue");
  e[6] = rec->video_encoder_queue =
      gst_element_factory_create (qf, "video-encoder-queue");
  e[7] = rec->audio_encoder_queue =
      gst_element_factory_create (qf, "audio-encoder-queue");
  e[8] = rec->video_source_manager =
      gst_element_factory_create (mf, "video-source-manager");
  e[9] = rec->audio_source_manager =
      gst_element_factory_create (mf, "audio-source-manager");
  e[10] = rec->video_source_emulator =
      gst_element_factory_create (em, "video-source-emulator");
  g_signal_connect (e[10], "error", G_CALLBACK (cb_error), rec);
  e[11] = rec->audio_source_emulator =
      gst_element_factory_create (em, "audio-source-emulator");
  g_signal_connect (e[11], "error", G_CALLBACK (cb_error), rec);
  e[12] = rec->audio_conv =
      gst_element_factory_make ("audioconvert", "audio-conversion");
  e[13] = rec->video_conv =
      gst_element_factory_make ("ffmpegcolorspace", "video-conversion");

  /* OK, now, someone really needs to explain me why this ref/sink
   * system actually helps in building large applications (it
   * doesn't), and especially why there's no single way in which I
   * can make objects that will *not* be FLOATING on creation.
   * In the end, this leads to horrible, over-complicated and
   * dumb recursive code like below (create, ref, sink). Some
   * people actually want to keep track of objects themselves.
   *
   * I understand what floating is and why it is useful, but not
   * in any case, so it should be optional. */
  for (n = 0; n < 14; n++) {
    gst_object_ref (GST_OBJECT (e[n]));
    gst_object_sink (GST_OBJECT (e[n]));
  }

  /* nothing happens */
  rec->started = FALSE;
  rec->lost_frames = 0;
  rec->ins_frames = 0;
  rec->del_frames = 0;

  /* aren't we boring? ;). */

  /* for statistics */
  g_signal_connect (rec, "iterate", G_CALLBACK (gst_rec_iterate), NULL);
}

static void
gst_rec_dispose (GObject * object)
{
  GstRec *rec = GST_REC (object);

  /* make sure we're NULL (required for the rest here) */
  gst_element_set_state (GST_ELEMENT (rec), GST_STATE_NULL);

  /* clear references (if any) of public/external elements */
  gst_rec_set_video_source (rec, NULL);
  gst_rec_set_audio_source (rec, NULL);
  gst_rec_set_video_encoder (rec, NULL);
  gst_rec_set_audio_encoder (rec, NULL);
  gst_rec_set_muxer (rec, NULL);
  gst_rec_set_output (rec, NULL);
  gst_rec_set_video_filter (rec, NULL);
  gst_rec_set_audio_filter (rec, NULL);

  /* get rid of internal/private elements */
  gst_object_unref (GST_OBJECT (rec->video_source_thread));
  gst_object_unref (GST_OBJECT (rec->audio_source_thread));
  gst_object_unref (GST_OBJECT (rec->video_encoder_thread));
  gst_object_unref (GST_OBJECT (rec->audio_encoder_thread));

  gst_object_unref (GST_OBJECT (rec->video_conv));
  gst_object_unref (GST_OBJECT (rec->audio_conv));

  gst_object_unref (GST_OBJECT (rec->video_source_queue));
  gst_object_unref (GST_OBJECT (rec->audio_source_queue));
  gst_object_unref (GST_OBJECT (rec->video_encoder_queue));
  gst_object_unref (GST_OBJECT (rec->audio_encoder_queue));

  gst_object_unref (GST_OBJECT (rec->video_source_manager));
  gst_object_unref (GST_OBJECT (rec->audio_source_manager));

  gst_object_unref (GST_OBJECT (rec->video_source_emulator));
  gst_object_unref (GST_OBJECT (rec->audio_source_emulator));

  gst_tag_list_free (rec->taglist);

  if (((GObjectClass *) parent_class)->dispose)
    ((GObjectClass *) parent_class)->dispose (object);
}

/*
 * After each iteration, we'd like to make some statistics
 * of what we're doing. We do that in the signal callback.
 *
 * Note that we use idle handlers for this, that has to do
 * with thread madness.
 */

static gboolean
gst_rec_create_stats (gpointer data)
{
  GstRec *rec = GST_REC (data);
  guint64 frames = 0, samples = 0;
  GstClockTime time = 0;
  GstFormat format;
  gint64 value;

  /* frames */
  if (rec->video_source) {
    format = GST_FORMAT_DEFAULT;
    if (gst_element_query (rec->video_source,
            GST_QUERY_POSITION, &format, &value)) {
      frames = value;
    }
  }

  /* samples */
  if (rec->audio_source) {
    format = GST_FORMAT_DEFAULT;
    if (gst_element_query (rec->audio_source,
            GST_QUERY_POSITION, &format, &value)) {
      samples = value;
    }
  }

  /* time */
  if (rec->clock) {
    time = gst_clock_get_time (rec->clock);
  } else {
    format = GST_FORMAT_TIME;
    if (rec->audio_source &&
        gst_element_query (rec->audio_source,
            GST_QUERY_POSITION, &format, &value)) {
      time = value;
    } else if (rec->video_source &&
        gst_element_query (rec->video_source,
            GST_QUERY_POSITION, &format, &value)) {
      time = value;
    }
    /* else: panic! */
  }

  g_signal_emit (G_OBJECT (rec), signals[SIGNAL_STATISTICS], 0,
      time, frames, samples,
      rec->lost_frames, rec->ins_frames, rec->del_frames);

  /* once */
  return FALSE;
}

static gboolean
gst_rec_iterate (GstBin * bin, gpointer data)
{
  GstRec *rec = GST_REC (bin);

  /* run in own thread */
  g_idle_add ((GSourceFunc) gst_rec_create_stats, (gpointer) rec);

  /* well, it requires a return value... */
  return FALSE;
}

static void
gst_rec_set_clock (GstElement * element, GstClock * clock)
{
  GST_REC (element)->clock = clock;

  if (GST_ELEMENT_CLASS (parent_class)->set_clock)
    GST_ELEMENT_CLASS (parent_class)->set_clock (element, clock);
}

/*
 * These four are the real 'fancy' functions in this class,
 * they're responsible for setting up the whole pipeline
 * and taking care of good connections between everything.
 *
 * gst_rec_link_pre () checks for the existance of video/
 * audio sources, encoders and outputs elements, and sets
 * up connections between them as needed. It also takes
 * care of threading and all that.
 *
 * gst_rec_link_post () adds video/audio source to the bin.
 *
 * gst_rec_unlink_pre () removes video/audio source from
 * the bin.
 *
 * gst_rec_unlink_post () unlinks everything and removes
 * all other elements.
 */

static gboolean
gst_rec_link_pre (GstRec * rec)
{
  GList *padtempllist;

  /* we need one source (at least). */
  g_return_val_if_fail (rec->video_source != NULL ||
      rec->audio_source != NULL, FALSE);

  /* for each source, we need either a filter or an encoder. */
  if (rec->video_source) {
    g_return_val_if_fail ((rec->video_encoder != NULL) ^
        (rec->video_filter != NULL), FALSE);
    g_return_val_if_fail (rec->video_width != 0 &&
        rec->video_height != 0, FALSE);
    g_return_val_if_fail (rec->video_fps != 0., FALSE);
  }
  if (rec->audio_source) {
    g_return_val_if_fail ((rec->audio_encoder != NULL) ^
        (rec->audio_filter != NULL), FALSE);
    g_return_val_if_fail (rec->audio_samplerate != 0 &&
        rec->audio_channels != 0, FALSE);
  }

  /* we definately need output */
  g_return_val_if_fail (rec->muxer != NULL && rec->output != NULL, FALSE);

  /* FIXME: do some checkings on whether the pads-to-link are
   * actually compatible. */

  /* Set up the pipeline - start with video */
  if (rec->video_source) {
    GstPadTemplate *mux_pad_templ = NULL;
    GstPad *mux_pad, *peer;
    GstCaps *filter = gst_caps_new_empty (), *peercaps, *rescaps;
    guint n;

    if (rec->video_encoder) {
      GstCaps *src;

      src = g_object_get_data (G_OBJECT (rec->video_source), "template");
      if (src) {
        src = gst_caps_copy (src);
      } else {
        src = gst_pad_get_caps (gst_element_get_pad (rec->video_source,
            "src"));
      }

      /* make a filter */
      for (n = 0; n < gst_caps_get_size (src); n++) {
        GstStructure *src_s = gst_caps_get_structure (src, n);
        const gchar *src_s_name = gst_structure_get_name (src_s);
        guint i;

        /* do we have this one already? */
        for (i = 0; i < gst_caps_get_size (filter); i++) {
          GstStructure *filter_s = gst_caps_get_structure (filter, i);

          if (!strcmp (src_s_name, gst_structure_get_name (filter_s)))
            break;
        }

        /* if not, then ... */
        if (i == gst_caps_get_size (filter)) {
          gst_caps_append_structure (filter,
              gst_structure_empty_new (src_s_name));
        }
      }

      gst_element_link_many (rec->video_source_queue, rec->video_conv,
          rec->video_encoder, rec->video_encoder_queue, NULL);

      gst_bin_add_many (GST_BIN (rec->video_encoder_thread),
          rec->video_conv, rec->video_encoder, rec->video_encoder_queue, NULL);
      gst_bin_add (GST_BIN (rec), rec->video_encoder_thread);

      peer = gst_element_get_pad (rec->video_encoder_queue, "src");
    } else {
      filter = gst_caps_copy (rec->video_filter);
      peer = gst_element_get_pad (rec->video_source_queue, "src");
    }

    /* get pad from muxer */
    peercaps = g_object_get_data (G_OBJECT (gst_pad_get_parent (peer)),
        "template");
    if (peercaps) {
      peercaps = gst_caps_copy (peercaps);
    } else {
      peercaps = gst_pad_get_caps (peer);
    }
    for (padtempllist = gst_element_get_pad_template_list (rec->muxer);
        padtempllist != NULL; padtempllist = padtempllist->next) {
      GstPadTemplate *templ = padtempllist->data;

      if (templ->direction != GST_PAD_SINK ||
          !strncmp (templ->name_template, "audio_", 6))
        continue;
      rescaps = gst_caps_intersect (peercaps,
          gst_pad_template_get_caps (templ));
      if (!gst_caps_is_empty (rescaps))
        mux_pad_templ = templ;
      gst_caps_free (rescaps);
      if (mux_pad_templ)
        break;
    }
    gst_caps_free (peercaps);
    g_return_val_if_fail (mux_pad_templ != NULL, FALSE);
    mux_pad = gst_element_get_request_pad (rec->muxer,
        mux_pad_templ->name_template);
    g_return_val_if_fail (mux_pad != NULL, FALSE);

    /* merge start filter with width/height props and set it */
    for (n = 0; n < gst_caps_get_size (filter); n++) {
      GstStructure *structure = gst_caps_get_structure (filter, n);

      gst_structure_set (structure,
          "width", G_TYPE_INT, (gint) rec->video_width,
          "height", G_TYPE_INT, (gint) rec->video_height,
          "framerate", G_TYPE_DOUBLE, rec->video_fps, NULL);
    }

    gst_element_link_filtered (rec->video_source_manager,
        rec->video_source_queue, filter);

    gst_pad_link (peer, mux_pad);

    /* we add the video source later... */
    gst_bin_add_many (GST_BIN (rec->video_source_thread),
        rec->video_source_manager, rec->video_source_queue, NULL);

    gst_bin_add (GST_BIN (rec), rec->video_source_thread);
  }

  /* Set up the pipeline - next comes audio */
  if (rec->audio_source) {
    GstPadTemplate *mux_pad_templ = NULL;
    GstPad *mux_pad, *peer;
    GstCaps *filter = gst_caps_new_empty (), *peercaps, *rescaps;
    guint n;

    if (rec->audio_encoder) {
      GstCaps *src = gst_pad_get_caps (gst_element_get_pad (rec->audio_source,
              "src"));

      /* make a filter */
      for (n = 0; n < gst_caps_get_size (src); n++) {
        GstStructure *src_s = gst_caps_get_structure (src, n);
        const gchar *src_s_name = gst_structure_get_name (src_s);
        guint i;

        /* do we have this one already? */
        for (i = 0; i < gst_caps_get_size (filter); i++) {
          GstStructure *filter_s = gst_caps_get_structure (filter, i);

          if (!strcmp (src_s_name, gst_structure_get_name (filter_s)))
            break;
        }

        /* if not, then ... */
        if (i == gst_caps_get_size (filter)) {
          gst_caps_append_structure (filter,
              gst_structure_empty_new (src_s_name));
        }
      }

      gst_element_link_many (rec->audio_source_queue, rec->audio_conv,
          rec->audio_encoder, rec->audio_encoder_queue, NULL);

      gst_bin_add_many (GST_BIN (rec->audio_encoder_thread),
          rec->audio_conv, rec->audio_encoder, rec->audio_encoder_queue, NULL);
      gst_bin_add (GST_BIN (rec), rec->audio_encoder_thread);

      peer = gst_element_get_pad (rec->audio_encoder_queue, "src");
    } else {
      filter = gst_caps_copy (rec->audio_filter);
      peer = gst_element_get_pad (rec->audio_source_queue, "src");
    }

    /* get pad from muxer */
    peercaps = gst_pad_get_caps (peer);
    for (padtempllist = gst_element_get_pad_template_list (rec->muxer);
        padtempllist != NULL; padtempllist = padtempllist->next) {
      GstPadTemplate *templ = padtempllist->data;

      if (templ->direction != GST_PAD_SINK ||
          !strncmp (templ->name_template, "video_", 6))
        continue;
      rescaps = gst_caps_intersect (peercaps,
          gst_pad_template_get_caps (templ));
      if (!gst_caps_is_empty (rescaps))
        mux_pad_templ = templ;
      gst_caps_free (rescaps);
      if (mux_pad_templ)
        break;
    }
    gst_caps_free (peercaps);
    g_return_val_if_fail (mux_pad_templ != NULL, FALSE);
    mux_pad = gst_element_get_request_pad (rec->muxer,
        mux_pad_templ->name_template);
    g_return_val_if_fail (mux_pad != NULL, FALSE);

    /* merge start filter with samplerate/channels props and set it */
    for (n = 0; n < gst_caps_get_size (filter); n++) {
      GstStructure *structure = gst_caps_get_structure (filter, n);

      gst_structure_set (structure,
          "rate", G_TYPE_INT, (gint) rec->audio_samplerate,
          "channels", G_TYPE_INT, (gint) rec->audio_channels, NULL);
    }

    gst_element_link_filtered (rec->audio_source_manager,
        rec->audio_source_queue, filter);

    gst_pad_link (peer, mux_pad);

    /* we add the audio source later... */
    gst_bin_add_many (GST_BIN (rec->audio_source_thread),
        rec->audio_source_manager, rec->audio_source_queue, NULL);

    gst_bin_add (GST_BIN (rec), rec->audio_source_thread);
  }

  /* Set up pipeline - lastly, the output */
  gst_element_link (rec->muxer, rec->output);
  gst_bin_add_many (GST_BIN (rec), rec->muxer, rec->output, NULL);

  return TRUE;
}

static void
gst_rec_link_post (GstRec * rec)
{
  if (rec->video_source) {
    gst_bin_add (GST_BIN (rec->video_source_manager),
        rec->video_source_emulator);
  }

  if (rec->audio_source) {
    gst_bin_add (GST_BIN (rec->audio_source_manager),
        rec->audio_source_emulator);
  }
}

static void
gst_rec_unlink_pre (GstRec * rec)
{
  if (rec->video_source)
    gst_bin_remove (GST_BIN (rec->video_source_manager),
        rec->video_source_emulator);

  if (rec->audio_source)
    gst_bin_remove (GST_BIN (rec->audio_source_manager),
        rec->audio_source_emulator);
}

static void
gst_rec_unlink_post (GstRec * rec)
{
  const GList *pads;

  /* Break down the pipeline - start with video */
  if (rec->video_source) {
    GstPad *peer;

    if (rec->video_encoder) {
      gst_element_unlink_many (rec->video_source_manager,
          rec->video_source_queue, rec->video_conv,
          rec->video_encoder, rec->video_encoder_queue, NULL);

      gst_bin_remove_many (GST_BIN (rec->video_encoder_thread),
          rec->video_conv, rec->video_encoder, rec->video_encoder_queue, NULL);
      gst_bin_remove (GST_BIN (rec), rec->video_encoder_thread);

      peer = gst_element_get_pad (rec->video_encoder_queue, "src");
    } else {
      gst_element_unlink (rec->video_source_manager, rec->video_source_queue);
      peer = gst_element_get_pad (rec->video_source_queue, "src");
    }

    if (GST_PAD_PEER (peer))
      gst_pad_unlink (peer, GST_PAD_PEER (peer));

    gst_bin_remove_many (GST_BIN (rec->video_source_thread),
        rec->video_source_manager, rec->video_source_queue, NULL);
    gst_bin_remove (GST_BIN (rec), rec->video_source_thread);
  }

  /* Break down the pipeline - next comes audio */
  if (rec->audio_source) {
    GstPad *peer;

    if (rec->audio_encoder) {
      gst_element_unlink_many (rec->audio_source_manager,
          rec->audio_source_queue, rec->audio_conv,
          rec->audio_encoder, rec->audio_encoder_queue, NULL);

      gst_bin_remove_many (GST_BIN (rec->audio_encoder_thread),
          rec->audio_conv, rec->audio_encoder, rec->audio_encoder_queue, NULL);
      gst_bin_remove (GST_BIN (rec), rec->audio_encoder_thread);

      peer = gst_element_get_pad (rec->audio_encoder_queue, "src");
    } else {
      gst_element_unlink (rec->audio_source_manager, rec->audio_source_queue);
      peer = gst_element_get_pad (rec->audio_source_queue, "src");
    }

    if (GST_PAD_PEER (peer))
      gst_pad_unlink (peer, GST_PAD_PEER (peer));

    gst_bin_remove_many (GST_BIN (rec->audio_source_thread),
        rec->audio_source_manager, rec->audio_source_queue, NULL);
    gst_bin_remove (GST_BIN (rec), rec->audio_source_thread);
  }

  /* Break down pipeline - lastly, the output */
  gst_element_unlink (rec->muxer, rec->output);
  gst_bin_remove_many (GST_BIN (rec), rec->muxer, rec->output, NULL);
  pads = gst_element_get_pad_list (rec->muxer);
  while (pads) {
    GstPad *pad = pads->data;

    pads = pads->next;
    if (GST_PAD_IS_SINK (pad))
      gst_element_release_request_pad (rec->muxer, pad);
  }
}

static void
cb_set_tag (const GstTagList * taglist, const gchar * tag, gpointer data)
{
  gchar *value;

  if (gst_tag_list_get_string (taglist, tag, &value) &&
      value[0] != '\0') {
    gst_tag_setter_add (data, GST_TAG_MERGE_REPLACE, tag, value, NULL);
  }
}

static GstElementStateReturn
gst_rec_change_state (GstElement * element)
{
  gint transition = GST_STATE_TRANSITION (element);
  GstRec *rec = GST_REC (element);
  GstElementStateReturn res = GST_STATE_SUCCESS;

  GST_LOG ("state change 0x%x", transition);

  switch (transition) {
    case GST_STATE_NULL_TO_READY:
      if (!gst_rec_link_pre (rec))
        return GST_STATE_FAILURE;
      break;
    case GST_STATE_READY_TO_NULL:
      gst_rec_unlink_pre (rec);
      break;
    case GST_STATE_READY_TO_PAUSED:
      if (rec->muxer && GST_IS_TAG_SETTER (rec->muxer)) {
        gst_tag_list_foreach (rec->taglist, cb_set_tag, rec->muxer);
      } else if (rec->video_encoder &&
                 GST_IS_TAG_SETTER (rec->video_encoder)) {
        gst_tag_list_foreach (rec->taglist, cb_set_tag, rec->video_encoder);
      } else if (rec->audio_encoder &&
                 GST_IS_TAG_SETTER (rec->audio_encoder)) {
        gst_tag_list_foreach (rec->taglist, cb_set_tag, rec->audio_encoder);
      }
      break;
    case GST_STATE_PAUSED_TO_READY:
      rec->started = FALSE;
      rec->lost_frames = 0;
      rec->ins_frames = 0;
      rec->del_frames = 0;
      rec->in_error = FALSE;
      break;
    case GST_STATE_PLAYING_TO_PAUSED:
      /* first do the state change on the sources, since they'll
       * eat memory and hang the rest even more. We do the rest
       * too then. The default impl does it in a different order
       * and that hangs us randomly... */
      GST_LOG_OBJECT (rec, "Doing play->pause state change on source threads");
      if (rec->video_source) {
        gst_element_set_state (rec->video_source_emulator, GST_STATE_PAUSED);
        gst_element_set_state (rec->video_source_thread, GST_STATE_PAUSED);
      }
      if (rec->audio_source) {
        gst_element_set_state (rec->audio_source_emulator, GST_STATE_PAUSED);
        gst_element_set_state (rec->audio_source_thread, GST_STATE_PAUSED);
      }
      GST_LOG_OBJECT (rec, "Now silencing the encoder threads");
      if (rec->video_encoder) {
        gst_element_set_state (rec->video_encoder_thread, GST_STATE_PAUSED);
      }
      if (rec->audio_encoder) {
        gst_element_set_state (rec->audio_encoder_thread, GST_STATE_PAUSED);
      }
      GST_LOG_OBJECT (rec, "Killing rest");
      gst_element_set_state (rec->muxer, GST_STATE_PAUSED);
      gst_element_set_state (rec->output, GST_STATE_PAUSED);
      GST_LOG_OBJECT (rec, "done");
      break;
    case GST_STATE_PAUSED_TO_PLAYING:
      rec->started = TRUE;
      break;
    default:
      break;
  }

  /* Setting state on the parent class will do the rest. */
  if (((GstElementClass *) parent_class)->change_state) {
    GST_DEBUG ("calling parent");
    res = ((GstElementClass *) parent_class)->change_state (element);
    GST_LOG ("Parent class reply: %d", res);
  }

  /* eh... :) */
  if (res == GST_STATE_FAILURE) {
    GST_LOG ("state change failed - reversing state change 0x%x->0x%x",
        transition, (transition & 0xff) << 8 | transition >> 8);
    transition = ((transition & 0xff) << 8 | transition >> 8);
  }

  /* This is fairly ugly, but we can only add the sources or
   * unlink everything after it's gone from/to NULL, so we
   * do it after setting state on the parent class. */
  switch (transition) {
    case GST_STATE_NULL_TO_READY:
      gst_rec_link_post (rec);
      break;
    case GST_STATE_READY_TO_NULL:
      gst_rec_unlink_post (rec);
      break;
    default:
      break;
  }

  GST_LOG ("Done");

  return res;
}

GstElementStateReturn
gst_rec_set_state (GstRec * rec, GstElementState state)
{
  if (rec->started && state <= GST_STATE_READY &&
      GST_STATE (rec) >= GST_STATE_PAUSED) {
    GstEvent *eos = gst_event_new (GST_EVENT_EOS);

    GST_LOG ("Sending EOS into pipeline");

    /* hmm, doubtful... note that this is expensive... */
    if (GST_STATE (rec) == GST_STATE_PAUSED)
      gst_element_set_state (GST_ELEMENT (rec), GST_STATE_PLAYING);

    if (rec->video_source) {
      GstPad *pad = gst_element_get_pad (rec->video_source_manager, "src");

      gst_pad_send_event (pad, gst_event_ref (eos));
    }
    if (rec->audio_source) {
      GstPad *pad = gst_element_get_pad (rec->audio_source_manager, "src");

      gst_pad_send_event (pad, gst_event_ref (eos));
    }

    gst_event_unref (eos);

    /* the EOS callback will take care of the actual state change */
    return GST_STATE_ASYNC;
  }

  /* default */
  return gst_element_set_state (GST_ELEMENT (rec), state);
}

/*
 * We use this idle function tweak because we're not allowed to
 * destroy (as in: set to GST_STATE_NULL) from ourselves. This
 * can, however, be done from within other threads (such as the
 * main thread), so we do that instead.
 */

static gboolean G_GNUC_UNUSED
gst_rec_destroy_myself (gpointer data)
{
  GstElement *rec = GST_ELEMENT (data);

  gst_element_set_state (rec, GST_STATE_NULL);

  /* once */
  return FALSE;
}

static void
gst_rec_eos (GstElement * element)
{
  GST_INFO ("EOS received, shutting down");

  if (GST_ELEMENT_CLASS (parent_class)->eos)
    GST_ELEMENT_CLASS (parent_class)->eos (element);

  /* first murder running threads */
  gst_element_set_state (element, GST_STATE_READY);
  /* then completely later on */
  g_idle_add (gst_rec_destroy_myself, element);
}

static void
gst_rec_error (GstElement * element,
    GstElement * source, GError * error, gchar * debug)
{
  GST_LOG ("Forwarding error %s [%d]", error, GST_STATE (element));

  if (GST_STATE (element) >= GST_STATE_PAUSED) {
    if (GST_REC (element)->in_error)
      return;
    GST_REC (element)->in_error = TRUE;

    if (GST_ELEMENT_CLASS (parent_class)->error)
      GST_ELEMENT_CLASS (parent_class)->error (element, source, error, debug);

    /* first murder running threads */
    //gst_element_set_state (element, GST_STATE_READY);
  } else {
    if (GST_ELEMENT_CLASS (parent_class)->error)
      GST_ELEMENT_CLASS (parent_class)->error (element, source, error, debug);
  }
}

/*
 * Interfaces...
 */

GstElement *
gst_rec_video_get_by_interface (GstRec * rec, GType type)
{
  return gst_rec_emulator_get_by_interface (
      GST_REC_EMULATOR (rec->video_source_emulator), type);
}

GstElement *
gst_rec_audio_get_by_interface (GstRec * rec, GType type)
{
  return gst_rec_emulator_get_by_interface (
      GST_REC_EMULATOR (rec->audio_source_emulator), type);
}

/*
 * Reference counting, keeping track of who does what,
 * and all other crap that makes people hate programming.
 */

static void
gst_rec_element_replace (GstRec * rec, GstRecEmulator * emulator,
    GstElement ** old, GstElement * new, GstElementState state, guint signal,
    GstCaps * (* func) (GstRec * rec))
{
  GstBin *bin = GST_BIN (emulator);

  /* no changes in pipeline while running */
  g_return_if_fail (GST_STATE (rec) == GST_STATE_NULL);

  if (*old != NULL) {
    if (bin) {
      gst_element_set_state (GST_ELEMENT (bin), GST_STATE_NULL);
      gst_bin_remove (bin, *old);
      gst_rec_emulator_set_filter (emulator, NULL);
    } else {
      gst_element_set_state (*old, GST_STATE_NULL);
    }
  }

  /* replace + do reference counting */
  gst_object_replace ((GstObject **) old, GST_OBJECT (new));

  if (new != NULL) {
    gst_object_sink (GST_OBJECT (new));

    /* set wanted state */
    if (bin) {
      gst_bin_add (bin, new);

      /* We call this signal twice. The second will be redundant. We do
       * this so we set up the video widget before we go into PLAYING
       * if emulation is disabled, which leads to random XWindow errors
       * or segfaults. */
      g_signal_emit (G_OBJECT (rec), signal, 0, new);

      /* We set filter before going to PLAYING, because the filter may
       * contain the autoprobed template of v4l webcams (which we do not
       * reprobe because it takes too long). After that, if initial setup
       * worked, we set it again so it actually plays. */
      gst_rec_emulator_set_filter (emulator, func (rec));
      if (gst_element_set_state (GST_ELEMENT (bin), state) ==
              GST_STATE_SUCCESS) {
        gst_rec_emulator_set_filter (emulator, func (rec));
      }
    } else {
      gst_element_set_state (new, state);
    }
  }

  g_signal_emit (G_OBJECT (rec), signal, 0, new);
}

static GstCaps *
gst_rec_update_video_filter (GstRec * rec)
{
  GstCaps *filter, *tmp;
  GstStructure *str;
  gint n, size;

  if (!rec->video_source)
    return NULL;

  /* get media type */
  filter = g_object_get_data (G_OBJECT (rec->video_source), "template");
  if (filter) {
    filter = gst_caps_copy (filter);
  } else {
    filter = gst_pad_get_caps (
        gst_element_get_pad (rec->video_source, "src"));
  }
  if (rec->video_filter) {
    tmp = filter;
    filter = gst_caps_intersect (tmp, rec->video_filter);
    gst_caps_free (tmp);
  }

  /* get size correct, only if it has size (DV!) */
  if (!gst_caps_is_empty (filter) &&
      (str = gst_caps_get_structure (filter, 0)) != NULL &&
      gst_structure_has_field (str, "width")) {
    size = gst_caps_get_size (filter);
    for (n = 0; n < size; n++) {
      GstStructure *str = gst_caps_get_structure (filter, n);

      gst_structure_set (str,
          "width", G_TYPE_INT, rec->video_width,
          "height", G_TYPE_INT, rec->video_height,
          "framerate", G_TYPE_DOUBLE, rec->video_fps, NULL);
    }
  }

  return filter;
}

static GstCaps *
gst_rec_update_audio_filter (GstRec * rec)
{
  GstCaps *filter, *tmp;
  gint n, size;

  if (!rec->audio_source)
    return NULL;

  /* get media type */
  filter = gst_pad_get_caps (
      gst_element_get_pad (rec->audio_source, "src"));
  if (rec->audio_filter) {
    tmp = filter;
    filter = gst_caps_intersect (tmp, rec->audio_filter);
    gst_caps_free (tmp);
  }

  /* get size correct */
  size = gst_caps_get_size (filter);
  for (n = 0; n < size; n++) {
    GstStructure *str = gst_caps_get_structure (filter, n);

    gst_structure_set (str,
        "channels", G_TYPE_INT, rec->audio_channels,
        "rate", G_TYPE_INT, rec->audio_samplerate, NULL);
  }

  return filter;
}

static gboolean
has_signal (gpointer obj, const gchar * name)
{
  GType type;
  guint *signals, num_signals, i;
  GSignalQuery query;

  for (type = G_OBJECT_TYPE (obj); type != 0; type = g_type_parent (type)) {
    signals = g_signal_list_ids (type, &num_signals);

    for (i = 0; i < num_signals; i++) {
      g_signal_query (signals[i], &query);

      /* no actions, only signals */
      if (query.signal_flags & G_SIGNAL_ACTION)
        continue;

      /* match? */
      if (!strcmp (query.signal_name, name)) {
        g_free (signals);
        return TRUE;
      }
    }

    g_free (signals);
  }

  return FALSE;
}

static void
add_int (GObject * obj, gpointer data)
{
  (*(guint *) data)++;
}

static void
g_signal_connect_if (GstElement * element,
    const gchar * name, GCallback handler, gpointer data)
{
  if (has_signal (element, name)) {
    g_signal_connect (G_OBJECT (element), name, handler, data);
  }
}

void
gst_rec_set_video_source (GstRec * rec, GstElement * video_source)
{
  /* disconnect signals */
  if (rec->video_source) {
    g_signal_handlers_disconnect_matched (rec->video_source,
        G_SIGNAL_MATCH_FUNC, 0, 0, NULL, G_CALLBACK (add_int), NULL);
  }

  gst_rec_element_replace (rec, GST_REC_EMULATOR (rec->video_source_emulator),
      &rec->video_source, video_source, GST_STATE_READY,
      signals[SIGNAL_VIDEO_SOURCE_CHANGED], gst_rec_update_video_filter);

  /* and reconnect */
  if (rec->video_source) {
    g_signal_connect_if (rec->video_source, "frame_lost",
        G_CALLBACK (add_int), &rec->lost_frames);
    g_signal_connect_if (rec->video_source, "frame_insert",
        G_CALLBACK (add_int), &rec->ins_frames);
    g_signal_connect_if (rec->video_source, "frame_drop",
        G_CALLBACK (add_int), &rec->del_frames);
  }
}

void
gst_rec_set_audio_source (GstRec * rec, GstElement * audio_source)
{
  gst_rec_element_replace (rec, GST_REC_EMULATOR (rec->audio_source_emulator),
      &rec->audio_source, audio_source, GST_STATE_READY,
      signals[SIGNAL_AUDIO_SOURCE_CHANGED], gst_rec_update_audio_filter);
}

void
gst_rec_set_video_encoder (GstRec * rec, GstElement * video_encoder)
{
  gst_rec_element_replace (rec, NULL, &rec->video_encoder, video_encoder,
      GST_STATE_NULL, signals[SIGNAL_VIDEO_ENCODER_CHANGED], NULL);
}

void
gst_rec_set_audio_encoder (GstRec * rec, GstElement * audio_encoder)
{
  gst_rec_element_replace (rec, NULL, &rec->audio_encoder, audio_encoder,
      GST_STATE_NULL, signals[SIGNAL_AUDIO_ENCODER_CHANGED], NULL);
}

void
gst_rec_set_muxer (GstRec * rec, GstElement * muxer)
{
  gst_rec_element_replace (rec, NULL, &rec->muxer, muxer,
      GST_STATE_NULL, signals[SIGNAL_MUXER_CHANGED], NULL);
}

void
gst_rec_set_output (GstRec * rec, GstElement * output)
{
  gst_rec_element_replace (rec, NULL, &rec->output, output,
      GST_STATE_NULL, signals[SIGNAL_OUTPUT_CHANGED], NULL);
}

static void
gst_rec_caps_replace (GstRec * rec, GstCaps ** old, GstCaps * new, guint signal)
{
  /* no changes in pipeline while running */
  g_return_if_fail (GST_STATE (rec) == GST_STATE_NULL);

  /* replace + do reference counting */
  gst_caps_replace (old, new ? gst_caps_copy (new) : NULL);

  g_signal_emit (G_OBJECT (rec), signal, 0, new);
}

void
gst_rec_set_video_filter (GstRec * rec, GstCaps * video_filter)
{
  gst_rec_caps_replace (rec, &rec->video_filter, video_filter,
      signals[SIGNAL_VIDEO_FILTER_CHANGED]);
  gst_rec_update_video_filter (rec);
}

void
gst_rec_set_audio_filter (GstRec * rec, GstCaps * audio_filter)
{
  gst_rec_caps_replace (rec, &rec->audio_filter, audio_filter,
      signals[SIGNAL_AUDIO_FILTER_CHANGED]);
  gst_rec_update_audio_filter (rec);
}

#define replace_num_func(type) \
static void \
gst_rec_ ##type## _replace (GstRec * rec, type * old, type new, guint signal) \
{ \
  /* no changes in pipeline while running */ \
  g_return_if_fail (GST_STATE (rec) == GST_STATE_NULL); \
 \
  /* replace */ \
  *old = new; \
 \
  g_signal_emit (G_OBJECT (rec), signal, 0, new); \
}

replace_num_func(uint);
replace_num_func(double);

void
gst_rec_set_video_width (GstRec * rec, guint video_width)
{
  gst_rec_uint_replace (rec, &rec->video_width, video_width,
      signals[SIGNAL_VIDEO_WIDTH_CHANGED]);
  gst_rec_update_video_filter (rec);
}

void
gst_rec_set_video_height (GstRec * rec, guint video_height)
{
  gst_rec_uint_replace (rec, &rec->video_height, video_height,
      signals[SIGNAL_VIDEO_HEIGHT_CHANGED]);
  gst_rec_update_video_filter (rec);
}

void
gst_rec_set_video_fps (GstRec * rec, gdouble video_fps)
{
  gst_rec_double_replace (rec, &rec->video_fps, video_fps,
      signals[SIGNAL_VIDEO_FPS_CHANGED]);
  gst_rec_update_video_filter (rec);
}

void
gst_rec_set_audio_samplerate (GstRec * rec, guint audio_samplerate)
{
  gst_rec_uint_replace (rec, &rec->audio_samplerate, audio_samplerate,
      signals[SIGNAL_AUDIO_SAMPLERATE_CHANGED]);
  gst_rec_update_audio_filter (rec);
}

void
gst_rec_set_audio_channels (GstRec * rec, guint audio_channels)
{
  gst_rec_uint_replace (rec, &rec->audio_channels, audio_channels,
      signals[SIGNAL_AUDIO_CHANNELS_CHANGED]);
  gst_rec_update_audio_filter (rec);
}

/*
 * Corresponding _get () functions for each of the above.
 */

GstElement *
gst_rec_get_video_source (GstRec * rec)
{
  return rec->video_source;
}

GstElement *
gst_rec_get_audio_source (GstRec * rec)
{
  return rec->audio_source;
}

GstElement *
gst_rec_get_video_encoder (GstRec * rec)
{
  return rec->video_encoder;
}

GstElement *
gst_rec_get_audio_encoder (GstRec * rec)
{
  return rec->audio_encoder;
}

GstElement *
gst_rec_get_muxer (GstRec * rec)
{
  return rec->muxer;
}

GstElement *
gst_rec_get_output (GstRec * rec)
{
  return rec->output;
}

GstCaps *
gst_rec_get_video_filter (GstRec * rec)
{
  return rec->video_filter;
}

GstCaps *
gst_rec_get_audio_filter (GstRec * rec)
{
  return rec->audio_filter;
}

guint
gst_rec_get_video_width (GstRec * rec)
{
  return rec->video_width;
}

guint
gst_rec_get_video_height (GstRec * rec)
{
  return rec->video_height;
}

gdouble
gst_rec_get_video_fps (GstRec * rec)
{
  return rec->video_fps;
}

guint
gst_rec_get_audio_samplerate (GstRec * rec)
{
  return rec->audio_samplerate;
}

guint
gst_rec_get_audio_channels (GstRec * rec)
{
  return rec->audio_channels;
}

/*
 * Snapshot.
 */

gboolean
gst_rec_get_snapshot (GstRec * rec,
    GstBuffer ** buffer, GstCaps ** caps)
{
  if (!rec->video_source)
    return FALSE;

  *buffer = NULL;
  g_object_get (rec->video_source_emulator, "frame", buffer, NULL);
  if (!*buffer)
    return FALSE;
  g_object_get (rec->video_source_emulator, "format", caps, NULL);

  return TRUE;
}

/*
 * Metadata.
 */

const gchar *
gst_rec_get_tag (GstRec * rec, const gchar * tag)
{
  gchar *value;

  if (gst_tag_list_get_string (rec->taglist, tag, &value))
    return value;

  return NULL;
}

void
gst_rec_set_tag (GstRec * rec, const gchar * tag, const gchar * value)
{
  gst_tag_list_add (rec->taglist, GST_TAG_MERGE_REPLACE_ALL, tag, value, NULL);
}
