/* GStreamer Recorder
 * (c) 2003 Ronald Bultje <rbultje@ronald.bitfreak.net>
 *
 * preferences.h: definition of preferences dialog
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gtk/gtk.h>
#include "configuration.h"

#ifndef __GST_REC_PREFERENCES_H__
#define __GST_REC_PREFERENCES_H__

#define GST_REC_TYPE_PREFERENCES \
  (gst_rec_preferences_get_type())
#define GST_REC_PREFERENCES(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_REC_TYPE_PREFERENCES, \
			      GstRecPreferences))
#define GST_REC_PREFERENCES_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass), GST_REC_TYPE_PREFERENCES, \
			   GstRecPreferencesClass))
#define GST_REC_IS_PREFERENCES(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_REC_TYPE_PREFERENCES))
#define GST_REC_IS_PREFERENCES_CLASS(obj) \
  (G_TYPE_CHECK_CLASS_TYPE((klass), GST_REC_TYPE_PREFERENCES))

typedef struct _GstRecPreferences GstRecPreferences;

struct _GstRecPreferences
{
  GstRecConfiguration parent;

  /* contains everything */
  GtkNotebook *notebook;
};

typedef struct _GstRecPreferencesClass
{
  GstRecConfigurationClass parent;
} GstRecPreferencesClass;

GType gst_rec_preferences_get_type (void);

/*
 * Create new preferences dialog. Note that all changes
 * apply directly.
 */
GtkWidget *gst_rec_preferences_new (GstRec * rec, GConfClient * client);

#endif /* __GST_REC_PREFERENCES_H__ */
